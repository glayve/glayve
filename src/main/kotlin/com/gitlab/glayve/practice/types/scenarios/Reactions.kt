package com.gitlab.glayve.practice.types.scenarios

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.network.Position
import com.gitlab.glayve.network.addEffect
import com.gitlab.glayve.network.title
import com.gitlab.glayve.practice.Freeze
import com.gitlab.glayve.practice.Play
import com.gitlab.glayve.practice.games.Arena
import com.gitlab.glayve.practice.show
import com.gitlab.glayve.world.BLOCK_ADOBE
import com.gitlab.glayve.world.MutableWorld
import kotlin.time.Duration.Companion.ZERO
import kotlin.time.Duration.Companion.seconds

val reactionTopLeft = Position(11186, 72, 8562)
val reactionBottomRight = Position(11184, 70, 8562)
val reactionWall = ArrayList<Position>().apply {
    for (y in (reactionBottomRight.y)..(reactionTopLeft.y))
        for (x in (reactionBottomRight.x)..(reactionTopLeft.x))
            add(Position(x, y, reactionBottomRight.z))
}

// add failed too early red
fun Togglable.Reactions(world: MutableWorld, online: Adepts, players: Adepts, arena: Arena) {
    val spectators = online.filter { id, _ -> id !in players }
    spectators.show(players)
    Play(players, arena.red)
    online.onEach { id, adept ->
        adept.addEffect(id, Effect.DIG_DOWN.toByte(), -1, -1)
    }

    val best = Published(ZERO)
    val inGame = phased {
        players.onEach { _, adept ->
            adept.onRightClick { this@phased.disable(); CONSUME }
            adept.onLeftClick {
                this@phased.disable();
                CONSUME
            }
        }
        measure {
            if (it < best.last || best.last == ZERO) best(it)
            online.send {
                title(
                    "${GOLD}${it.inWholeMilliseconds}ms",
                    stay = 1.seconds, wax = .2.seconds
                )
            }
        }
        onDisabled {
            reactionWall.forEach {
                world[it] = BLOCK_ADOBE[COLORED_RED]
            }
        }
        println()

        onEnabled {
            reactionWall.forEach { world[it] = BLOCK_ADOBE[COLORED_GREEN] }
        }
    }

    val delay = phased {
        onEnabled {
            transient {
                scheduler.after((2..10).random().seconds)() {
                    inGame.enable()
                    this@phased.disable()
                }
            }
        }

    }

    players.onEach { _, adept ->
        adept.onLeftClick { interaction ->
            if (delay.enabled) {
                delay.disable()
                online.send {
                    title(
                        "${RED}Failed",
                        stay = 1.seconds, wax = .2.seconds
                    )
                }
                delay.enable()
            }; PASS
        }

        adept.board(0, just("${RED}${BOLD}Aim Practice"))
        adept.board(1, date())
        adept.board(2, just(" "))
        adept.board(4, just("Scenario:$RED Reactions"))
        adept.board(5, best.map { "Personal Best:$RED ${it.inWholeMilliseconds} ms" })
        adept.board(6, just(" "))
        adept.board(7, just(DOMAIN))
        // adept.gamemode(Gamemode.ADVENTURE)
    }

    Freeze(players)

    this.onEnabled(delay::enable)
    //  delay.onDisabled(inGame::enable)
    inGame.onDisabled(delay::enable)
}