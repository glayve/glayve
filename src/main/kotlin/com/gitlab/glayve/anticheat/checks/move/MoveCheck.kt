package com.gitlab.glayve.anticheat.checks.move

import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.onChanged
import com.gitlab.ballysta.architecture.onEach
import com.gitlab.glayve.Adept
import com.gitlab.glayve.Adepts
import com.gitlab.glayve.GENERIC_SPEED
import com.gitlab.glayve.anticheat.CheckType
import com.gitlab.glayve.anticheat.flag
import com.gitlab.glayve.anticheat.utils.math.Buffer
import com.gitlab.glayve.anticheat.utils.math.Vector2D
import com.gitlab.glayve.anticheat.utils.math.ceilToInt
import com.gitlab.glayve.anticheat.utils.math.floorToInt
import com.gitlab.glayve.network.Position
import com.gitlab.glayve.util.*
import com.gitlab.glayve.world.MutableWorld
import com.gitlab.glayve.world.material
import com.gitlab.glayve.world.meta

fun Toggled.MoveCheck(adepts: Adepts, world: MutableWorld) {
    adepts.onEach { _, adept ->
        var liquidTicks = 0
        val moveDataBuf = Buffer<MoveData>(5)

        adept.location.onChanged { from, to ->
            // prepare data
            val speedAttribute = adept.attributes[GENERIC_SPEED]?.plus(0.03) ?: 0.13
            val moveData = MoveData.build(from, to, speedAttribute)

            // analysis the movement
            if (moveDataBuf.isFull()) {
                // detect liquid condition
                if (liquidTicks > 0) liquidTicks--

                // analyse the movement
                if (isInLiquid(moveData, world)) {
                    liquidTicks = 40
                    handleMovementLiquid(adept, moveData, moveDataBuf, world)
                } else {
                    handleMovementNormal(adept, moveData, moveDataBuf, world)
                }
            }

            // save the movement into buffer
            moveDataBuf += moveData
        }
    }
}

private fun handleMovementNormal(adept: Adept, currMovement: MoveData, pastMovements: Buffer<MoveData>, world: MutableWorld) {
    val lastMovement = pastMovements.latest()

    val predictionOfLast = lastMovement.predictionNext

    // Check Ground Spoof
    if(currMovement.to.ground){
        val destY = currMovement.to.y
        val toBox = Bounds.ofEntity(currMovement.to, 0.6, 1.8).expand(Velocity(0.1, 0.51, 0.1))
        val groundSpoof = toBox.blocksInside(world).none { (block, location) ->
            block.bounds !== Bounds.NONE
                    && destY in (block.bounds.maxY + location.y)..(block.bounds.maxY + location.y + 0.065)
        }
        if(groundSpoof){
            adept.flag(
                type = CheckType.MOVE,
                message = "ground spoof",
                vl = 1
            )
        }
    }

    val predictVerticalMovement = predictionOfLast.x * 0.98f

    // Prepare the data for next movement prediction
    lastMovement.predictionNext = Vector2D(
        x = currMovement.deltaY - 0.08,
        z = currMovement.distanceHorizontal // todo: predict it
    )
}

private fun handleMovementLiquid(adept: Adept, currMovement: MoveData, pastMovements: Buffer<MoveData>, world: MutableWorld) {

}

private fun isInLiquid(moveData: MoveData, world: MutableWorld): Boolean {
    val playerBox = Bounds.ofEntity(moveData.from, 0.3, 1.8).expandDirectional(
        dirX = moveData.to.x - moveData.from.x,
        dirY = moveData.to.y - moveData.from.y,
        dirZ = moveData.to.z - moveData.from.z
    )
    val i: Int = floorToInt(playerBox.minX)
    val j: Int = ceilToInt(playerBox.maxX)
    val k: Int = floorToInt(playerBox.minY)
    val l: Int = ceilToInt(playerBox.maxY)
    val m: Int = floorToInt(playerBox.minZ)
    val n: Int = ceilToInt(playerBox.maxZ)
    for (p in i until j) {
        for (q in k until l) {
            for (r in m until n) {
                world[Position(p, q, r)].let {
                    if (it.material in 8..11) {
                        val maxY = q + 1 - ((it.meta + 1) / 9f) + 0.1
                        if (maxY >= playerBox.minY) {
                            return true
                        }
                    }
                }
            }
        }
    }
    return false
}