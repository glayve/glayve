@file:Suppress("NOTHING_TO_INLINE")
package com.gitlab.glayve.util

import com.gitlab.glayve.network.Position
import kotlin.math.pow
import kotlin.math.sqrt

typealias Vector = Velocity

@JvmInline value class Velocity(private val values: DoubleArray) {
    val x get() = values[0]
    val y get() = values[1]
    val z get() = values[2]
    val compressedX get() = (x * 8000).toInt().toShort()
    val compressedY get() = (y * 8000).toInt().toShort()
    val compressedZ get() = (z * 8000).toInt().toShort()

    operator fun get(index: Int) = values[index]

    inline fun copy(x: Double = this.x, y: Double = this.y, z: Double = this.z)
            = Velocity(x, y, z)
}
inline fun Velocity(x: Double, y: Double, z: Double)
        = Velocity(doubleArrayOf(x, y, z))
inline fun Vector(x: Double, y: Double, z: Double)
        = Velocity(doubleArrayOf(x, y, z))
inline fun Velocity(unit: Double)
        = Velocity(unit, unit, unit)
operator fun Velocity.times(amount: Double) =
    Velocity(x * amount, y * amount, z * amount)
operator fun Velocity.unaryMinus() =
    Velocity(-x, -y, -z)
operator fun Velocity.minus(other: Velocity) =
    Velocity(x - other.x, y - other.y, z - other.z)
operator fun Velocity.plus(other: Velocity) =
    Velocity(x + other.x, y + other.y, z + other.z)
operator fun Velocity.times(other: Velocity) =
    Velocity(x * other.x, y * other.y, z * other.z)
operator fun Velocity.div(other: Velocity) =
    Velocity(x / other.x, y / other.y, z / other.z)

operator fun Velocity.component1() = x
operator fun Velocity.component2() = y
operator fun Velocity.component3() = z

val Velocity.lengthSquared get() = (x * x) + (y * y) + (z * z)
val Velocity.length get() = sqrt(lengthSquared)
fun Velocity.normalize() = length.let { Velocity(x / it, y / it, z / it) }
fun Velocity.normalizeZeros(): Velocity = Velocity(
    x = if (x == -0.0) 0.0 else x,
    y = if (y == -0.0) 0.0 else y,
    z = if (z == -0.0) 0.0 else z
)
infix fun Velocity.distance(another: Velocity) = sqrt(
(x - another.x).pow(2) + (y - another.y).pow(2) + (z - another.z).pow(2)
)

fun Position.toVelocity() = Velocity(x.toDouble(), y.toDouble(), z.toDouble())