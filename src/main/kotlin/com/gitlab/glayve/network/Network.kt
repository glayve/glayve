package com.gitlab.glayve.network

import com.github.exerosis.mynt.SocketProvider
import com.github.exerosis.mynt.base.Address
import com.github.exerosis.mynt.base.Connection
import com.github.exerosis.mynt.base.Read
import com.github.exerosis.mynt.base.Write
import com.github.exerosis.mynt.bytes
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.util.Location
import com.gitlab.glayve.util.Velocity
import com.mojang.authlib.GameProfile
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel as Queue
import kotlinx.coroutines.channels.consumeEach
import java.net.Proxy.NO_PROXY
import java.net.StandardSocketOptions.SO_KEEPALIVE
import java.net.StandardSocketOptions.TCP_NODELAY
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousChannelGroup.withThreadPool
import java.nio.channels.ClosedChannelException
import java.nio.charset.StandardCharsets.UTF_8
import java.util.*
import java.util.UUID.randomUUID
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedDeque
import java.util.concurrent.ExecutorService
import java.util.concurrent.ThreadLocalRandom.current
import javax.crypto.Cipher.DECRYPT_MODE
import javax.crypto.Cipher.ENCRYPT_MODE
import kotlin.math.max

const val WIDTH = 0.3;
const val HEIGHT = 1.8
typealias Version = Int
typealias Profile = GameProfile

suspend inline fun <Return> Read.packet(id: Int, block: Read.() -> (Return)): Return {
    varInt() //Packet length. (Ignored)
    if (varInt() == id) return block()
    throw IllegalStateException("No longer in sync, aborting!")
}

class BufferedWrite(capacity: Int) : Write {
    val buffer = ByteBuffer.allocateDirect(capacity)!!

    override suspend fun buffer(buffer: ByteBuffer) {
        buffer.put(buffer)
    }

    override suspend fun bytes(bytes: ByteArray, amount: Int, offset: Int) {
        buffer.put(bytes, offset, amount)
    }

    override suspend fun skip(amount: Int) {
        buffer.position(buffer.position() + amount)
    }

    override suspend fun byte(byte: Byte) {
        buffer.put(byte)
    }

    override suspend fun short(short: Short) {
        buffer.putShort(short)
    }

    override suspend fun int(int: Int) {
        buffer.putInt(int)
    }

    override suspend fun long(long: Long) {
        buffer.putLong(long)
    }

    override suspend fun float(float: Float) {
        buffer.putFloat(float)
    }

    override suspend fun double(double: Double) {
        buffer.putDouble(double)
    }
}

var largest = 0

val WRITERS = ConcurrentLinkedDeque<BufferedWrite>()
suspend fun <Return> Write.packet(id: Int, block: suspend Write.() -> (Return)): Return {
    val writer = WRITERS.poll() ?: BufferedWrite(5000000)
    writer.varInt(id)
    val result = block(writer)
    largest = max(writer.buffer.position(), largest)
    varInt(writer.buffer.position())
    buffer(writer.buffer)
    WRITERS.push(writer)
    return result
}

fun Toggled.LoginComponent(
    executor: ExecutorService, address: Address, online: Bool,
    onHandshake: suspend Connection.(Version, Address) -> (String),
    onJoin: suspend Connection.(Version, Address, Profile) -> (Unit)
) {
    val authentication = YggdrasilAuthenticationService(NO_PROXY, randomUUID().toString())
    val sessions = authentication.createMinecraftSessionService()
    val dispatcher = executor.asCoroutineDispatcher()
    val provider = SocketProvider(1048576, withThreadPool(executor)) {
        it.setOption(TCP_NODELAY, true)
        it.setOption(SO_KEEPALIVE, false)
    }
    val (public, private) = createKeys("RSA")
    val decryption = RSA(DECRYPT_MODE, private)
    val server = "".toByteArray(UTF_8)
    var task: Deferred<Unit>? = null
    onEnabled {
        task = GlobalScope.async(dispatcher) {
            while (provider.isOpen) try {
                provider.accept(address).apply {
                    GlobalScope.launch {
                        try {
                            read.varInt() //Handshake length. (Ignored)
                            if (read.varInt() != 0) return@launch
                            val version = read.varInt()
                            val remote = Address(read.string(), read.short().toInt())
                            if (read.varInt() == 1) read.packet(0x00) {
                                write.packet(0x00) { string(onHandshake(version, remote)) }
                                val time = read.packet(0x01) { long() }
                                write.packet(0x01) { long(time) }
                            } else try {
                                if (version != 47) error("Must login with a 1.8 client.")
                                val username = read.packet(0x00) { string() }
                                if (online) {
                                    val tokens = ByteArray(4).also(current()::nextBytes)
                                    write.packet(0x01) {
                                        varInt(server.size); bytes(server)
                                        varInt(public.size); bytes(public)
                                        varInt(tokens.size); bytes(tokens)
                                    }
                                    val secret = read.packet(0x01) {
                                        val secret = decryption.doFinal(bytes(varInt()))
                                        val verify = decryption.doFinal(bytes(varInt()))
                                        if (tokens.contentEquals(verify)) secret
                                        else error("Invalid authentication token.")
                                    }
                                    onJoin(
                                        object : Connection by this@apply {
                                            override val read = decrypt(this@apply.read, AES(DECRYPT_MODE, secret))
                                            override val write = encrypt(this@apply.write, AES(ENCRYPT_MODE, secret))
                                        }, version, remote, sessions.hasJoinedServer(
                                            GameProfile(null, username), createHexdigest(secret, public)
                                        ) ?: error("Bad login, restart client.")
                                    )
                                } else onJoin(version, remote, GameProfile(null, username))
                            } catch (reason: Throwable) {
                                write.packet(0x00) {
                                    string("{\"text\": \"${reason.message ?: "Unknown Error"}\"}")
                                }
                            }
                        } catch (ignored: Throwable) {
                        }
                    }
                }
            } catch (ignored: Throwable) {
            }
        }
    }
    onDisabled {
        runBlocking {
            try {
                //FIXME find a way to make shutting down the server work
                //provider.close(); task?.await(); task = null
            } catch (ignored: Throwable) {
            }
        }
    }
}

fun Toggled.PlayerComponent(
    executor: ExecutorService, address: Address, online: Bool = true,
    onHandshake: suspend Connection.(Version, Address) -> (String)
): Table<UUID, Channel> {
    val names = ConcurrentHashMap<String, UUID>()
    val channels = MutableTable<UUID, Channel>()
    val dispatcher = executor.asCoroutineDispatcher()
    onDisabled {
        channels.forEach { id, channel ->
            channel.kick("Shutdown"); channels[id] = null
        }
    }
    LoginComponent(
        executor, address, online,
        onJoin = { version, _, user ->
            val profile = if (user.id != null) user else GameProfile(
                names.getOrPut(user.name) { randomUUID() }, user.name
            ); write.packet(0x02) { string(profile.id.toString()); string(profile.name) }
//            if (remote.hostName.endsWith("\u0000FML\u0000")) {
//                write.packet(0x00) { string("{\"text\": \"Forge not allowed!\"}") }
//                return@LoginComponent
//            }
            try {
                val inbound = Array<suspend Read.(Int) -> (Unit)>(26) { { bytes(it) } }
                val channel = object : Channel {
                    override val version = version
                    override val address = address
                    override val id = EID.getAndIncrement()
                    override val name = profile.name
                    override val uuid = profile.id
                    override val properties = profile.properties.values().toTypedArray()

                    override val onWindowClick = TreeEvent<(Byte, Slot, Int, Short, Byte, Stack) -> (Unit)>()
                    override val onWindowSlot = TreeEvent<(Slot, Stack) -> (Unit)>()
                    override val onWindowDrop = TreeEvent<(Slot, Int) -> Unit>()
                    override val onWindowConfirm = TreeEvent<(Byte, Short, Bool) -> (Unit)>()
                    override val onWindowClose = TreeEvent<(Byte) -> Unit>()

                    override val onTab = TreeEvent<(String) -> (Unit)>()
                    override val onChat = TreeEvent<(String) -> (Unit)>()

                    override val location = Published(Location(0.0, 0.0, 0.0, 0f, 0f, false))
                    override val onRelease = TreeEvent<() -> (Unit)>()
                    override val selected = Published(36.toByte())
                    override val onSwing = TreeEvent<() -> (Unit)>()
                    override val sneaking = Published(false)
                    override val sprinting = Published(false)
                    override val health = Published(20f)
                    override val shield = Published(0f)
                    override val saturation = Published(1f)
                    override val gamemode = Published(Gamemode.SURVIVAL)
                    override val difficulty = Published(3.toByte())
                    override val food = Published(20)
                    override val burning = Published(false)

                    override fun toString() = name
                    override fun equals(other: Any?) = other is Channel && other.id == id

                    override val digging = object : Component() {
                        override fun invoke(value: Bool) {
                            super.invoke(value); if (!value) clear()
                        }
                    }
                    override val using = object : Component() {
                        override fun invoke(value: Bool) {
                            super.invoke(value); if (!value) clear()
                        }
                    }

                    override val onLeftClick = object : TreeEvent<Togglable.(Interaction) -> (Bool)>() {
                        operator fun invoke(interaction: Interaction) {
                            digging.disable()
                            this(digging, interaction)
                        }
                    }
                    override val onRightClick = object : TreeEvent<Togglable.(Interaction) -> (Bool)>() {
                        operator fun invoke(interaction: Interaction) {
                            using.disable() //If someone forgot to disable their item.
                            this(using, interaction)
                        }
                    }

                    init {
                        inbound[IN_TAB_COMPLETE] = {
                            onTab(string())
                            if (bool()) position()
                        }
                        inbound[IN_CHAT] = { onChat(string()) }
                        inbound[IN_INTERACT] = {
                            val entity = varInt()
                            when (varInt()) {
                                0 -> onRightClick(Interaction(null, null, entity, null))
                                1 -> onLeftClick(Interaction(null, null, entity, null))
                                else -> onRightClick(
                                    Interaction(
                                        null, null, entity, Velocity(
                                            x = float().toDouble(),
                                            y = float().toDouble(),
                                            z = float().toDouble()
                                        )
                                    )
                                )
                            }
                        }
                        inbound[IN_PLAYER] = { location(location.last.copy(ground = bool())) }
                        inbound[IN_PLAYER_POSITION] = {
                            location(
                                location.last.copy(
                                    x = double(), y = double(), z = double(), ground = bool()
                                )
                            )
                        }
                        inbound[IN_PLAYER_LOOK] = {
                            location(
                                location.last.copy(
                                    yaw = float(), pitch = float(), ground = bool()
                                )
                            )
                        }
                        inbound[IN_PLAYER_POSITION_LOOK] = {
                            location.invoke(
                                Location(
                                    double(), double(), double(), float(), float(), bool()
                                )
                            )
                        }

                        inbound[IN_WINDOW_CONFIRM] = {
                            onWindowConfirm(byte(), short(), bool())
                        }

                        inbound[IN_WINDOW_CLICK] = {
                            onWindowClick(
                                byte(), slot(), byte().toInt(), short(), byte(), stack()
                            )
                        }

                        inbound[IN_WINDOW_SLOT] = { onWindowSlot(slot(), stack()) }

                        inbound[IN_BLOCK_DIGGING] = {
                            val status = byte().toInt()
                            val block = position()
                            val face = face()
                            when (status) {
                                0 -> onLeftClick(Interaction(block, face, null, null))
                                1 -> digging.disable()
                                2, 5 -> onRelease() //TODO is it a problem that we won't be able to verify here?
                                else -> onWindowDrop(selected.last, status)
                            }
                        }
                        inbound[IN_BLOCK_PLACEMENT] = {
                            val position = position()
                            val face = byte().toInt()
                            stack()
                            val offset = Velocity(byte().toDouble(), byte().toDouble(), byte().toDouble())
                            val faced = face in Face.values().indices
                            onRightClick(
                                Interaction(
                                    if (faced) position else null,
                                    if (faced) Face.values()[face] else null,
                                    null, if (faced) offset else null
                                )
                            )
                        }

                        inbound[IN_ITEM_CHANGE] = { selected((36 + short()).toByte()) }
                        inbound[IN_ANIMATION] = { onSwing() }
                        inbound[IN_ACTION] = {
                            varInt() //Entity id. (Ignored)
                            when (varInt()) {
                                0 -> sneaking(true)
                                1 -> sneaking(false)
                                3 -> sprinting(true)
                                4 -> sprinting(false)
                            }
                            varInt() //Horse jump boost. (Ignored)
                        }

                        inbound[IN_WINDOW_CLOSE] = { onWindowClose(byte()) }
                    }

                    val queue = Queue<suspend Write.() -> (Unit)>(10_000)

                    override fun send(id: Int, packet: suspend Write.() -> (Unit)) {
                        if (queue.isClosedForSend || !isOpen) return
                        if (!queue.trySend { packet(id, packet) }.isSuccess)
                            queue.close(Error("Connection unsuitable for online play!"))
                    }

                    override fun kick(message: String) = Unit.apply { queue.close(Error(message)) }
                }
                write.packet(OUT_JOIN_GAME) {
                    int(channel.id); byte(0); byte(0); byte(0)
                    byte(100); string("default"); bool(!DEBUG)
                }
                //FIXME why is downloading terrain screen closing instantly?
                write.packet(OUT_RESPAWN) { int(1); short(0); string("") }
                channels.set(profile.id, channel)?.kick("You logged in elsewhere!")

                GlobalScope.launch(dispatcher) {
                    try {
                        while (isOpen) {
                            val length = read.varInt() - 1; val id = read.varInt()
                            if (read.measure { inbound[id](length) } != length)
                                error("Read head out of sync!")
                        }
                    } catch (reason: Throwable) {
                        channel.queue.close(reason)
                    }
                }
                GlobalScope.launch { while (isOpen) {
                    delay(10_000); channel.send(0x00) { byte(0) }
                } }

                channel.queue.consumeEach {
                    try { it(write) }
                    catch (reason: Throwable) {
                        channel.queue.close(reason)
                    }
                }
            } catch (ignored: ClosedChannelException) {
            } catch (reason: Throwable) {
                if (reason !is Error) reason.printStackTrace()
                try {
                    write.packet(OUT_DISCONNECT) {
                        string(reason.message ?: "Unknown Error")
                    }
                } catch (ignored: Throwable) {}
            } finally { close(); channels[profile.id] = null }
        },
        onHandshake = onHandshake
    ); return channels
}