package com.gitlab.glayve.practice.types.scenarios.looting

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Container
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.network.*
import com.gitlab.glayve.practice.*
import com.gitlab.glayve.practice.games.*
import com.gitlab.glayve.world.material
import kotlin.time.Duration.Companion.ZERO
import kotlin.time.Duration.Companion.seconds

// add hotkeying
fun Togglable.ChestGrab(online: Adepts, players: Adepts, arena: Arena, stack: () -> (Stack)) {
    val spectators = online.filter { id, _ -> id !in players }
    spectators.onRemoved { _, adept -> println("Spectators - $adept") }
    spectators.onAdded { _, adept -> println("Spectators + $adept") }
    Play(players, arena.red)
    CancelClicks(players)
    Freeze(players)

    val scores = MutableTable<Difficulty, Int>()
    val grabs = Published(0)
    val timer = Published(0)

    val preGame = phased { onEnabled { grabs(0) } }
    val difficulty = preGame.DifficultySelector(players, 36, EASY, MEDIUM, HARD)
    val items = difficulty.map {
        when (it) {
            EASY -> 5; MEDIUM -> 3; else -> 1
        }
    }
    val inGame = phased inGame@{
        scheduler.every(1.seconds, ZERO)(timer)
        timer.filter { 15 - it == 0 }() { disable() }
        players.onEach { _, adept ->
            adept.onWindowClose { this@inGame.disable() }
        }

        onDisabled {
            if (grabs.last > (scores[difficulty.last] ?: 0)) {
                scores[difficulty.last] = grabs.last
            }
        }
    }

    val postGame = phased {
        players.onEach { _, adept -> adept.closeWindow(1) }
        onEnabled {
            players.send {
                title(
                    "${RED}${grabs.last}", "${GOLD}Items",
                    stay = 3.seconds, wax = .2.seconds
                )
            }
        }

        scheduler.after(3.seconds)() { this@phased.disable() }
    }

    online.onEach { _, adept ->
        adept.board[0] = just("${RED}${BOLD}Chest Practice")
        adept.board[1] = date()
        adept.board[2] = just(" ")
        adept.board[3] = timer.map { "Time Left: $RED${15 - it} Second(s)" }
        adept.board(4, grabs.map { "Score:$RED $it Item(s)" })
        adept.board(5, difficulty.map { "${WHITE}Difficulty: ${it.name}" })
        adept.board(6, just(" "))
        adept.board(7, just("Scenario:$RED Chest Looting I"))
        adept.board(8, scores.watch(difficulty).map { "Personal Record: $RED${it ?: 0} Items" })
        adept.board(9, just(" "))
        adept.board(10, just(DOMAIN))
        adept.gamemode(Gamemode.ADVENTURE)
    }

    players.onEach { _, adept ->
        adept.inventory.onShiftClick { slot ->
            if (!postGame.enabled &&
                adept.inventory[slot]!!.count != 0.toByte() && slot != 36.toByte() &&
                adept.inventory.type.last == Container.CHEST_3
            ) {
                grabs(grabs.last + 1)
                val found = (45..71).filter { adept.inventory[it.toByte()]!!.count == 0.toByte() }.random()
                adept.inventory[slot] = Stack.EMPTY
                adept.inventory[found.toByte()] = stack()
                if (preGame.enabled) preGame.disable()
            }; CONSUME
        }

        adept.onRightClick { interaction ->
            if (!postGame.enabled && interaction.position != null && adept.blocks[interaction.position].material == 54.toShort()) {
                adept.inventory.title("""{"text": "Chest"}""")
                adept.inventory.type(Container.CHEST_3)
                adept.inventory.clear(45..71)
                (45..71).shuffled().take(items.last).forEach {
                    adept.inventory[it.toSlot()] = stack()
                }; CONSUME
            } else PASS
        }

        adept.location { if (it.y <= 72) adept.location(arena.red) }
    }

    this.onEnabled(preGame::enable)
    preGame.onDisabled(inGame::enable)
    inGame.onDisabled(postGame::enable)
    postGame.onDisabled(preGame::enable)
}
