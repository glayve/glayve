package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.inventory.material
import com.gitlab.glayve.network.*
import com.gitlab.glayve.util.Location
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import kotlin.experimental.and
import kotlin.experimental.or

fun Toggled.EntityPlayer(
    viewer: Channel, id: Int, uuid: UUID,

    location: PublishedObservable<Location>,
    health: PublishedObservable<Float>,

    hand: PublishedObservable<Stack?>,
    head: PublishedObservable<Stack?>,
    chest: PublishedObservable<Stack?>,
    legs: PublishedObservable<Stack?>,
    feet: PublishedObservable<Stack?>,

    //todo replace with watcher
    sprinting: PublishedObservable<Boolean>,
    sneaking: PublishedObservable<Boolean>,
    burning: PublishedObservable<Boolean>,
    using: PublishedObservable<Boolean>,

    onSwing: Event<() -> (Unit)>
) {
    val flags = Published(0.toByte()).apply {
        using { this(if (it) last or 0x10 else last and 0x10.inv()) }
        sprinting { this(if (it) last or 0x08 else last and 0x08.inv()) }
        sneaking { this(if (it) last or 0x02 else last and 0x02.inv()) }
        burning { this(if (it) last or 0x01 else last and 0x01.inv()) }
    }
    val meta = mapOf(
        0 to Meta(0, flags.last), 1 to Meta(1, 0.toShort()),
        2 to Meta(4, ""), 3 to Meta(0, 1.toByte()),
        4 to Meta(0, 0.toByte()), 6 to Meta(3, 20.toFloat()),
        7 to Meta(2, 0), 8 to Meta(0, 0.toByte()),
        9 to Meta(0, 0.toByte()), 15 to Meta(0, 0.toByte()),
        10 to Meta(0, 0.toByte()), 16 to Meta(0, 0.toByte()),
        17 to Meta(3, 0.toFloat()), 18 to Meta(2, 0)
    )
    onEnabled { viewer.spawn(id, uuid, location.last, hand.last?.item?.material ?: 0, meta) }
    hand { viewer.equipment(id, 0, it ?: Stack.EMPTY) }
    head { viewer.equipment(id, 4, it ?: Stack.EMPTY) }
    chest { viewer.equipment(id, 3, it ?: Stack.EMPTY) }
    legs { viewer.equipment(id, 2, it ?: Stack.EMPTY) }
    feet { viewer.equipment(id, 1, it ?: Stack.EMPTY) }
    flags.onChanged { _, to -> viewer.metadata(id, mapOf(0 to Meta(0, to))) }
    location.onNext { from, to ->
        viewer.entityMove(id, from, to)
        viewer.headMove(id, to.yaw)
    }
    health.onNext { from, to ->
        if (to < from) {
            val pitch =
                (ThreadLocalRandom.current().nextFloat() - ThreadLocalRandom.current().nextFloat()) * 0.2f + 1.0f
            viewer.sound("game.neutral.hurt", location.last, 1f, pitch)
            viewer.animation(id, 1)
        }
    }
    onSwing { viewer.animation(id, 0) }
    onDisabled { viewer.despawn(id) }
}