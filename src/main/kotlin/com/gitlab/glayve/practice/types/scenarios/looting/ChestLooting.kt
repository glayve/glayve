package com.gitlab.glayve.practice.types.scenarios.looting

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Container
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.inventory.material
import com.gitlab.glayve.network.*
import com.gitlab.glayve.practice.*
import com.gitlab.glayve.practice.games.*
import com.gitlab.glayve.world.material
import kotlin.time.Duration
import kotlin.time.Duration.Companion.ZERO
import kotlin.time.Duration.Companion.seconds
import kotlin.time.TimeSource

// easy 7 good 3 bad, medium 5 good 5 bad, hard 3 good 7 bad
val chestGrabItems = listOf(264, 266, 388)
val badChestGrabItems = listOf(352, 30)
fun Togglable.ChestLooting(online: Adepts, players: Adepts, arena: Arena, good: () -> (Stack), bad: () -> (Stack)) {
    val spectators = online.filter { id, _ -> id !in players }
    Spectate(spectators, arena.spectators)
    Play(players, arena.red)
    CancelClicks(players)
    Freeze(players)
    val scores = MutableTable<Difficulty, Duration>()
    val left = Published(0)
    val clicked = Published(0)
    val difficulty = Published(EASY)
    val goods = difficulty.map {
        when (it) {
            MEDIUM -> 5; HARD -> 3; else -> 7
        }
    };
    val bads = goods.map { 10 - it }

    val click: (Adept, Slot) -> (Unit) = { player, slot ->
        val goodItem = chestGrabItems.contains(player.inventory[slot]!!.item.material.toInt())
        if (goodItem) clicked(clicked.last + 1)
        left(left.last + if (!goodItem) -1 else 1)
        player.inventory[slot] = Stack.EMPTY
    }

    fun fill(adept: Adept) {
        adept.inventory.clear(45..71)
        (45..71).shuffled().take(goods.last + bads.last).forEachIndexed { i, slot ->
            adept.inventory[slot.toSlot()] = if (i < goods.last) good() else bad()
        }
    }

    val preGame = phased {
        DifficultySelector(players, 36, EASY, MEDIUM, HARD)(difficulty)
        players.onEach { _, adept ->
            adept.board(3, goods.map { "Items Left: $RED$it Item(s)" })
        }
        onEnabled {
            clicked(0); left(0)
        }
    }

    val postGame = phased {
        players.onEach { _, adept -> adept.closeWindow(1) }
        onEnabled {
            players.send {
                title(
                    "${RED}${left.last}", "${GOLD}ms",
                    stay = 3.seconds, wax = .2.seconds
                )
            }
        }

        scheduler.after(3.seconds)() { this@phased.disable() }
    }

    val inGame = phased {
        var start = TimeSource.Monotonic.markNow()
        players.onEach { _, adept ->
            adept.board(3, left.map { "Items Left:$RED ${goods.last - it} Item(s)" })

            clicked {
                if (it == goods.last) {
                    val time = start.elapsedNow()
                    if (time < (scores[difficulty.last] ?: ZERO))
                        scores[difficulty.last] = time
                    fill(adept); postGame.enable(); disable()
                }
            }

            adept.onWindowClose {
                postGame.enable(); this@phased.disable()
            }
        }

        onEnabled { start = TimeSource.Monotonic.markNow() }
    }

    online.onEach { _, adept ->
        adept.board(0, just("${RED}${BOLD}Chest Practice"))
        adept.board(1, date())
        adept.board(2, just(" "))
        adept.board(4, difficulty.map { "${WHITE}Difficulty: ${it.name}" })
        adept.board(5, just(" "))
        adept.board(6, just("Scenario:$RED Chest Looting II"))
        adept.board(7, scores.watch(difficulty).map { "Personal Record: $RED${it ?: 0}ms" })
        adept.board(8, just(" "))
        adept.board(9, just(DOMAIN))
        adept.gamemode(Gamemode.ADVENTURE)
    }

    players.onEach { _, adept ->
        adept.inventory.onShiftClick { slot ->
            if (adept.inventory[slot]!!.count != 0.toByte() && slot != 36.toByte()
                && adept.inventory.type.last == Container.CHEST_3
            ) {
                if (preGame.enabled) preGame.disable()
                click(adept, slot);
            }; CONSUME
        }

        adept.location {
            if (it.y <= 72) adept.location(arena.red)
        }

        adept.onRightClick { interaction ->
            if (interaction.position != null && adept.blocks[interaction.position].material == 54.toShort()) {
                adept.inventory.title("""{"text": "Chest"}""")
                adept.inventory.type(Container.CHEST_3)
                fill(adept)
                CONSUME
            } else PASS
        }
    }

    this.onEnabled(preGame::enable)
    preGame.onDisabled(inGame::enable)
    postGame.onDisabled(preGame::enable)
}

