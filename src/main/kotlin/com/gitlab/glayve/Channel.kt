package com.gitlab.glayve

import com.github.exerosis.mynt.base.Address
import com.github.exerosis.mynt.base.Write
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.network.*
import com.gitlab.glayve.util.Location
import com.mojang.authlib.properties.Property
import java.util.*
import kotlin.experimental.or

interface Channel {
    val version: Version
    val address: Address
    val id: Id
    val name: String
    val uuid: UUID
    val properties: Array<Property>

    val onWindowClick: Event<(Byte, Slot, Int, Short, Byte, Stack) -> (Unit)>
    val onWindowSlot: Event<(Slot, Stack) -> (Unit)>
    val onWindowDrop: Event<(Slot, Int) -> (Unit)>
    val onWindowConfirm: Event<(Byte, Short, Bool) -> (Unit)>
    val onWindowClose: Event<(Byte) -> Unit>

    val location: PublishedObservable<Location>
    val selected: PublishedObservable<Slot>

    val onTab: Event<(String) -> (Unit)>
    val onChat: Event<(String) -> (Unit)>

    val onLeftClick: Event<Togglable.(Interaction) -> (Bool)>
    val onRightClick: Event<Togglable.(Interaction) -> (Bool)>
    val digging: PublishedObservable<Bool>
    val using: PublishedObservable<Bool>

    val onRelease: Event<() -> (Unit)>
    val onSwing: Event<() -> (Unit)>
    val sneaking: PublishedObservable<Bool>
    val sprinting: PublishSubject<Bool>

    val burning: PublishSubject<Bool>
    val health: PublishSubject<Float>
    val shield: PublishSubject<Float>
    val saturation: PublishSubject<Float>
    val gamemode: PublishSubject<Gamemode>
    val difficulty: PublishSubject<Byte>
    val food: PublishSubject<Int>

    fun send(id: Int, packet: suspend (Write).() -> (Unit))
    fun kick(message: String)
}

typealias Channels = Mutated<Id, Channel>

fun Channel.abilities(
    invulnerable: Bool, flying: Bool, creative: Bool,
    fly: Float, move: Float
) {
    var flags = 0.toByte()
    if (invulnerable) flags = flags or 0x1
    if (flying) flags = flags or 0x2
    if (creative) flags = flags or 0x8
    if (fly > 0) flags = flags or 0x4
    send(OUT_ABILITIES) {
        byte(flags); float(fly); float(move)
    }
}

fun Channel.send(message: Any, color: Any = "white", action: Bool = false)
    = chat(message, color, action)


