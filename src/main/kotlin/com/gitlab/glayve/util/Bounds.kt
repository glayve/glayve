package com.gitlab.glayve.util

import com.gitlab.glayve.anticheat.utils.math.ceilToInt
import com.gitlab.glayve.anticheat.utils.math.floorToInt
import com.gitlab.glayve.network.Position
import com.gitlab.glayve.world.Block
import com.gitlab.glayve.world.World
import kotlin.math.max
import kotlin.math.min

data class Bounds(val min: Velocity, val max: Velocity) {
    constructor(
        x1: Double, y1: Double, z1: Double,
        x2: Double, y2: Double, z2: Double
    ): this(
        Velocity(min(x1, x2), min(y1, y2), min(z1, z2)),
        Velocity(max(x1, x2), max(y1, y2), max(z1, z2))
    )

    // length
    val lengthX get() = max.x - min.x
    val lengthZ get() = max.z - min.z
    val height get() = max.y - min.y

    // member alias
    val minX get() = min.x
    val minY get() = min.y
    val minZ get() = min.z
    val maxX get() = max.x
    val maxY get() = max.y
    val maxZ get() = max.z

    fun mapToLocation(location: Location) = Bounds(
        min + location.toVelocity(),
        max + location.toVelocity()
    )

    fun mapToPosition(position: Position) = Bounds(
        min + position.toVelocity(),
        max + position.toVelocity()
    )

    companion object {
        val NONE = Bounds(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        val FULL = Bounds(0.0, 0.0, 0.0, 1.0, 1.0, 1.0)

        fun ofEntity(location: Location, width: Double, height: Double): Bounds {
            require(width > 0)
            require(height > 0)

            val widthPart = width / 2.0

            return Bounds(
                location.x - widthPart,
                location.y,
                location.z - widthPart,
                location.x + widthPart,
                location.y + height,
                location.z + widthPart
            )
        }
    }
}
operator fun Bounds.contains(point: Velocity) =
    point.x in min.x..max.x &&
            point.y in min.y..max.y &&
            point.z in min.z..max.z
fun Bounds.intersects(other: Bounds): Boolean {
    return other.max.x > this.min.x &&
            other.min.x < this.max.x &&
            other.max.y > this.min.y &&
            other.min.y < this.max.y &&
            other.max.z > this.min.z &&
            other.min.z < this.max.z
}
operator fun Bounds.plus(amount: Velocity)
        = Bounds(min + amount, max + amount)
fun Bounds.expand(amount: Velocity)
        = Bounds(min - amount, max + amount)
fun Bounds.at(location: Velocity)
        = Bounds(max + location, min + location)
fun Bounds.at(location: Position)
        = at(location.toVelocity())
fun Bounds.ensureValid() {
    require(min.x <= max.x)
    require(min.y <= max.y)
    require(min.z <= max.z)
}

fun Bounds.rayTrace(start: Vector, direction: Vector, maxDistance: Double): RayTraceResult? {
    if (maxDistance < 0.0) return null

    // ray start:
    val startX = start.x
    val startY = start.y
    val startZ = start.z

    // ray direction:
    val dir: Vector = direction.normalizeZeros().normalize()
    val dirX = dir.x
    val dirY = dir.y
    val dirZ = dir.z

    // saving a few divisions below:
    // Note: If one of the direction vector components is 0.0, these
    // divisions result in infinity. But this is not a problem.
    val divX = 1.0 / dirX
    val divY = 1.0 / dirY
    val divZ = 1.0 / dirZ
    var tMin: Double
    var tMax: Double

    // intersections with x planes:
    if (dirX >= 0.0) {
        tMin = (min.x - startX) * divX
        tMax = (max.x - startX) * divX
    } else {
        tMin = (max.x - startX) * divX
        tMax = (min.x - startX) * divX
    }

    // intersections with y planes:
    val tyMin: Double
    val tyMax: Double
    if (dirY >= 0.0) {
        tyMin = (min.y - startY) * divY
        tyMax = (max.y - startY) * divY
    } else {
        tyMin = (max.y - startY) * divY
        tyMax = (min.y - startY) * divY
    }
    if (tMin > tyMax || tMax < tyMin) {
        return null
    }
    if (tyMin > tMin) {
        tMin = tyMin
    }
    if (tyMax < tMax) {
        tMax = tyMax
    }

    // intersections with z planes:
    val tzMin: Double
    val tzMax: Double
    if (dirZ >= 0.0) {
        tzMin = (min.z - startZ) * divZ
        tzMax = (max.z - startZ) * divZ
    } else {
        tzMin = (max.z - startZ) * divZ
        tzMax = (min.z - startZ) * divZ
    }
    if (tMin > tzMax || tMax < tzMin) {
        return null
    }
    if (tzMin > tMin) {
        tMin = tzMin
    }
    if (tzMax < tMax) {
        tMax = tzMax
    }

    // intersections are behind the start:
    if (tMax < 0.0) return null
    // intersections are to far away:
    if (tMin > maxDistance) {
        return null
    }

    // find the closest intersection:
    val t: Double = if (tMin < 0.0) {
        tMax
    } else {
        tMin
    }
    // reusing the newly created direction vector for the hit position:
    val hitPosition = dir.times(t).plus(start)
    return RayTraceResult(hitPosition)
}

data class RayTraceResult(
    val hitPosition: Vector
)

fun Bounds.expandDirectional(dirX: Double, dirY: Double, dirZ: Double): Bounds {
    val negativeX = if (dirX < 0.0) -dirX  else 0.0
    val negativeY = if (dirY < 0.0) -dirY else 0.0
    val negativeZ = if (dirZ < 0.0) -dirZ else 0.0
    val positiveX = if (dirX > 0.0) dirX else 0.0
    val positiveY = if (dirY > 0.0) dirY else 0.0
    val positiveZ = if (dirZ > 0.0) dirZ else 0.0

    val newMinX = min.x - negativeX
    val newMinY = min.y - negativeY
    val newMinZ = min.z - negativeZ
    val newMaxX = max.x + positiveX
    val newMaxY = max.y + positiveY
    val newMaxZ = max.z + positiveZ

    return Bounds(newMinX, newMinY, newMinZ, newMaxX, newMaxY, newMaxZ).apply {
        ensureValid()
    }
}

fun Bounds.blocksInside(world: World): List<Pair<Block, Position>> {
    val i: Int = floorToInt(minX)
    val j: Int = ceilToInt(maxX)
    val k: Int = floorToInt(minY)
    val l: Int = ceilToInt(maxY)
    val m: Int = floorToInt(minZ)
    val n: Int = ceilToInt(maxZ)
    val blocks = mutableListOf<Pair<Block, Position>>()
    for (p in i until j) {
        for (q in k until l) {
            for (r in m until n) {
                blocks += world[Position(p, q, r)] to Position(p, q, r)
            }
        }
    }
    return blocks
}