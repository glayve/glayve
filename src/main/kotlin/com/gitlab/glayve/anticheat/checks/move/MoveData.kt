package com.gitlab.glayve.anticheat.checks.move

import com.gitlab.glayve.anticheat.utils.math.Vector2D
import com.gitlab.glayve.util.Location
import com.gitlab.glayve.util.distance
import com.gitlab.glayve.util.distanceHorizontal

data class MoveData(
    val from: Location,
    val to: Location,
    val speedAttribute: Double,
    var tags: MutableSet<String> = mutableSetOf()
){
    val deltaY: Double get() = to.y - from.y
    val distance: Double get() = to.distance(from)
    val distanceHorizontal get() = to.distanceHorizontal(from)

    // Prediction Data (vert+hor)
    // Need to be set after analysis done
    var predictionNext: Vector2D = Vector2D.Zero

    companion object {
        fun build(
            from: Location,
            to: Location,
            speedAttribute: Double
        ) = MoveData(
            from = from,
            to = to,
            speedAttribute = speedAttribute
        )
    }
}

object MoveTags {

}