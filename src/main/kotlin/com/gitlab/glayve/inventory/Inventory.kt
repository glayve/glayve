package com.gitlab.glayve.inventory

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.ARMOUR
import com.gitlab.glayve.Channel
import com.gitlab.glayve.inventory.Container.PLAYER
import com.gitlab.glayve.inventory.Stack.Companion.EMPTY
import com.gitlab.glayve.network.*
import com.google.common.primitives.UnsignedBytes.toInt
import kotlin.math.abs
import kotlin.math.min

enum class Container(val size: Int, val type: String) {
    PLAYER(45, "minecraft:player"),
    CHEST_1(9, "minecraft:container"),
    CHEST_2(18, "minecraft:container"),
    CHEST_3(27, "minecraft:container"),
    CHEST_4(36, "minecraft:container"),
    CHEST_5(45, "minecraft:container"),
    CHEST_6(54, "minecraft:container")
}

//TODO figure out how to make this fuck all the way off... or at least replace it with an array or something.
//also did we factor in the non armour wearables... eg skulls or jack o'lantern.
fun Int.armourSlot(): Byte = when (this) {
    301, 305, 309, 313, 317 -> 8
    300, 304, 308, 312, 316 -> 7
    299, 303, 307, 311, 315 -> 6
    298, 302, 306, 310, 314 -> 5
    else -> 0
}.toByte()

interface Inventory : Mutable<Slot, Stack> {
    val cursor: PublishedObservable<Stack>
    val type: PublishSubject<Container>
    val title: PublishSubject<String>
    val using: PublishSubject<Bool>
    val onDrop: Event<(Slot, Stack) -> (Bool)>
    val onSwap: Event<(Slot, Slot) -> (Bool)>
    val onClick: Event<(Slot) -> (Bool)>
    val onShiftClick: Event<(Slot) -> (Bool)>
    val onDrag: Event<(Iterable<Slot>, Int) -> (Bool)>
    val onCollect: Event<(Iterable<Slot>, Int) -> (Bool)>
    fun clear(range: Iterable<Int> = 0 until type.last.size)
}

//fun Inventory.add(stack: Stack): Int {
//    var left = stack.count.toInt()
//    for (i in ((36..44) + (9..35))) {
//        val current = this[i.toSlot()]!!
//        if (current.same(stack))  {
//            val space = current.item.max - current.count
//            this[i.toSlot()] = current.add(left)
//            left -= space; if (left <= 0) return 0
//        }
//    }
//    for (i in ((36..44) + (9..35))) {
//        val current = this[i.toSlot()]!!
//        if (current.isEmpty())  {
//            val amount = min(stack.item.max.toInt(), left)
//            this[i.toSlot()] = stack.take(amount)
//            left -= amount; if (left <= 0) return 0
//        }
//    }
//    return left
//}
//fun Inventory.remove(stack: Stack): Int {
//    var amount = stack.count.toInt()
//    for (i in ((36..44) + (9..35))) {
//        val current = this[i.toSlot()]!!
//        if (current.same(stack)) {
//            this[i.toSlot()] = current.add(-amount)
//            amount -= current.count
//            if (amount <= 0) return 0
//        }
//    }; return amount
//}
fun Inventory.add(stack: Stack): Int {
    var left = stack.count.toInt()
    for (i in ((36..44) + (9..35))) {
        val current = this[i.toSlot()]!!
        if (current.same(stack)) {
            val space = current.item.max - current.count
            if (current == set(i.toSlot(), current, current.add(left))) {
                left -= space; if (left <= 0) return 0
            }
        }
    }
    for (i in ((36..44) + (9..35))) {
        val current = this[i.toSlot()]!!
        if (current.isEmpty())  {
            val amount = min(stack.item.max.toInt(), left)
            if (current == set(i.toSlot(), current, stack.take(amount))) {
                left -= amount; if (left <= 0) return 0
            }
        }
    }
    return left
}
fun Inventory.remove(stack: Stack): Int {
    var amount = stack.count.toInt()
    for (i in ((36..44) + (9..35))) {
        val current = this[i.toSlot()]!!
        if (current.same(stack)) {
            if (current == set(i.toSlot(), current, current.add(-amount))) {
                amount -= current.count
                if (amount <= 0) return 0
            }
        }
    }; return amount
}

fun Toggled.Inventory(channel: Channel): Inventory {
    fun send(slot: Slot, stack: Stack) {
        when {
            slot < 0 -> channel.setSlot(-1, -1, stack)
            slot <= 44 -> channel.setSlot(0, slot, stack)
            else -> channel.setSlot(1, (slot - 45).toByte(), stack)
        }
    }

    val inventory = object : Inventory {
        val items = Array(256) { EMPTY }

        override val type = Published(PLAYER)
        override val title = Published("""{"text": ""}""")
        override val onChanged = TreeEvent<(Slot, Stack?, Stack?) -> (Unit)>()
        override val onSwap = TreeEvent<(Slot, Slot) -> (Bool)>()
        override val onDrop = TreeEvent<(Slot, Stack) -> (Bool)>()
        override val onDrag = TreeEvent<(Iterable<Slot>, Int) -> (Bool)>()
        override val onCollect = TreeEvent<(Iterable<Slot>, Int) -> (Bool)>()
        override fun clear(range: Iterable<Int>) = range.forEach { this[it] = EMPTY }

        override val onClick = TreeEvent<(Slot) -> (Bool)>()
        override val onShiftClick = TreeEvent<(Slot) -> Bool>()

        override val cursor = Published(EMPTY)
        override val using = Published(false)

        override val size = 256
        override val keys = (0 until size).enumerator().map { it.toByte() }
        override val values = (0 until size).enumerator().map { items[it] }

        override fun get(key: Slot): Stack {
            return items[toInt(key)]
        }

        override fun set(key: Slot, value: Stack?): Stack {
            val stack = if (value.isEmpty()) EMPTY else value
            return if (key >= 0) items[toInt(key)].also {
                items[toInt(key)] = stack
                onChanged(key, it, stack)
                if (key < 45 || type.last != PLAYER) send(key, stack)
            } else cursor.last.apply { cursor(stack); send(key, stack) }
        }
    }

    operator fun Array<Stack>.get(slot: Slot) = get(toInt(slot))
    operator fun Array<Stack>.set(slot: Slot, value: Stack?) = set(toInt(slot),
        if (value.isEmpty()) EMPTY else value
    )

    var cancelled = false
    var action = 0.toShort()
    val slots = HashSet<Slot>() //Consider better options.

    fun open(container: Container, title: String) {
        cancelled = false
        val stacks = Array(container.size) { EMPTY }
        for (i in stacks.indices)
            stacks[i] = inventory.items[45 + i]
        channel.openWindow(1, container, title)
        channel.windowItems(1, stacks.size.toShort(), *stacks)
    }

    inventory.type {
        if (it == PLAYER) {
            cancelled = false
            channel.closeWindow(1)
        } else open(it, inventory.title.last)
    }
    inventory.title.onChanged { _, to ->
        val current = inventory.type.last
        if (current != PLAYER) open(inventory.type.last, to)
    }

    channel.onWindowClose {
        val cursor = inventory.cursor.last
        if (cursor.item.material.toInt() > 0) {
            inventory.cursor(EMPTY)
            inventory.onDrop(-1, cursor)
        }
        for (i in 1..4) {
            val found = inventory.items[i]
            if (found.item.material > 0) inventory.onDrop(i.toByte(), found)
            inventory.items[i.toByte()] = EMPTY
            inventory.onChanged(i.toByte(), found, EMPTY)
        }
        inventory.type(PLAYER)
    }

    channel.onWindowSlot { slot, updated ->
        if (slot >= 0) inventory[slot] = updated
        else inventory.onDrop(-2, updated)
    }
    channel.onWindowClick { window, _slot, button, action1, mode, item ->
        val thread = Thread.currentThread()
        action = action1
        val size = inventory.type.last.size
        val slot = (if (window != 0.toByte()) {
            if (_slot < inventory.type.last.size) _slot + 45
            else (_slot - size) + 9
        } else _slot).toByte()

        if (!cancelled) {
            when (mode.toInt()) {
                0 -> {
                    val cursor = inventory.cursor.last
                    val clicked = inventory.items[slot]
                    inventory.onClick.forEach { if (it(slot)) cancelled = true }

                    if (!cancelled) {
                        if (slot in 1..4) {
                            send(0, EMPTY)
                        }

                        if (!cursor.isEmpty()) { //Dropping something
                            if (slot.toInt() == -2) {
                                val to = if (button == 0) cursor else cursor.take(1)
                                if (!inventory.onDrop(-2, to)) {
                                    inventory.cursor(cursor.take(cursor.count - to.count))
                                }

                            } else {
                                if (slot.toInt() in 5..8 && slot != cursor.item.material.toInt().armourSlot())
                                    return@onWindowClick
                                val dropping = if (button == 0) cursor.count else 1
                                val updated = if (cursor.same(clicked)) {
                                    val total = clicked.count + dropping
                                    val max = cursor.item.max
                                    if (total > max) {
                                        inventory.cursor(cursor.take(total - max))
                                        clicked.take(max)
                                    } else {
                                        inventory.cursor(cursor.take(cursor.count - dropping))
                                        clicked.take(total)
                                    }
                                } else {
                                    if (clicked.isEmpty()) {
                                        inventory.cursor(cursor.take(cursor.count - dropping))
                                        cursor.take(dropping)
                                    } else {
                                        inventory.cursor(clicked)
                                        cursor
                                    }
                                }
                                inventory.items[slot] = updated
                                inventory.onChanged(slot, clicked, updated)
                            }
                        } else { //picking something up
                            val updated = if (button != 0) clicked.take(clicked.count / 2) else EMPTY
                            val armour = clicked.item.material.toInt().armourSlot().toInt() != 0
                            val updatedCursor =
                                if (button != 0 && !armour) updated.take(clicked.count - updated.count) else clicked
                            inventory.cursor(updatedCursor)
                            inventory.items[slot] = updated
                            inventory.onChanged(slot, clicked, updated)
                        }
                    }
                }
                1 -> {
                    inventory.onShiftClick.forEach { if (it(slot)) cancelled = true }
                    if (!cancelled) {
                        val clicked = inventory.items[slot]
                        val armourSlot = clicked.item.material.toInt().armourSlot()

                        if (armourSlot.toInt() != 0 && slot in 5..8) {
                            for (i in 9..44) {
                                val found = inventory.items[i]
                                if (found.item.material.toInt() == 0) {
                                    inventory.items[i.toByte()] = clicked
                                    inventory.items[slot] = EMPTY
                                    inventory.onChanged(i.toByte(), found, clicked)
                                    return@onWindowClick
                                }
                            }
                        } else if (ARMOUR[inventory.items[armourSlot].item.material.toInt()] != 0.toByte()) {
                            val assign = inventory.items[armourSlot]
                            if (assign.item.material.toInt() == 0) {
                                inventory.items[armourSlot] = clicked
                                inventory.items[slot] = EMPTY
                                return@onWindowClick
                            }
                        }

                        var amount = clicked.count.toInt()
                        var range = if (window.toInt() == 0) {
                            if (slot.toInt() in 36..44) 9..35 else 36..44
                        } else {
                            if (slot.toInt() >= 45) 44 downTo 9 else 45..(45 + inventory.type.last.size)
                        }
                        if (slot.toInt() in 1..4) range = 9..44


                        for (i in range) {
                            val found = inventory.items[i.toByte()]
                            if (found.same(clicked) && amount != 0) {
                                if (found.count + amount <= clicked.item.max) {
                                    inventory.items[i.toByte()] = found.take(found.count + amount)
                                    amount = 0
                                } else {
                                    inventory.items[i.toByte()] = found.take(64)
                                    amount = found.count + amount - 64
                                }
                            }
                        }

                        for (i in range) {
                            if (amount != 0) {
                                val found = inventory.items[i.toByte()]
                                if (found.item.material.toInt() == 0) {
                                    val updated = clicked.take(amount)
                                    inventory.items[i.toByte()] = updated
                                    inventory.onChanged(i.toByte(), found, updated)
                                    amount = 0
                                }
                            }
                        }
                        inventory.items[slot] = clicked.take(amount)
                    }
                }
                2 -> {
                    val to = (button + 36).toByte()
                    inventory.onSwap.forEach { if (it(slot, to)) cancelled = true }
                    if (!cancelled) {
                        inventory[to] = inventory.set(slot, inventory[to])
                    }
                }
                4 -> {
                    if (slot in 0 until inventory.size) {
                        val current = inventory.items[slot]
                        val amount = if (button == 0) 1 else current.count
                        val dropping = current.take(amount)

                        inventory.onDrop.forEach { if (it(slot, dropping)) cancelled = true }
                        if (!cancelled) {
                            val updated = if (button == 0) current.take(current.count - 1) else EMPTY
                            inventory.items[slot] = updated
                            inventory.onChanged(slot, current, updated)
                        }
                    } else inventory.onDrop(-2, EMPTY)
                }
                5 -> {
                    if (!cancelled) {
                        when (button) {
                            0, 4 -> slots.clear()
                            1, 5 -> slots.add(slot)
                            2, 6 -> {
                                val cursor = inventory.cursor.last
                                val amount = if (button == 2) cursor.count / slots.size else 1
                                inventory.onDrag.forEach { if (it(slots, amount)) cancelled = true }
                                if (!cancelled) {
                                    val painted = cursor.take(amount)
                                    var difference = 0
                                    slots.forEach {
                                        val current = inventory.items[it]
                                        val updated = if (!current.same(cursor)) painted
                                        else {
                                            val total = current.count + amount;
                                            val max = cursor.item.max.toInt()
                                            cursor.take(
                                                if (total > max) {
                                                    difference += amount - (max - current.count); max
                                                } else total
                                            )
                                        }
                                        inventory.items[it] = updated
                                        inventory.onChanged(it, current, updated)
                                    }
                                    inventory.cursor(cursor.take(cursor.count - ((amount * slots.size) - difference)))
                                }
                            }
                        }
                    }
                }
                6 -> {
                    slots.clear()
                    val cursor = inventory.cursor.last
                    val max = cursor.item.max.toInt()
                    var amount = max - cursor.count

                    for (i in (0 until inventory.size)) {
                        val current = inventory.items[i]
                        if (current.same(cursor)) {
                            slots.add(i.toByte())
                            amount -= current.count
                            if (amount <= 0) break
                        }
                    }

                    inventory.onDrag.forEach { if (it(slots, max - amount)) cancelled = true }
                    if (!cancelled) {
                        amount = max - cursor.count
                        slots.forEach {
                            val from = inventory.items[it]
                            amount -= from.count
                            val to = if (amount >= 0) EMPTY else from.take(abs(amount))
                            inventory.items[it] = to
                            inventory.onChanged(it, from, to)
                        }
                        inventory.cursor(cursor.take(if (amount < 1) max else (max - amount)))
                    }
                }
            }
        }

        if (cancelled) {
            channel.windowConfirm(window, action, false)
            send(slot, inventory.items[slot])
            send(-1, inventory.cursor.last)
            if (mode.toInt() != 0 && mode.toInt() != 3 && mode.toInt() != 4) {
                val range = if (inventory.type.last == PLAYER) 0..44 else 0..(44 + inventory.type.last.size)
                for (i in range) send(i.toByte(), inventory.items[i])
            }
        }

        if (thread != Thread.currentThread()) println("WARNING: Inventory between threads!")
    }
    channel.onWindowConfirm { _, current, accepted ->
        if (cancelled && action == current && cancelled == accepted)
            cancelled = false
    }
    channel.onWindowDrop { slot, status ->
        //FIXME fire inventory drop event from here.
    }
    inventory.onChanged { slot, _, _ -> if (slot in 1..4) send(0, Stack.EMPTY) }

    return inventory
}

operator fun Inventory.get(key: Int) = get(key.toByte())!!
operator fun Inventory.set(key: Int, stack: Stack?) = set(key.toByte(), stack)

