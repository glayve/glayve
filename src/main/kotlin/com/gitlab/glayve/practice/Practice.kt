package com.gitlab.glayve.practice

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.network.Id
import com.gitlab.glayve.network.id
import com.gitlab.glayve.practice.games.Duels
import com.gitlab.glayve.practice.games.Scenarios
import com.gitlab.glayve.practice.games.minus
import com.gitlab.glayve.util.Location
import com.gitlab.glayve.util.minus
import com.gitlab.glayve.util.setDirection
import com.gitlab.glayve.util.toVelocity
import com.gitlab.glayve.world.HashWorld
import com.gitlab.glayve.world.LoadedWorld
import com.gitlab.glayve.world.chunks
import com.gitlab.glayve.world.types
import java.nio.file.Paths

//val hubSpawn = Location(88.0, 60.0, 87.0, 0f, 0f, false)
val hubSpawn = Location(88.0, 65.0, 87.0, 0f, 0f, false)
val hubRegion by lazy { LoadedWorld(chunks(Paths.get("hub.mca"), hubSpawn, 15), types) }

fun Toggled.Practice(online: Channels) {
    TabList(online)
    val playing = MutableTable<Id, Togglable>()

    val hub = transient {
        val players = online and playing.group(this)
        playing.onEach { id, game ->
            game.onDisabled {
                playing[id] = this@transient
            }
        }
        val world = HashWorld(hubRegion)
        val hub = Adepts(players, world, hubSpawn)

        Scenarios(online, hub, playing)
        Duels(online, hub, playing)
        GameNPC(hub, Location(96.5, 58.0, 106.5), "${GREEN}${BOLD}Survival") {
            _, adept -> adept.send("Coming soon...")
        }

        (online and playing).onEach { id, adept ->
            adept.handle("/hub") { action { playing[id] = this@transient } }
        }

        hub.onEach { id, adept ->
            onEnabled {
                adept.location(hubSpawn)
                adept.gamemode(Gamemode.ADVENTURE)
            }
            adept.location { if (it.y < 40) adept.location(hubSpawn) }
            adept.handle("/spec") {
                val target = player((online and playing) - id, "That player is not online and in a game.")
                action {
                    playing[adept.id] = playing[target.id]
                    adept.send("§7You are now spectating §c${target.name}§7.")
                }
            }
        }
        hub.show(hub)
    }
    hub.enable()

    online.onAdded { id, _ ->  playing[id] = hub }
    online.onRemoved { id, _ -> playing[id] = null }

    online.onEach { _, channel ->
        channel.handle("/kb") { action {
            when (string()) {
                "h" -> HORIZONTAL = real("invalid")
                "v" -> VERTICAL = real("invalid")
                "ev" -> EXTRA_VERTICAL = real("invalid")
                "eh" -> EXTRA_HORIZONTAL = real("invalid")
            }
            channel.send("Current KB:", "red")
            channel.send("Horizontal: $HORIZONTAL", "blue")
            channel.send("Vertical: $VERTICAL", "blue")
            channel.send("Extra Vertical: $EXTRA_VERTICAL", "blue")
            channel.send("Extra Horizontal: $EXTRA_HORIZONTAL", "blue")
        } }
        channel.onChat { if (!it.startsWith("/"))
            online.send("§d${channel.name}§7: §f$it")
        }
    }
}
fun Toggled.GameNPC(
    adepts: Adepts, stand: Location,
    name: String, action: (Id, Adept) -> (Unit)
) = adepts.onEach { id, adept ->
    NPC(adept, name, adept.location.map {
        stand.setDirection(it.toVelocity() - stand.toVelocity())
    }, adept.properties, onRightClick = { action(id, adept) })
}

fun displayName(name: Any) = mapOf("display" to mapOf("Name" to name.toString()))
fun lore(name: Any, lore: Array<String>) = mapOf("Name" to name.toString(), "Lore" to lore)

