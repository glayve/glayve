package com.gitlab.glayve.network

import com.github.exerosis.mynt.base.Read
import com.github.exerosis.mynt.base.Write
import java.io.DataInputStream
import java.nio.ByteBuffer
import java.nio.channels.SeekableByteChannel

fun SeekableByteChannel.toRead() = object : Read {
    override suspend fun skip(
        amount: Int
    ) { position(this@toRead.position() + amount) }
    override suspend fun buffer(
        buffer: ByteBuffer
    ) = buffer.apply {
        limit(buffer.position() + buffer.remaining())
        read(buffer)
    }
    override suspend fun bytes(
        bytes: ByteArray,
        amount: Int,
        offset: Int
    ) = buffer(ByteBuffer.wrap(bytes).position(offset) as ByteBuffer).array()

    override suspend fun byte() =  buffer(ByteBuffer.allocate(1)).get(0)
    override suspend fun short() = buffer(ByteBuffer.allocate(2)).getShort(0)
    override suspend fun int() = buffer(ByteBuffer.allocate(4)).getInt(0)
    override suspend fun float() = buffer(ByteBuffer.allocate(4)).getFloat(0)
    override suspend fun long() = buffer(ByteBuffer.allocate(8)).getLong(0)
    override suspend fun double() = buffer(ByteBuffer.allocate(8)).getDouble(0)
}
fun DataInputStream.toRead() = object : Read {
    override suspend fun skip(amount: Int) {
        skipBytes(amount)
    }
    override suspend fun buffer(buffer: ByteBuffer): ByteBuffer {
        read(buffer.array(), buffer.position(), buffer.remaining())
        return buffer
    }
    override suspend fun bytes(bytes: ByteArray, amount: Int, offset: Int): ByteArray {
        var required = amount
        while (required > 0)
            required -= read(bytes, offset + (amount - required), required)
        return bytes
    }

    override suspend fun byte() = readByte()
    override suspend fun short() = readShort()
    override suspend fun int() = readInt()
    override suspend fun float() = readFloat()
    override suspend fun long() = readLong()
    override suspend fun double() = readDouble()
}

inline fun Read.measure(block: Read.() -> (Unit)): Int {
    var count = 0; block(object : Read {
        override suspend fun buffer(buffer: ByteBuffer): ByteBuffer {
            count += buffer.remaining()
            return this@measure.buffer(buffer)
        }
        override suspend fun bytes(bytes: ByteArray, amount: Int, offset: Int): ByteArray {
            count += amount
            return this@measure.bytes(bytes, amount, offset)
        }
        override suspend fun skip(amount: Int) {
            count += amount; this@measure.skip(amount)
        }

        override suspend fun float(): Float {
            count += 4
            return this@measure.float()
        }
        override suspend fun double(): Double {
            count += 8
            return this@measure.double()
        }

        override suspend fun byte(): Byte {
            count += 1; return this@measure.byte()
        }
        override suspend fun short(): Short {
            count += 2; return this@measure.short()
        }
        override suspend fun int(): Int {
            count += 4; return this@measure.int()
        }
        override suspend fun long(): Long {
            count += 8; return this@measure.long()
        }
    }); return count
}
