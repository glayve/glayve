package com.gitlab.glayve.practice.games

import com.gitlab.glayve.practice.hubSpawn
import com.gitlab.glayve.util.Location
import com.gitlab.glayve.world.LoadedWorld
import com.gitlab.glayve.world.World
import com.gitlab.glayve.world.chunks
import com.gitlab.glayve.world.types
import java.nio.file.Paths


val redSpawn = Location(38.0, 55.0, 59.0, -90f, 0f, false)
val blueSpawn = Location(97.0, 55.0, 59.0, 90f, 0f, false)

val bridgesRegion by lazy { LoadedWorld(chunks(Paths.get("bridges.mca"), hubSpawn, 8), types) }
val ladderSpawn = Location(615.0, 25.0, -807.0, 0f, 0f, false)
val ladderRegion by lazy { LoadedWorld(chunks(Paths.get("ladders.mca"), ladderSpawn, 5), types) }

val oppositeBridging = Location(7545.0, 82.0, 6952.0, 0f, 0f, false)
val bridgingSpawn = Location(7545.0, 70.0, 6900.0, 0f, 0f, false)
val bridgingRegion by lazy { LoadedWorld(chunks(Paths.get("bridging.mca"), bridgingSpawn, 6), types) }

val duelsRegion by lazy { LoadedWorld(chunks(Paths.get("1v1.mca"), hubSpawn, 4), types) }
val duelsSpawn1 = Location(80.0, 67.0, 122.0, 0f, 0f, false)
val duelsSpawn2 = Location(80.0, 67.0, 42.0, 0f, 0f, false)

val chestSpawn = Location(615.0, 10.0, -793.0, 0f, 0f, false)
val chestRegion by lazy { LoadedWorld(chunks(Paths.get("chest.mca"), chestSpawn), types) }

val bridgeTest = Location(136.0, 65.0, 342.0, 0f, 0f, false)
val bridgeTestRegion by lazy { LoadedWorld(chunks(Paths.get("bridges1.mca"), bridgeTest, 10), types) }

val chestGrab = Location(5483.0, 76.0, 6332.0, 180f, 0f, false)
val chestGrabRegion by lazy { LoadedWorld(chunks(Paths.get("chestgrab.mca"), chestGrab, 5), types) }

val cpsArena = Location(5276.0, 70.0, 3020.0, 180f, 0f, false)
val cpsRegion by lazy { LoadedWorld(chunks(Paths.get("cps.mca"), cpsArena, 5), types) }

val lineBotRed = Location(2756.5, 70.0, 1119.5, 0f, 0f, false)
val botHopArena = Location(2756.5, 70.0, 1123.5, 0f, 0f, false)
val botHopRegion by lazy { LoadedWorld(chunks(Paths.get("bot.mca"), cpsArena, 6), types) }

val frenzySpawn = Location(20000.5, 70.0, 20000.5, 0f, 0f, false)
val frenzyRegion by lazy { LoadedWorld(chunks(Paths.get("frenzy.mca"), frenzySpawn, 10), types) }

val sumoRed = Location(7000.5, 40.0, 7005.5, 180f, 0f, false)
val sumoBlue = Location(7000.5, 40.0, 6995.5, 0f, 0f, false)
val sumoRegion by lazy { LoadedWorld(chunks(Paths.get("sumo.mca"), sumoRed, 10), types) }

val bridgesArenas = arrayOf(
    Arena(redSpawn, blueSpawn, redSpawn, bridgesRegion),
    Arena(bridgeTest, bridgeTest, bridgeTest, bridgeTestRegion)
)
val ladderArenas = arrayOf(
    Arena(ladderSpawn, null, ladderSpawn, ladderRegion)
)
val bridgingArenas = arrayOf(
    Arena(bridgingSpawn, oppositeBridging, bridgingSpawn, bridgingRegion)
)
val botArenas = arrayOf(
    Arena(bridgingSpawn.copy(y = 4.0, ground = true), null, bridgingSpawn, bridgingRegion)
)
val duelsArena = arrayOf(
    Arena(duelsSpawn1, duelsSpawn2, duelsSpawn1, duelsRegion)
)

val chestGrabArenas = arrayOf(
    Arena(chestGrab, chestGrab, chestGrab, chestGrabRegion)
)

val cpsArenas = arrayOf(
    Arena(cpsArena, cpsArena, cpsArena, cpsRegion)
)

val botHopArenas = arrayOf(
    Arena(botHopArena, botHopArena, botHopArena, botHopRegion)
)

val lineBotArenas = arrayOf(
    Arena(lineBotRed, botHopArena, botHopArena, botHopRegion)
)

val frenzyArenas = arrayOf(
    Arena(frenzySpawn, frenzySpawn, frenzySpawn, frenzyRegion)
)

val sumoArenas = arrayOf(
    Arena(sumoRed, sumoBlue, sumoRed, sumoRegion)
)

val reactionSpawn = Location(11185.5, 70.0, 8559.5, 0f, 0f, false)
val reactionRegion by lazy { LoadedWorld(chunks(Paths.get("reactions.mca"), reactionSpawn, 10), types) }
val reactionsArenas = arrayOf(
    Arena(reactionSpawn, reactionSpawn, reactionSpawn, reactionRegion)
)

data class Arena(val red: Location, val blue: Location?, val spectators: Location, val world: World)
