package com.gitlab.glayve.practice.types.scenarios

import DIAMOND_SWORD
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.anticheat.utils.eyes
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.items.Item
import com.gitlab.glayve.network.Id
import com.gitlab.glayve.network.Position
import com.gitlab.glayve.network.title
import com.gitlab.glayve.practice.*
import com.gitlab.glayve.practice.games.*
import com.gitlab.glayve.util.*
import com.gitlab.glayve.world.BLOCK_AIR
import com.gitlab.glayve.world.BLOCK_SKULL
import com.gitlab.glayve.world.MutableWorld
import kotlin.time.Duration.Companion.ZERO
import kotlin.time.Duration.Companion.seconds

private fun walls(top: Position, bottom: Position)
    = ArrayList<Position>().also {
        for (y in bottom.y..top.y)
            for (z in bottom.z..top.z)
                it += Position(bottom.x, y, z)
    }
private val EASY_WALLS = walls(
    Position(19998, 73, 20001),
    Position(19998, 71, 19999)
)
private val HARD_WALLS = walls(
    Position(19998, 74, 20002),
    Position(19998, 70, 19998)
)

fun Togglable.BlockFrenzy(world: MutableWorld, online: Adepts, players: Adepts, arena: Arena) {
    val spectators = online.filter { id, _ -> id !in players }
    spectators.show(players)

    val easyPos = arena.red.copy(x = arena.red.x + 2, y = arena.red.y + 1, yaw = 90f, pitch = -15f)
    val mediumPos = easyPos.copy(x = easyPos.x + 2)
    val hardPos = mediumPos.copy(x = mediumPos.x + 2)


    Play(players, easyPos)
    CancelClicks(online)

    val difficulty = Published(EASY)
    val selected = MutableHolder<Position>()
    val points = Published(0)
    val currentWall = difficulty.map {
        when (it) {
            EASY -> EASY_WALLS; else -> HARD_WALLS
        }
    }
    val viewers = MutableTable<Id, Adept>()

    selected.onEach { position, _ ->
        onDisabled { world[position] = BLOCK_AIR; selected.add(currentWall.last.filter { it !in selected }.random()) }
        onEnabled { world[position] = BLOCK_SKULL[5] }
    }

    val scores = MutableTable<Difficulty, Int>()
    Freeze(players)

    val preGame = phased {
        DifficultySelector(viewers, 44, EASY, MEDIUM, HARD)(difficulty)

        players.onEach { _, adept ->
            adept.board(3, just("Time Left:$RED 15 Second(s)"))
        }
        currentWall.last.shuffled().take(3).forEach(selected::add)
        onEnabled {
            points(0)
            selected.forEach { position, _ -> selected[position] = null }
        }
    }

    val postGame = phased {
        onEnabled {
            players.send {
                println(this.name)
                title(
                    "${RED}${points.last}", "${GOLD}Hits",
                    stay = 3.seconds, wax = .2.seconds
                )
            }
        }

        scheduler.after(3.seconds)() { this@phased.disable() }
    }

    val inGame = phased {
        players.onEach { _, adept -> adept.inventory[44] = Stack.EMPTY }

        val timer = scheduler.every(1.seconds, ZERO)
        timer {
            if (it >= 15) {
                postGame.enable(); disable()
            }
        }

        online.onEach { _, adept ->
            adept.board(3, timer.map {
                "Time Left:$RED ${15 - it} Second(s)"
            })
        }

        onDisabled {
            if (points.last > scores[difficulty.last]!!) {
                scores[difficulty.last] = scores[difficulty.last]!!.plus(points.last)
            }
        }
    }

    val restarter = Item(players, 399, 0, 1, displayName("Restart"), { adept, _ ->
        if (inGame.enabled) {
            adept.send("You have restarted.")
            inGame.disable()
            postGame.enable()
        }; CONSUME
    }) { _, _ -> PASS }

    val bounds = BLOCK_SKULL[5].bounds
    players.onEach { _, adept ->
        adept.inventory[36] = Stack(com.gitlab.glayve.inventory.Item(DIAMOND_SWORD, 0, 1, emptyMap()), 1)
        adept.inventory[40] = Stack(restarter, 1)
        // TODO use left click, add haste
        adept.onSwing {
            if (!postGame.enabled) selected.forEach { id, position ->
                val direction = adept.location.last.direction
                val from = adept.eyes.last.toVelocity()
                val hit = intersection(bounds.at(position), from, direction)
                if (hit < Double.POSITIVE_INFINITY) {
                    if (preGame.enabled) preGame.disable()
                    points(points.last + 1)
                    selected[id] = null
                }
            }; CONSUME
        }
    }

    online.onEach { _, adept ->
        adept.board(0, just("${RED}${BOLD}Aim Practice"))
        adept.board(1, date())
        adept.board(2, just(" "))
//        adept.board(3, just("Time Left:$RED 15 Second(s)"))
        adept.board(4, points.map { "Hits:$RED $it Head(s)" })
        adept.board(5, difficulty.map { "Difficulty: ${it.name}" })
        adept.board(6, just(" "))
        adept.board(7, just("Scenario:$RED Block Frenzy"))
        adept.board(8, scores.watch(difficulty).map { "Personal Best:$RED ${it ?: 0} Head(s)" })
        adept.board(9, just(" "))
        adept.board(10, just(DOMAIN))
        adept.gamemode(Gamemode.ADVENTURE)
    }


    this.onEnabled(preGame::enable)
    preGame.onDisabled(inGame::enable)
    postGame.onDisabled(preGame::enable)
}

