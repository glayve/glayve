package com.gitlab.glayve.practice.types.duels

import DIAMOND_SWORD
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.items.DefaultHit
import com.gitlab.glayve.items.ItemSword
import com.gitlab.glayve.items.SHARPNESS
import com.gitlab.glayve.network.TRUE
import com.gitlab.glayve.network.addEffect
import com.gitlab.glayve.practice.*
import com.gitlab.glayve.practice.games.Arena
import kotlin.time.Duration.Companion.seconds

fun Togglable.Boxing(online: Adepts, red: Adepts, blue: Adepts, arena: Arena) {
    val players = red + blue
    val spectators = online.filter { id, _ -> id !in players }
    Spectate(spectators, arena.spectators)
    Play(red, arena.red)
    Play(blue, arena.blue!!)
    online.onEach { id, adept ->
        adept.addEffect(id, Effect.DIG_DOWN.toByte(), -1, -1)
    }

    val redHits = Published(0)
    val blueHits = Published(0)
    CancelClicks(online)
    online.show(players)

    val sword = ItemSword(online, DIAMOND_SWORD, mapOf(
        "ench" to arrayOf(mapOf("id" to SHARPNESS, "lvl" to 1)),
        "Unbreakable" to TRUE, "HideFlags" to (1 shl 2)
    ), onHit = { _, _ -> -1f })
    DefaultHit(online) { adept, target ->
        if (!target.invulnerable.last) {
            if (adept in red && target in blue) redHits += 1
            else if (adept in blue && target in red) blueHits += 1
        }; 0f
    }

    val speed = online.Speed(1, Int.MAX_VALUE.seconds)
    val timer = scheduler.every(1.seconds)
    online.onEach { _, adept ->
        adept.board(0, just("${RED}${BOLD}Multiplayer"))
        adept.board(1, date())
        adept.board(2, SPACER)
        adept.board(3, timer.map { "${RED}Time: $WHITE${String.format("%02d:%02d", it / 60, it % 60)}" })
        adept.board(4, SPACER)
        adept.board(5, just("${RED}Total Hits:"))
        adept.board(6, redHits.map { "${red.values()}: $RED$it" })
        adept.board(7, blueHits.map { "${blue.values()}: $RED$it" })
        adept.board(8, SPACER)
        adept.board(9, just("Game:$RED Boxing Duel"))
        adept.board(10, just("Winstreak:$RED 0"))
        adept.board(11, SPACER)
        adept.board(12, just(DOMAIN))
    }

    val preGame = phased {
        val preTimer = scheduler.every(1.seconds)
        preTimer {
            val time = (5 - it)
            if (time == 0) disable()
        }; Freeze(players)
    }

    val inGame = phased {
        players.onEach { id, adept ->
            adept.inventory[36] = Stack(sword, 1)
            adept.effects += speed
        }
    }

    redHits { if (it == 100) disable() }
    blueHits { if (it == 100) disable() }
    this.onEnabled(preGame::enable)
    preGame.onDisabled(inGame::enable)
}