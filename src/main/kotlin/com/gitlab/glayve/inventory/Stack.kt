package com.gitlab.glayve.inventory


import com.gitlab.glayve.items.ITEM_AIR
import com.google.common.primitives.UnsignedBytes.*
import kotlin.contracts.contract

data class Stack(val item: Item, val count: Byte) {
    companion object { val EMPTY = Stack(ITEM_AIR, 0) }
}

fun Stack?.isEmpty(): Boolean {
    contract { returns(false) implies (this@isEmpty != null) }
    return this == null || toInt(count) < 1 || item.type == 0
}
fun Stack?.same(other: Stack?) = other.normalize().item == normalize().item
fun Stack?.normalize() = if (isEmpty()) Stack.EMPTY else this
fun Stack?.take(count: Int) = this?.copy(count = count.toByte()).normalize()
fun Stack?.take(count: Byte) = take(count.toInt())
fun Stack?.add(amount: Int) = add(amount.toByte())
fun Stack?.add(amount: Byte) = this?.copy(
    count = min(item.max, (count + amount).toByte())
).normalize()

//fun Stack.isConsumable() = when (type.toInt()) {
//    276, 283, 267, 272, 268, 261, 364 -> true
//    else -> false
//}
//
//fun Stack.damage() = when (type.toInt()) {
//    283, 268 -> 4f
//    272 -> 5f
//    267 -> 6f
//    276 -> 2f // normal is 7
//    else -> 1f
//}


