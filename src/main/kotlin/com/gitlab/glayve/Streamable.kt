package com.gitlab.glayve

import com.gitlab.ballysta.architecture.Toggled

typealias Streamer = Toggled.(Channel) -> (Unit)
interface Streamable {
    val streamTo: Streamer
}

