package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.network.Bool
import com.gitlab.glayve.network.Id
import com.gitlab.glayve.network.Position
import com.gitlab.glayve.util.Bounds
import com.gitlab.glayve.util.Location
import com.gitlab.glayve.util.Velocity
import com.gitlab.glayve.util.toPosition
import com.gitlab.glayve.world.World
import java.util.*
import kotlin.time.Duration

@Deprecated("This is just some terrible stuff don't use it for anything")
interface Physics {
    fun scheduleUpdate(position: Position, delay: Duration, block: () -> (Bool))
    fun rayTraceEntities(location: Location, direction: Velocity, block: (Id) -> (Bool))
    fun rayTraceBlocks(location: Location, direction: Velocity, block: (Position) -> (Bool))
    fun entitiesIn(bounds: Bounds, block: (Id) -> (Unit))
}

interface Entity {
    val id: Id
    val location: PublishedObservable<Location>
}


fun Toggled.Physics(world: World, entities: Mutated<Id, Entity>): Physics {
    return object : Physics {
        val entityChunks = HashMap<Int, BitSet>()
        val updates = MutableTable<Position, Pair<Duration, () -> (Bool)>>()
        init {
            updates.onEach { position, (duration, block) ->
                scheduler.after(duration)() {
                    updates[position] = null
                    if (block()) updates[position] = duration to block
                }
            }
        }
        override fun scheduleUpdate(position: Position, delay: Duration, block: () -> Bool) {
            updates[position] = delay to block
        }

        val Position.chnk get() //FIXME completely incorrect.
            = ((x shl 4) shr 8) and ((z shl 4) shr 4) and (z shl 4)
        override fun rayTraceEntities(location: Location, direction: Velocity, block: (Id) -> Bool) {
            entityChunks[location.toPosition().chnk]
        }

        override fun rayTraceBlocks(location: Location, direction: Velocity, block: (Position) -> Bool) {
            TODO("Not yet implemented")
        }

        override fun entitiesIn(bounds: Bounds, block: (Id) -> Unit) {
            TODO("Not yet implemented")
        }

    }
}
