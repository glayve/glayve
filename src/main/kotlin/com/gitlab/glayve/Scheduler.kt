package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit.*
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.DurationUnit


typealias Scheduler = ScheduledExecutorService

val Number.ticks get() = (toLong() * 50L).milliseconds
@Deprecated("outdated", ReplaceWith("inWholeTicks"))
val Duration.inTicks get() = toDouble(DurationUnit.MILLISECONDS) / 50
val Duration.inWholeTicks get() = inWholeMilliseconds / 50

val tick = 1.ticks

//FIXME think about making this published.
fun Scheduler.every(
    period: Duration,
    delay: Duration = period,
    interrupt: Boolean = true
): Observable<Int> = {
    var task: ScheduledFuture<*>? = null; var times = 0
    //TODO consider the implications of this.
    onDisabled { task?.cancel(interrupt); times = 0 }
    onEnabled { task = scheduleAtFixedRate({
        if (enabled) it(times++)
    }, delay.inWholeMilliseconds, period.inWholeMilliseconds, MILLISECONDS) }
}
fun Scheduler.after(duration: Duration): Event<() -> (Unit)> = {
    var task: ScheduledFuture<*>? = null
    onDisabled { task?.cancel(true) }
    onEnabled { task = schedule(
        { if (enabled) it() },
        duration.inWholeMilliseconds,
        MILLISECONDS
    ) }
}
