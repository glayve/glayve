package com.gitlab.glayve.practice

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.*
import com.gitlab.glayve.inventory.Container.CHEST_1
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.inventory.Stack.Companion.EMPTY
import com.gitlab.glayve.items.Item
import com.gitlab.glayve.network.*
import com.gitlab.glayve.util.Location
import com.gitlab.glayve.util.Velocity
import com.gitlab.glayve.util.plus
import com.gitlab.glayve.util.toPosition
import com.gitlab.glayve.world.BLOCK_AIR
import com.gitlab.glayve.world.MutableWorld
import com.gitlab.glayve.world.material
import com.mojang.authlib.properties.Property
import java.util.*
import java.util.concurrent.ThreadLocalRandom.current
import kotlin.time.Duration.Companion.seconds

fun Toggled.SlotPicker(
    viewers: Mutable<Id, Adept>, available: MutableList<Slot>
) = Menu(viewers, CHEST_1, just("Slot Selector")) { _, slots ->
    for (i in 0..8) {
        slots[i.toByte()] = {
            material(160)
            val slot = (i + 36).toByte()
            if (available.contains(slot)) meta(5) else meta(14)
            name("${GREEN}${(i + 1)}")
            onClick {
                if (available.size != 1 && available.remove(slot)) meta(COLORED_RED)
                else if (available.add(slot)) meta(COLORED_LIME); CONSUME
            }
        }
    }
}

fun Toggled.Fire(world: MutableWorld, viewers: Adepts, adepts: Mutated<Id, out Adept>) {
    adepts.onEach { _, adept ->
        adept.gamemode(Gamemode.SURVIVAL)
        adept.onLeftClick { interaction ->
            if (interaction.clicked != null) {
                if (world[interaction.clicked].material == 51.toShort()) {
                    world[interaction.clicked] = BLOCK_AIR
                    val clicked = interaction.clicked.toLocation()
                    val location = clicked.plus(Velocity(clicked.x + 0.5, clicked.y + 0.5, clicked.z + 0.5))

                    viewers.send {
                        sound(
                            "random.fizz",
                            location,
                            0.5f,
                            2.6f + (current().nextFloat() - current().nextFloat()) * 0.8f
                        )
                    }
                }
                CONSUME
            } else PASS
        }

        adept.burning {
            if (it) {
                transient {
                    scheduler.every(1.seconds)() { time ->
                        if (time == 8) {
                            adept.burning(false)
                            disable()
                        } else adept.damage(1f, knockbackVelocity(), direct = true)
                    }
                }
            }
        }
    }

    val burningTicks = MutableTable<Adept, Int>()
    scheduler.every(1.ticks)() {
        adepts.forEach { _, adept ->
            if (world[adept.location.last.toPosition()].material == 51.toShort()) {
                burningTicks[adept] = (burningTicks[adept] ?: 0) + 1
                if (burningTicks[adept] == 20) adept.burning(true)
                if (burningTicks[adept] == 1 || (burningTicks[adept] ?: 0) % 20 == 0) {
                    adept.damage(1f, knockbackVelocity(), direct = true)
                }
            } else burningTicks[adept] = 0
        }
    }
}

data class Difficulty(val name: String, val color: Int)
val EASY = Difficulty("${GREEN}Easy", COLORED_LIME)
val MEDIUM = Difficulty("${GOLD}Medium", COLORED_ORANGE)
val HARD = Difficulty("${RED}Hard", COLORED_RED)

fun Toggled.DifficultySelector(
    players: Adepts, slot: Slot = 36, vararg difficulties: Difficulty
) = Published(difficulties[0]).apply {
    val viewers = MutableTable<Id, Adept>()
    val switcher = Item(players, 347, 0, 1, displayName("Difficulty Switcher"), { adept, _ ->
        adept.inventory.clear(45..71)
        viewers[adept.id] = adept; CONSUME
    }) { _, _ -> PASS }
    Menu(viewers, CHEST_1, just("Difficulty")) { adept, slots ->
        difficulties.forEachIndexed { _, it ->
            val fixed = when(it) {EASY -> 2; MEDIUM -> 4; else -> 6; }.toSlot()
            slots[fixed] = {
                material(159); meta(it.color)
                name("${CHAT_COLORS[it.color]}${it.name}")
                onClick { disable(); this@apply(it); PASS }
            }
        }
    }
    players.onEach { _, adept ->
        map {
            if (it) Stack(switcher, 1) else EMPTY
        }(adept.inventory.at(slot))
    }
} as PublishedObservable<Difficulty>

fun Toggled.Freeze(adepts: Mutated<*, out Adept>) {
    adepts.onEach { _, adept ->
        //TODO consider saving previous state? eh?
        onEnabled { adept.movement(false) }
        onDisabled { adept.movement(true) }
    }
}
//TODO replace with something better eventually.
fun Toggled.Play(adepts: Mutated<*, out Adept>, location: Location) {
    adepts.onEach { _, adept ->
        adept.location(location)
        adept.gamemode(Gamemode.SURVIVAL)
        adept.health(20f)
        adept.food(20)
    }
}
fun Toggled.Spectate(adepts: Mutated<*, out Adept>, spawn: Location) {
    adepts.onEach { _, adept ->
        adept.location(spawn)
        adept.fly(DEFAULT_FLY)
        adept.gamemode(Gamemode.ADVENTURE)
        adept.health(20f)
        adept.food(20)
        adept.inventory.clear()
        adept.inventory.onDrop {_, _ -> false}
        adept.inventory.onClick { false }
        adept.inventory.onShiftClick { false }
        adept.inventory.onSwap { _, _ -> false }
        onDisabled { adept.fly(0f) }
    }
}

fun Toggled.CancelClicks(adepts: Mutated<*, out Adept>) {
    adepts.onEach { _, adept ->
        adept.inventory.onShiftClick { PASS }
        adept.inventory.onClick { PASS }
        adept.inventory.onDrop { _, _ -> PASS }
        adept.inventory.onSwap { _, _ -> PASS }
        adept.inventory.onDrag { _, _ -> PASS }
        adept.inventory.onCollect { _, _ -> PASS }
        adept.inventory.onClick { slot -> slot.toInt() == -2 }
    }
}


fun Toggled.NPC(
    channel: Channel, name: String,
    location: PublishedObservable<Location>,
    properties: Array<Property> = emptyArray(),
    onRightClick: (Int) -> (Unit) = {},
    onLeftClick: (Int) -> (Unit) = {}
) {
    val id = EID.getAndIncrement()
    val item = PlayerListItem(UUID.randomUUID(), name, properties)
    onEnabled { channel.playerListItems(0, item) }
    EntityPlayer(
        channel, id, item.uuid, location,
        just(20f), just(EMPTY),
        just(EMPTY), just(EMPTY),
        just(EMPTY), just(EMPTY),
        just(false), just(false),
        just(false), just(false)
    ){}
    onDisabled { channel.playerListItems(4, item) }
    channel.onRightClick {
        if (it.entity != id) PASS
        else { onRightClick(id); CONSUME }
    }
    channel.onLeftClick {
        if (it.entity != id) PASS
        else { onLeftClick(id); CONSUME }
    }
}



fun Toggled.TabList(channels: Channels) = channels.onEach { _, channel ->
    val item = PlayerListItem(channel.uuid, channel.name, channel.properties)
    onEnabled { channel.playerListItems(0, item) }
    onDisabled { channel.playerListItems(4, item) }
}

val Mutated<*, out Channel>.show get(): Toggled.(Mutated<*, out Adept>) -> (Unit) = { adepts ->
    onEach { _, channel -> adepts.onEach { _, adept ->
        if (channel.uuid != adept.uuid) EntityPlayer(
            channel, adept.id, adept.uuid, adept.location,
            adept.health, adept.inventory.watch(adept.selected),
            adept.inventory.watch(5), adept.inventory.watch(6),
            adept.inventory.watch(7), adept.inventory.watch(8),
            adept.sprinting, adept.sneaking, adept.burning,
            adept.using, adept.onSwing
        );
    } }
}