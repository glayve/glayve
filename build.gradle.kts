plugins {
    id("org.jetbrains.kotlin.jvm").version("1.6.0")
    id("com.github.johnrengelman.shadow").version("6.0.0")
    id("org.gradle.maven-publish")
}

group = "com.gitlab.glayve"
version = "1.0.0"

repositories {
    maven("https://repo1.maven.org/maven2/")
    maven("https://libraries.minecraft.net")
    mavenLocal();maven("https://jitpack.io")
}

dependencies {
    api(files("/Server/arch.jar"))
    api("com.github.exerosis:mynt:1.0.9")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0")
    implementation("com.mojang:authlib:1.5.21")
}

tasks.shadowJar {
    archiveFileName.set("${project.name}.jar")
    destinationDirectory.set(file("./Server"))
    manifest.attributes["Main-Class"] = "com.gitlab.glayve.MainKt"
}

tasks.build { dependsOn(tasks.shadowJar) }

tasks.compileKotlin {
    kotlinOptions.freeCompilerArgs = listOf(
        "-Xopt-in=kotlin.time.ExperimentalTime", "-Xinline-classes",
        "-Xopt-in=kotlin.contracts.ExperimentalContracts",
        "-Xopt-in=kotlin.ExperimentalUnsignedTypes",
        "-Xopt-in=kotlinx.coroutines.ExperimentalCoroutinesApi",
        "-Xopt-in=kotlinx.coroutines.DelicateCoroutinesApi"
    )
}