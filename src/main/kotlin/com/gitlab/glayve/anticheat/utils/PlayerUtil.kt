package com.gitlab.glayve.anticheat.utils

import com.gitlab.glayve.Adept
import com.gitlab.glayve.util.add

//replace this with something cleaner someday.
val Adept.eyeHeight get() = if(sneaking.last) 1.54 else 1.62
val Adept.eyes get() = location.map { it.add(y = if(sneaking.last) 1.54 else 1.62) }