package com.gitlab.glayve.world

import com.gitlab.glayve.util.Bounds

val blockFrictionFactorMap = FloatArray(65536){
    when(it) {
        // TODO: Implement all blocks
        else -> 0.6f
    }
}

val types: (Int) -> (Block) = { type ->
    val material = type ushr 4; val meta = type and 0xF
    when {
        type == 0 -> BLOCK_AIR
        material.toShort() == BLOCK_ADOBE[0].material -> {
            BLOCK_ADOBE[meta]
        }
        else ->  Block(material.toShort(), meta.toShort(), 0, Bounds.FULL, "")
    }
}