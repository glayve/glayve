package com.gitlab.glayve.anticheat.checks.combat

import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.onEach
import com.gitlab.glayve.Adepts
import com.gitlab.glayve.PASS
import com.gitlab.glayve.anticheat.CheckType
import com.gitlab.glayve.anticheat.flag
import com.gitlab.glayve.anticheat.utils.markNow
import com.gitlab.glayve.anticheat.utils.math.Buffer
import kotlin.time.TimeMark

private const val MaxCps = 20

fun Toggled.ClickCheck(players: Adepts) {
    players.onEach { _, player ->
        var lastSwing = markNow()
        val clickBuffer = Buffer<Click>(30)

        player.onLeftClick {
            if (it.entity != null) {
                val now = markNow()
                val elapsed = lastSwing.elapsedNow().inWholeMilliseconds
                lastSwing = now
                clickBuffer += Click(
                    elapsed = elapsed,
                    timeStamp = now
                )

                if (clickBuffer.isFull()) {
                    val cps = clickBuffer.count { click ->
                        click.timeStamp.elapsedNow().inWholeMilliseconds <= 1000L
                    }
                    if (cps > MaxCps) {
                        player.flag(
                            type = CheckType.CLICK,
                            message = "cps: $cps"
                        )
                    }
                }
            }
            PASS
        }
    }
}

data class Click(
    val elapsed: Long,
    val timeStamp: TimeMark
)