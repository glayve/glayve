package com.gitlab.glayve.anticheat.checks.net

import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.onChanged
import com.gitlab.ballysta.architecture.onEach
import com.gitlab.glayve.Adepts
import com.gitlab.glayve.anticheat.CheckType
import com.gitlab.glayve.anticheat.flag
import com.gitlab.glayve.anticheat.utils.formatToString
import com.gitlab.glayve.anticheat.utils.math.distanceOfAngles
import kotlin.math.abs

// Prevent player send weird rotation packet
fun Toggled.DerpCheck(adepts: Adepts){
    adepts.onEach { _, adept ->
        var prevRotationYaw = 0f
        var buffer = 0

        adept.location.onChanged { from, to ->
            val deltaYaw = distanceOfAngles(from.yaw, to.yaw)
            if(deltaYaw > 0f && (deltaYaw == prevRotationYaw || deltaYaw % 1f == 0f)){
                buffer = buffer.plus(1).coerceAtMost(10)
                if(buffer > 3){
                    adept.flag(
                        type = CheckType.PACKETS,
                        message = "spin",
                        data = listOf(
                            "buf" to buffer.toString(),
                            "deltaYaw" to deltaYaw.formatToString(),
                            "prevDelta" to prevRotationYaw.formatToString()
                        )
                    )
                }
            }else {
                buffer = buffer.minus(1).coerceAtLeast(0)
            }
            prevRotationYaw = deltaYaw
        }

        adept.location {
            // pitch derp
            if(abs(it.pitch) > 90f){
                adept.flag(
                    type = CheckType.PACKETS,
                    message = "derp",
                    vl = 10,
                    data = listOf(
                        "pitch" to it.pitch.toString()
                    )
                )
            }
        }
    }
}