package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.network.Position
import com.gitlab.glayve.network.despawn
import com.gitlab.glayve.network.spawnObject
import com.gitlab.glayve.network.velocity
import com.gitlab.glayve.util.*
import java.util.concurrent.ThreadLocalRandom.current
import kotlin.math.*


fun hookCast(location: Location): Velocity {
    val rotationYaw = location.yaw / 180 * PI
    val rotationPitch = location.pitch / 180 * PI
    var x = -sin(rotationYaw) * cos(rotationPitch) * 0.4f
    var z = cos(rotationYaw) * cos(rotationPitch) * 0.4f
    var y = -sin(rotationPitch) * 0.4f
    val yaw = 1.5f
    val pitch = 1f

    val dist = sqrt(x * x + y * y + z * z)
    x /= dist
    y /= dist
    z /= dist
    fun rand(): Double {
        val direction = if (current().nextBoolean()) -1 else 1
        val random = current().nextGaussian() * direction
        return random * 0.007499999832361937 * pitch.toDouble()
    }
    x += rand()
    y += rand()
    z += rand()
    x *= yaw.toDouble()
    y *= yaw.toDouble()
    z *= yaw.toDouble()

    return Velocity(x, y, z)
}

//TODO maybe replace with single method.
fun arrowVelocity(location: Location, draw: Float): Velocity {
    val yaw = location.yaw / 180 * PI
    val pitch = location.pitch / 180 * PI
    var x = -sin(yaw) * cos(pitch)
    var y = -sin(pitch)
    var z = cos(yaw) * cos(pitch)
    val dist = sqrt(x * x + y * y + z * z)
    x /= dist
    y /= dist
    z /= dist
    fun rand(): Double {
        val direction = if (current().nextBoolean()) -1 else 1
        val random = current().nextGaussian() * direction
        return random * 0.007499999832361937
    }
    x += rand()
    y += rand()
    z += rand()
    val velocity = draw * 1.5
    x *= velocity
    y *= velocity
    z *= velocity
    return Velocity(x, y, z)
}
fun splashVelocity(location: Location) : Velocity {
    val yaw = location.yaw.toRadians()
    val pitch = location.pitch.toRadians()
    val inaccuracy = -20.0
    val velocity = 0.5
    var x = -sin(yaw) * cos(pitch) * 0.4
    var y = -sin((location.pitch + inaccuracy).toRadians()) * 0.4
    var z = cos(yaw) * cos(pitch) * 0.4

    fun rand() = current().nextGaussian() * 0.007499999832361937 * 1f

    val dist = sqrt(x * x + y * y + z * z)
    x /= dist
    y /= dist
    z /= dist

    x += rand()
    y += rand()
    z += rand()

    x *= velocity
    y *= velocity
    z *= velocity

    return Velocity(x, y, z)
}
fun dropVelocity(location: Location) : Velocity {
    val yaw = location.yaw.toRadians()
    val pitch = location.pitch.toRadians()

    var x = (-sin(yaw) * cos(pitch) * 0.3f)
    var y = (-sin(pitch) * 0.3f) + 0.1f //fixed something here
    var z = (cos(yaw) * cos(pitch) * 0.3f)

    current().apply {
        val f3 = nextFloat() * PI * 2.0f
        val f2 = 0.02f * nextFloat()
        x += cos(f3) * f2
        y += (nextFloat() - nextFloat()) * 0.1f
        z += sin(f3) * f2
    }

    return Velocity(x, y, z)
}

fun Toggled.Projectile(
    scheduler: Scheduler, adepts: Adepts,
    id: Int, type: Byte, data: Int,
    origin: Location, force: Velocity,
    gravity: Double, drag: Double,
): Pair<PublishedObservable<Location>, PublishedObservable<Velocity>> {
    val velocity = Published(force).apply {
        //TODO should this run right away or after a delay?
        scheduler.every(tick)() {
            this(
                last.copy(
                    x = last.x * drag,
                    y = (last.y * drag) - gravity,
                    z = last.z * drag
                )
            )
        }
    }
    return Published(origin).also { location ->
        if (type != 60.toByte()) velocity.filter {
            it.length < 0.00001 || (it + -velocity.last).length > 0.02
        }() { adepts.send { velocity(id, it) } }
        var first = true
        velocity {
            if (!first) {
                val horizontal = sqrt(it.x * it.x + it.z * it.z)
                location(location.last.run {
                    copy(
                        x = x + it.x,
                        y = y + it.y,
                        z = z + it.z,
                        yaw = atan2(it.x, it.z).toDegrees().toFloat(),
                        pitch = atan2(it.x, horizontal).toDegrees().toFloat()
                    )
                })
            } else first = false
        }
        onEnabled { adepts.send { spawnObject(id, type, location.last, data, force) } }
        onDisabled { adepts.send { despawn(id) } }
    } to velocity
}

fun Toggled.ProjectileTracker(
    source: Adept, adepts: Adepts,
    location: PublishedObservable<Location>,
    onHit: (Position?, Adept?) -> (Unit)
) = location { to ->
    val from = location.last.toVelocity()
    val slope = to.toVelocity() - from
    if (to.y < 0 && enabled) onHit(to.toPosition().copy(y = 0), null)
    if (enabled) {
        for (block in location.last..to) {
            val bounds = source.blocks[block].bounds.at(block.toVelocity())
            val enter = intersection(bounds, from, slope.normalize())
            if (enter * enter <= slope.lengthSquared) onHit(block, null)
            if (!enabled) return@location
        }
        adepts.values.filter { it !== source }.forEach {
            val enter = intersection(it.bounds.last, from, slope.normalize())
            if (enter * enter <= slope.lengthSquared) onHit(null, it)
            if (!enabled) return@location
        }
    }
}

fun Toggled.PotionTracker(
    source: Adept, adepts: Adepts,
    location: PublishedObservable<Location>,
    onHit: (Position?, Adept?) -> (Unit)
) = location { to ->
    val from = location.last.toVelocity()
    val slope = to.toVelocity() - from
    if (to.y < 0 && enabled) onHit(to.toPosition().copy(y = 0), null)
    if (enabled) {
        for (block in location.last..to) {
            val bounds = source.blocks[block].bounds.at(block.toVelocity())
            val enter = intersection(bounds, from, slope.normalize())
            if (enter * enter <= slope.lengthSquared) onHit(block, null)
            if (!enabled) return@location
        }

        adepts.values.forEach {
            val enter = intersection(it.bounds.last, from, slope.normalize())
            if (enter * enter <= slope.lengthSquared) onHit(null, it)
            if (!enabled) return@location
        }
    }
}


//val SPLASH = 0.8f //1.52
//val ARROW = 0.6f //1.899999999

//arrow
//val from = location.last.toVelocity()
//val slope = to.toVelocity() - from
//for (other in players.values.filter { it !== player }) {
//    val bounds = other.bounds.last
//    val enter = intersection(bounds, from, slope.normalize())
//    if (enter * enter <= slope.lengthSquared) {
//        player.state(6, 0f)
//        disable()
//        //TODO deal damage and knockback.
//        return@location
//    }
//}

//item
//metadata(id, mapOf(10 to Meta(5, stack)))
//scheduler.every(tick)() {
//    if (it >= 10) for (player in players.values) {
//        val current = landed ?: location.last
//        // .425 is normal
//        if (player.location.last.distance(current) <= 1) {
//            val pitch = (current().run { nextFloat() - nextFloat() } * 0.7f + 1.0f)
//            players.send {
//                collect(id, player.id)
//                sound("random.pop", current, 0.2f, pitch * 2f)
//            }; onPickup(player); disable();  break
//        }
//    }
//}

//potion
//players.send("random.bow", location.last.add(y = 1.0), 0.5f, 0.5f)
//effect(2002, location.last.toPosition(), potion.toInt(), false)
//if (distance < 4) {
//    val power = (((1.0 - distance / 4.0) * 200) + 0.5).toInt()
//    if (power > 20) onHit(it, power)
//}