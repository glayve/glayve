package com.gitlab.glayve.anticheat.checks.combat

import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.onChanged
import com.gitlab.ballysta.architecture.onEach
import com.gitlab.ballysta.architecture.onNext
import com.gitlab.glayve.Adepts
import com.gitlab.glayve.CONSUME
import com.gitlab.glayve.PASS
import com.gitlab.glayve.anticheat.CheckType
import com.gitlab.glayve.anticheat.flag
import com.gitlab.glayve.anticheat.utils.isNearlyInt
import com.gitlab.glayve.anticheat.utils.markNow
import com.gitlab.glayve.anticheat.utils.math.Buffer
import com.gitlab.glayve.anticheat.utils.math.distanceOfAngles
import kotlin.math.roundToInt
import kotlin.time.TimeMark

// How many players can a player attack per second?
// TODO: Test on real environment to get the best value
private const val MaxSwitchAttackPerSecond = 3

fun Toggled.KillAuraCheck(adepts: Adepts) {
    PacketSequenceCheck(adepts)
    SensitivityCheck(adepts)
    MultiAuraCheck(adepts)
}

private fun Toggled.MultiAuraCheck(adepts: Adepts) {
    adepts.onEach { _, adept ->
        val attackBuffer = Buffer<Pair<TimeMark, Int>>(20)
        adept.onLeftClick { interaction ->
            var cancel = false
            interaction.entity?.let { adepts[it] }?.let {
                attackBuffer += markNow() to it.id
                val switchEntities = attackBuffer
                    .filter { pair ->
                        pair.first.elapsedNow().inWholeMilliseconds <= 1000L
                    }
                    .map { pair -> pair.second }
                    .distinct()
                    .size
                if (switchEntities >= MaxSwitchAttackPerSecond) {
                    adept.flag(
                        type = CheckType.KILLAURA,
                        message = "multi aura",
                        data = listOf(
                            "switch" to switchEntities.toString()
                        )
                    )
                    cancel = true
                }
            }
            if(!cancel) PASS else CONSUME
        }
    }
}

private fun Toggled.SensitivityCheck(adepts: Adepts) {
    fun findGcd(a: Float, b: Float, accuracy: Float = 0.00012f): Float? {
        return SENSITIVITY_MAP.keys.find {
            (a / it).isNearlyInt(accuracy) && (b / it).isNearlyInt(accuracy)
        }
    }

    fun Float.toClientSensitivity() = ((SENSITIVITY_MAP[this] ?: 0f) * 200f).roundToInt()

    adepts.onEach { _, adept ->
        // Client Sensitivity
        var currentSensitivity: Float = -1f

        // Rotation Data
        var lastDeltaYaw = 0f
        var lastDeltaPitch = 0f

        // attack / teleport tracker
        var lastAttack = markNow()
        var lastTeleport = markNow()

        // Buffer
        var buffer = 0

        adept.onTeleport {
            lastTeleport = markNow()
        }

        adept.onLeftClick {
            if (it.entity != null) {
                lastAttack = markNow()
            }
            PASS
        }

        adept.location.onChanged { from, to ->
            val deltaYaw = distanceOfAngles(from.yaw, to.yaw)
            val deltaPitch = distanceOfAngles(from.pitch, to.pitch)
            if (
                deltaYaw > 0
                && deltaPitch > 0
                && lastDeltaPitch > 0
                && lastDeltaYaw > 0
                && lastTeleport.elapsedNow().inWholeMilliseconds >= 1500L
                && lastAttack.elapsedNow().inWholeMilliseconds <= 1500L
            ) {
                // 初始化
                if (currentSensitivity < 0) {
                    val pitchGcd = findGcd(deltaPitch, lastDeltaPitch) ?: -1f
                    if (pitchGcd > 0) {
                        currentSensitivity = pitchGcd
                    }
                } else {
                    if ((deltaPitch / currentSensitivity).isNearlyInt()) {
                        // NOT CHEATING
                        buffer = (buffer - 1).coerceAtLeast(0)
                    } else {
                        // Update sens
                        findGcd(deltaPitch, lastDeltaPitch)?.takeIf { it > 0 }?.let {
                            currentSensitivity = it // Update Gcd
                        }
                        // Hacking?
                        if (!(deltaPitch / currentSensitivity).isNearlyInt()) {
                            buffer = (buffer + 2).coerceAtMost(10)
                            if (buffer > 4) {
                                buffer = 0
                                adept.flag(
                                    type = CheckType.KILLAURA,
                                    message = "exhibited rotation patterns characteristic of aimbot",
                                    vl = 0,
                                    data = listOf(
                                        "yaw" to (deltaYaw / currentSensitivity).toString(),
                                        "pitch" to (deltaPitch / currentSensitivity).toString(),
                                        "sensitivity" to currentSensitivity.toClientSensitivity().toString() + "%",
                                        "buf" to buffer.toString()
                                    )
                                )
                            }
                        }
                    }
                }
            }
            lastDeltaYaw = deltaYaw
            lastDeltaPitch = deltaPitch
        }
    }
}

private fun Toggled.PacketSequenceCheck(adepts: Adepts) {
    adepts.onEach { _, adept ->
        var lastMovingPacket = markNow()
        var lastFlag = markNow()

        // Make sure the player triggers the check multiple times
        var internalBuffer = 0

        adept.onLeftClick {
            if (it.entity != null) {
                // send the attack packet after the moving packet
                if (lastMovingPacket.elapsedNow().inWholeMilliseconds <= 5L) {
                    // avoid massive flags in 1 tick
                    if (lastFlag.elapsedNow().inWholeMilliseconds > 50L) {
                        internalBuffer = internalBuffer.plus(1).coerceAtMost(10)
                        if (internalBuffer > 5) {
                            adept.flag(
                                type = CheckType.KILLAURA,
                                message = "wrong packet sequence: $internalBuffer"
                            )
                        }
                    }
                    lastFlag = markNow()
                } else {
                    internalBuffer = internalBuffer.minus(2).coerceAtLeast(0)
                }
            }
            PASS
        }

        adept.location.onNext { _, _ ->
            lastMovingPacket = markNow()
        }
    }
}

private val SENSITIVITY_MAP = hashMapOf<Float, Float>().apply {
    (1..142).forEach { value ->
        fun Float.toGcd(): Float {
            val f = this * 0.6F + 0.2F
            return f * f * f * 1.2F
        }
        this[(value / 142f).toGcd()] = value / 142f
        // this[((value + 0.25f) / 142f).toGcd()] = ((value + 0.25f) / 142f)
        // this[((value + 0.5f) / 142f).toGcd()] = ((value + 0.5f) / 142f)
        // this[((value + 0.75f) / 142f).toGcd()] = ((value + 0.75f) / 142f)
    }
}