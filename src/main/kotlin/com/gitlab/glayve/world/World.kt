package com.gitlab.glayve.world

import com.gitlab.ballysta.architecture.ChangeEvent
import com.gitlab.ballysta.architecture.MutableTable
import com.gitlab.ballysta.architecture.onEach
import com.gitlab.glayve.Streamable
import com.gitlab.glayve.Streamer
import com.gitlab.glayve.network.Position
import com.gitlab.glayve.network.blockChange
import com.gitlab.glayve.network.chunkBulk

interface World : Streamable {
    operator fun get(position: Position): Block
    val onChanged: ChangeEvent<Position, Block>
}
interface MutableWorld : World {
    operator fun set(position: Position, block: Block): Block
}

fun LoadedWorld(
    chunks: List<Chunk>,
    types: (Int) -> (Block)
) = object : World {
    override fun get(position: Position): Block {
        val section = chunks.find {
            it.x == position.x shr 4 && it.z == position.z shr 4
        }?.sections?.get(position.y shr 4) ?: return types(0)
        if (section.isEmpty) return types(0)
        val x = position.x and 15
        val y = position.y and 15
        val z = position.z and 15
        val index = x or (z shl 4) or (y shl 8)
        return types(section.blockData[index].toInt())
    }
    override val onChanged: ChangeEvent<Position, Block> = {}

    val packets = chunks.filter {
        it.sections.any { section -> !section.isEmpty }
    }.chunked(15) { it.toTypedArray() }
    override val streamTo: Streamer = { player ->
        onEnabled {
            packets.forEach { player.chunkBulk(it, sky = true) }
        }
    }
}
fun HashWorld(world: World) = object : MutableWorld {
    val changes = MutableTable<Position, Block>()
    override val onChanged = changes.onChanged

    override fun get(position: Position) =
        changes[position] ?: world[position]
    override fun set(position: Position, block: Block) =
        changes.set(position, block) ?: world[position]

    override val streamTo: Streamer = { player ->
        world.streamTo(player)
        changes.onEach { position, block ->
            player.blockChange(position, (block.material.toInt() shl 4) or block.meta.toInt())
        }
    }
}
