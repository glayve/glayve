package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.network.Bool
import com.gitlab.glayve.network.Id
import com.gitlab.glayve.network.Meta
import com.gitlab.glayve.network.metadata
import kotlin.math.max
import kotlin.time.Duration



interface Effect {
    companion object {
        const val SPEED_UP = 1
        const val SPEED_DOWN = 2
        const val DIG_UP = 3
        const val DIG_DOWN = 4
        const val STRENGTH = 5
        const val INSTANT_HEAL = 6
        const val INSTANT_HARM = 7
        const val JUMP_UP = 8
        const val CONFUSION = 9
        const val REGENERATION = 10
        const val ALL_DAMAGE_DOWN = 11
        const val FIRE_DAMAGE_DOWN = 12
        const val BREATHING = 13
        const val INVISIBILITY = 14
        const val VISION_DOWN = 15
        const val VISION_UP = 16
        const val HUNGER_UP = 17
        const val ALL_DAMAGE_UP = 18
        const val POISON = 19
        const val WITHER = 20
        const val RED_HEARTS = 21
        const val GOLD_HEARTS = 22
        const val HUNGER_DOWN = 23

        val COLORS = intArrayOf(
            8171462, 5926017, 14270531, 4866583, 9643043, 16262179, 4393481, 2293580,
            5578058, 13458603, 10044730, 14981690, 3035801, 8356754, 2039587, 2039713,
            5797459, 4738376, 5149489, 3484199, 16284963, 2445989, 16262179
        )
    }

    val type: Id
    val duration: Duration
    val strength: Byte
    val particles: Bool
    val color: Color
}

fun Effect(
    type: Id, duration: Duration, strength: Byte = 1,
    particles: Bool = false, color: Color = Effect.COLORS[type]
) = object : Effect {
    override val type = type
    override val duration = duration
    override val strength = strength
    override val particles = particles
    override val color = color
}

fun Toggled.Effect(
    adepts: Adepts, type: Id, duration: Duration,
    strength: Byte = 1, particles: Bool = false,
    color: Color = Effect.COLORS[type],
    onEffect: Togglable.(Adept) -> (Unit)
) = Effect(type, duration, strength, particles, color).also {
    adepts.onEach { _, adept ->
        adept.effects.onEach { _, effect ->
            if (effect === it) onEffect(adept)
        }
    }
}


private const val BASE_SPEED = .10000000149011612
val Adepts.Speed get(): Toggled.(Byte, Duration) -> (Effect) = { strength, duration ->
    Effect(this@Speed, Effect.SPEED_UP, duration, strength) { adept ->
        val increased = BASE_SPEED * (1 + 0.20000000298023224 * (strength + 1))
        onEnabled { adept.attributes[GENERIC_SPEED] = increased }
        onDisabled { adept.attributes[GENERIC_SPEED] = BASE_SPEED }
    }
}
val Adepts.Slowness get(): Toggled.(Byte, Duration) -> (Effect) = { strength, duration ->
    Effect(this@Slowness, Effect.SPEED_DOWN, duration, strength) { adept ->
        val decreased = BASE_SPEED * (1 + -0.15000000596046448 * strength)
        onEnabled { adept.attributes[GENERIC_SPEED] = decreased }
        onDisabled { adept.attributes[GENERIC_SPEED] = BASE_SPEED }
    }
}
val Adepts.Absorption get(): Toggled.(Byte, Duration) -> (Effect) = { strength, duration ->
    Effect(this@Absorption, Effect.GOLD_HEARTS, duration, strength) { adept ->
        val amount = 4 * (strength + 1).toFloat()
        onEnabled { adept.shield(amount) }
        onDisabled { adept.shield(max(0f, adept.shield.last - amount)) }
    }
}
val Adepts.Regeneration get(): Toggled.(Byte, Duration) -> (Effect) = { strength, duration ->
    Effect(this@Regeneration, Effect.REGENERATION, duration, strength) { adept ->
        scheduler.every((50 shr strength.toInt()).ticks)() {
            if (adept.health.last < 20f) adept.health(adept.health.last + 1f)
        }
    }
}
val Adepts.Poison get(): Toggled.(Byte, Duration) -> (Effect) = { strength, duration ->
    Effect(this@Poison, Effect.POISON, duration, strength) { adept ->
        scheduler.every((50 shr strength.toInt()).ticks)() {
            if (adept.health.last > 1f) adept.health(adept.health.last - 1f)
        }
    }
}
val Adepts.Wither get(): Toggled.(Byte, Duration) -> (Effect) = { strength, duration ->
    Effect(this@Wither, Effect.WITHER, duration, strength) { adept ->
        scheduler.every((50 shr strength.toInt()).ticks)() {
            adept.health(adept.health.last - 1f)
        }
    }
}