package com.gitlab.glayve

import com.gitlab.ballysta.architecture.PublishedObservable
import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.TreeEvent
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.network.Bool
import com.gitlab.glayve.util.Location

sealed class Cause {
    data class PlayerAttack(
        val channel: Channel, val critical: Bool, val sprinting: Bool
    ): Cause()
}

interface Damagable {
    val onDamage: TreeEvent<(Float, Cause) -> (Unit)>
}

//receiving attack packet
//checking distance, vulnerability, etc.
//checking held item for damage.
//checking held item and locations to get knockback vector.
//checking held item and location to get crit types.
//sending crit and damage animations for hit id.
//sending knockback velocity for it id.
fun Toggled.Attacker(
    channel: Channel, stack: PublishedObservable<Stack>,
    target: (Int) -> (Location?)
) {

}