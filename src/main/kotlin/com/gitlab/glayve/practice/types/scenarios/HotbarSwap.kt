package com.gitlab.glayve.practice.types.scenarios

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Item
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.items.Item
import com.gitlab.glayve.network.Id
import com.gitlab.glayve.network.Slot
import com.gitlab.glayve.network.toSlot
import com.gitlab.glayve.practice.*
import com.gitlab.glayve.practice.games.Arena
import com.gitlab.glayve.world.BLOCK_WOOL
import com.gitlab.glayve.world.material
import com.gitlab.glayve.world.meta
import kotlin.time.Duration.Companion.ZERO
import kotlin.time.Duration.Companion.seconds



const val SWAP_TIME = 10
fun Togglable.HotbarSwap(online: Adepts, players: Adepts, arena: Arena) {
    val spectators = online.filter { id, _ -> id !in players }
    Spectate(spectators, arena.spectators)
    Play(players, arena.red)
    val best = Published(0)
    val points = Published(0)
    var currentSlot: Slot = 40

    val available = ArrayList<Slot>()
    for (i in 36..44) available.add(i.toSlot())

    players.onEach { _, adept ->
        adept.selected {
            if (it != currentSlot) {
                if (points.last != 0) points(points.last - 1)
            }
        }
    }

    lateinit var wool: Item
    wool = Item(players, BLOCK_WOOL.material, BLOCK_WOOL.meta, 1, emptyMap(), { adept, _ ->
        adept.inventory.clear(36..44)
        val slot = available.random()
        adept.inventory[slot] = Stack(wool, 1)
        points(points.last + 1)
        currentSlot = slot
        CONSUME
    }) { _, _ -> PASS }

    online.onEach { _, adept ->
        adept.board(0, just("${RED}${BOLD}Aim Practice"))
        adept.board(1, date())
        adept.board(2, just(" "))
        adept.board(4, points.map { "Swaps:$RED $it Block(s)" })
        adept.board(5, just(" "))
        adept.board(6, just("Scenario:$RED Hotbar Swap"))
        adept.board(7, best.map { "Personal Best:$RED $it Swap(s)" })
        adept.board(8, just(" "))
        adept.board(9, just(DOMAIN))
        adept.gamemode(Gamemode.ADVENTURE)

    }

    CancelClicks(players)

    val viewers = MutableTable<Id, Adept>()
    val slotPicker = Item(players, 347, 0, 1, displayName("Slot Picker"), { adept, _ ->
        viewers[adept.id] = adept; CONSUME
    }) { _, _ -> PASS }
    SlotPicker(viewers, available)

    val preGame = phased {
        players.onEach { _, adept ->
            adept.inventory.clear()
            adept.inventory[40] = Stack(wool, 1)
            adept.inventory[36] = Stack(slotPicker, 1)
        }

        online.onEach { _, adept ->
            adept.board(3, just("Time Left:$RED $SWAP_TIME Second(s)"))
        }

        points { if (points.last > 0) disable() }
        onEnabled { points(0); }
    }

    val inGame = phased {
        val timer = scheduler.every(1.seconds, ZERO)
        timer {
            if ((SWAP_TIME - it) < 1) {
                disable()
            }
        }
        online.onEach { _, adept ->
            adept.board(3, timer.map { "Time Left:$RED ${(SWAP_TIME - it)} Second(s)" })
        }

        onDisabled {
            if (points.last > best.last) best(points.last)
            points(0)
        }
    }

    players.onEach { _, adept ->
        adept.location {
            if (it.y <= 68) adept.location(arena.red)
        }
    }

    this.onEnabled(preGame::enable)
    preGame.onDisabled(inGame::enable)
    inGame.onDisabled(preGame::enable)
}