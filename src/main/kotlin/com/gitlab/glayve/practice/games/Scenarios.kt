package com.gitlab.glayve.practice.games

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.*
import com.gitlab.glayve.inventory.Container.*
import com.gitlab.glayve.network.Id
import com.gitlab.glayve.practice.GameNPC
import com.gitlab.glayve.practice.types.scenarios.BlockFrenzy
import com.gitlab.glayve.practice.types.scenarios.HotbarSwap
import com.gitlab.glayve.practice.types.scenarios.Reactions
import com.gitlab.glayve.practice.types.scenarios.bots.Bots
import com.gitlab.glayve.practice.types.scenarios.bots.Clicks
import com.gitlab.glayve.practice.types.scenarios.looting.ChestGrab
import com.gitlab.glayve.practice.types.scenarios.looting.ChestLooting
import com.gitlab.glayve.practice.types.scenarios.looting.badChestGrabItems
import com.gitlab.glayve.practice.types.scenarios.looting.chestGrabItems
import com.gitlab.glayve.util.Location
import com.gitlab.glayve.world.HashWorld


fun Toggled.Scenarios(
    online: Channels, hub: Adepts,
    playing: Mutable<Id, Togglable>
) {
    fun start(channel: Channel, type: Id) {
        val game = phased {
            val players = online and playing.group(this)
            players.onRemoved { _, _ ->
                if (players.size <= 1) disable()
            }
            val arena = getArena(type)
            val world = HashWorld(arena.world)

            val adepts = Adepts(players, world, arena.spectators)
            val player = adepts.filter { otherId, _ -> otherId == channel.id }
            when (type) {
                REACTIONS -> Reactions(world, adepts, player, arena)
                HOTBAR_SWAP -> HotbarSwap(adepts, player, arena)
                CHEST_LOOTING_1 -> ChestGrab(adepts, player, arena,
                    { Stack(Item(chestGrabItems.random().toShort()), 1) }
                )
                CHEST_LOOTING_2 -> ChestLooting(adepts, player, arena,
                    { Stack(Item(chestGrabItems.random().toShort()), 1) },
                    { Stack(Item(badChestGrabItems.random().toShort()), 1) }
                )
                BLOCK_FRENZY -> BlockFrenzy(world, adepts, player, arena)
                CLICK_TRAINER -> Clicks(adepts, player, arena)
                TRACKING_TRAINER -> Bots(adepts, player, arena)
            }
        }
        playing[channel.id] = game
        game.enable();
    }

    val menu = hub.mapMutable()
    Menu(menu, CHEST_2, just("Select A Mode")) { adept, slots ->
        slots[(0..8) - 4] = {
            material(160); meta(COLORED_BLACK)
            name(""); onClick { PASS }
        }
        slots[4] = {
            material(397)
            name("${RED}Player Info")
            lore(
                "${WHITE}Name:$RED ${adept.name}", "",
                "${RED}Click to go to store"
            ); onClick { PASS }
        }
        slots[9] = {
            material(389)
            name("${RED}Reactions")
            lore(
                "${SILVER}Click to join the",
                "${RED}Reactions$SILVER scenario.", "",
                "${SILVER}The$RED objective$SILVER of this scenario",
                "${SILVER}is to$RED click$SILVER on the wall",
                "${SILVER}as soon as it changes$RED color$SILVER.",
                "", "${SILVER}Top Score:$RED ..."
            )
            onClick { disable(); start(adept, REACTIONS); PASS }
        }
        slots[10] = {
            material(399)
            name("${RED}Hotbar Swap")
            lore(
                "${SILVER}Click to join the",
                "${RED}Hotbar Swap$SILVER scenario.", "",
                "${SILVER}The$RED objective$SILVER of this scenario",
                "${SILVER}is to$RED select$SILVER the item",
                "${SILVER}in your$RED hotbar$SILVER as many times as possible.",
                "", "${SILVER}Top Score:$RED ..."
            )
            onClick {  disable(); start(adept, HOTBAR_SWAP); PASS }
        }
        slots[11] = {
            material(54)
            name("${RED}Chest Looting$SILVER I")
            lore(
                "${SILVER}Click to join the",
                "${RED}Chest Looting$SILVER scenario.", "",
                "${SILVER}The$RED objective$SILVER of this scenario",
                "${SILVER}is to$RED collect$SILVER all the",
                "${SILVER}items in the$RED chest$SILVER.",
                "", "${SILVER}Top Score:$RED ..."
            )
            onClick { disable(); start(adept, CHEST_LOOTING_1); PASS }
        }
        slots[12] = {
            material(54)
            name("${RED}Chest Looting$SILVER II")
            lore(
                "${SILVER}Click to join the",
                "${RED}Chest Looting$SILVER scenario.", "",
                "${SILVER}The$RED objective$SILVER of this scenario",
                "${SILVER}is to$RED collect$SILVER all the",
                "${SILVER}items in the$RED chest$SILVER, without selecting",
                "${SILVER}the bad items.", "", "${SILVER}Top Score:$RED ..."
            )
            onClick { disable(); start(adept, CHEST_LOOTING_2); PASS }
        }
        slots[13] = {
            material(159); meta(COLORED_RED)
            name("${RED}Block Frenzy")
            lore(
                "${SILVER}Click to join the",
                "${RED}Block Frenzy$SILVER scenario.", "",
                "${SILVER}The$RED objective$SILVER of this scenario",
                "${SILVER}is to$RED click$SILVER on the",
                "${SILVER}player heads as fast as$RED possible$SILVER.",
                "", "${SILVER}Top Score:$RED ..."
            )
            onClick { disable(); start(adept, BLOCK_FRENZY); PASS }
        }
    }
    val trainer = hub.mapMutable()
    Menu(trainer, CHEST_2, just("Select A Mode")) { adept, slots ->
        slots[(0..8) - 4] = {
            material(160); meta(COLORED_BLACK)
            name(""); onClick { PASS }
        }
        slots[4] = {
            material(397)
            name("${RED}Player Info")
            lore(
                "${WHITE}Name:$RED ${adept.name}", "",
                "${RED}Click to go to store"
            ); onClick { PASS }
        }
        slots[9] = {
            material(311)
            name("${RED}Tracking Trainer")
            lore(
                "${SILVER}Click to join the",
                "${RED}Tracking$SILVER Trainer.", "",
                "${SILVER}You have to$RED track$SILVER the Trainer",
                "${SILVER}as it$RED circles$SILVER around you.",
            )
            onClick { disable(); start(adept, TRACKING_TRAINER); CONSUME }
        }
        slots[10] = {
            material(272); tags(1 shl 1)
            name("${RED}Click Speed Trainer")
            lore(
                "${SILVER}Click to join the",
                "${RED}Click Speed$SILVER Trainer.", "",
                "${SILVER}You have to$RED attack$SILVER the Trainer",
                "${SILVER}to test your$RED CPS$SILVER."
            )
            onClick { disable(); start(adept, CLICK_TRAINER); CONSUME }
        }
    }

    GameNPC(hub, Location(84.5, 58.0, 107.5), "${RED}${BOLD}Scenarios", menu::set)
    GameNPC(hub, Location(92.5, 58.0, 107.5), "${GOLD}${BOLD}PvP", trainer::set)
}