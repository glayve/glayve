package com.gitlab.glayve.network

import com.github.exerosis.mynt.base.Read
import com.github.exerosis.mynt.base.Write
import com.github.exerosis.mynt.bytes
import java.math.BigInteger
import java.nio.ByteBuffer
import java.security.*
import java.security.spec.PKCS8EncodedKeySpec
import javax.crypto.Cipher
import javax.crypto.Cipher.*
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

fun RSA(mode: Int, key: ByteArray): Cipher =
    getInstance("RSA/ECB/PKCS1Padding").apply {
        val factory = KeyFactory.getInstance("RSA")
        val encoded = PKCS8EncodedKeySpec(key)
        init(mode, if (mode == DECRYPT_MODE)
            factory.generatePrivate(encoded)
        else factory.generatePublic(encoded))
    }
fun AES(mode: Int, key: ByteArray): Cipher =
    getInstance("AES/CFB8/NoPadding").apply {
        init(mode, SecretKeySpec(key, "AES"), IvParameterSpec(key))
    }

fun createKeys(type: String): Pair<ByteArray, ByteArray> =
    KeyPairGenerator.getInstance(type).run {
        initialize(1024); generateKeyPair()
    }.run { public.encoded to private.encoded }
fun createHexdigest(vararg bytes: ByteArray): String =
    BigInteger(MessageDigest.getInstance("SHA-1").apply {
        bytes.forEach { update(it) }
    }.digest()).toString(16)

fun decrypt(read: Read, cipher: Cipher) = object : Read {
    override suspend fun skip(amount: Int) = read.skip(amount)

    override suspend fun buffer(buffer: ByteBuffer): ByteBuffer {
        bytes(buffer.array(), buffer.position())
        return buffer
    }
    override suspend fun bytes(bytes: ByteArray, amount: Int, offset: Int): ByteArray {
        val input = ByteArray(amount)
        read.bytes(input, input.size, 0)
        cipher.update(input, 0, input.size, bytes, offset)
        return bytes
    }

    override suspend fun byte(): Byte {
        val buffer = ByteBuffer.allocate(1)
        bytes(buffer.array(), 1, 0)
        return buffer.get()
    }
    override suspend fun short(): Short {
        val buffer = ByteBuffer.allocate(2)
        bytes(buffer.array(), 2, 0)
        return buffer.short
    }
    override suspend fun int(): Int {
        val buffer = ByteBuffer.allocate(4)
        bytes(buffer.array(), 4, 0)
        return buffer.int
    }
    override suspend fun float(): Float {
        val buffer = ByteBuffer.allocate(4)
        bytes(buffer.array(), 4, 0)
        return buffer.float
    }
    override suspend fun long(): Long {
        val buffer = ByteBuffer.allocate(8)
        bytes(buffer.array(), 8, 0)
        return buffer.long
    }
    override suspend fun double(): Double {
        val buffer = ByteBuffer.allocate(8)
        bytes(buffer.array(), 8, 0)
        return buffer.double
    }
}
fun encrypt(write: Write, cipher: Cipher) = object : Write {
    override suspend fun buffer(buffer: ByteBuffer) {
        bytes(buffer.array(), buffer.position(), 0)
    }
    override suspend fun bytes(bytes: ByteArray, amount: Int, offset: Int) {
        val result = cipher.update(bytes)
        write.bytes(result, result.size, 0)
    }

    override suspend fun byte(byte: Byte) {
        val bytes = ByteArray(1)
        ByteBuffer.wrap(bytes).put(byte)
        bytes(bytes, 1, 0)
    }
    override suspend fun short(short: Short) {
        val bytes = ByteArray(2)
        ByteBuffer.wrap(bytes).putShort(short)
        bytes(bytes, 2, 0)
    }

    override suspend fun skip(amount: Int) {
        bytes(ByteArray(amount))
    }

    override suspend fun int(int: Int) {
        val bytes = ByteArray(4)
        ByteBuffer.wrap(bytes).putInt(int)
        bytes(bytes, 4, 0)
    }
    override suspend fun float(float: Float) {
        val bytes = ByteArray(4)
        ByteBuffer.wrap(bytes).putFloat(float)
        bytes(bytes, 4, 0)
    }
    override suspend fun long(long: Long) {
        val bytes = ByteArray(8)
        ByteBuffer.wrap(bytes).putLong(long)
        bytes(bytes, 8, 0)
    }
    override suspend fun double(double: Double) {
        val bytes = ByteArray(8)
        ByteBuffer.wrap(bytes).putDouble(double)
        bytes(bytes, 8, 0)
    }
}