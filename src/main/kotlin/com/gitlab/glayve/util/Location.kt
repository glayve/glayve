package com.gitlab.glayve.util

import com.gitlab.glayve.network.Bool
import com.gitlab.glayve.network.Position
import com.gitlab.glayve.toDegrees
import java.lang.Math.toRadians
import kotlin.math.*

data class Location(
    val x: Double, val y: Double, val z: Double,
    val yaw: Float = 0f, val pitch: Float = 0f, val ground: Bool = true
)

fun Location.distance(other: Location): Double {
    val deltaX = x - other.x
    val deltaY = y - other.y
    val deltaZ = z - other.z
    return kotlin.math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ)
}

fun Location.distanceHorizontal(other: Location): Double {
    val deltaX = x - other.x
    val deltaZ = z - other.z
    return kotlin.math.sqrt(deltaX * deltaX + deltaZ * deltaZ)
}

fun Location.add(x: Double = 0.0, y: Double = 0.0, z: Double = 0.0)
    = copy(x = this.x + x, y = this.y + y, z = this.z + z)

// remove it?
val Location.directionn get(): Velocity {
    val dist = cos(toRadians(pitch.toDouble()))
    val x = -dist * sin(toRadians(yaw.toDouble()))
    val y = -sin(toRadians(pitch.toDouble()))
    val z = dist * cos(toRadians(yaw.toDouble()))
    return Velocity(x, y, z)
}

operator fun Location.plus(other: Velocity) =
    copy(x = x + other.x, y = y + other.y, z = z + other.z)

fun Location.setDirection(direction: Velocity): Location {
    val (x, y, z) = direction
    return if (x != 0.0 || z != 0.0) copy(
        yaw = ((atan2(-x, z) + PI * 2) % (PI * 2)).toDegrees().toFloat(),
        pitch = atan(-y / sqrt(x * x + z * z)).toDegrees().toFloat()
    ) else copy(pitch = if (y > 0.0) -90f else 90f)
}

val Location.direction get(): Velocity {
    val yRotation = toRadians(pitch.toDouble())
    val xRotation = toRadians(yaw.toDouble())
    val xz = cos(yRotation)
    return Velocity(
        -xz * sin(xRotation),
        -sin(yRotation),
        xz * cos(xRotation)
    )
}

fun Location.toPosition() = Position(floor(x).toInt(), floor(y).toInt(), floor(z).toInt())
fun Location.toVelocity() = Velocity(x, y, z)

val Location.compressedYaw get() = ((yaw * 256) / 360f).toInt().toByte()
val Location.compressedPitch get() = ((pitch * 256) / 360f).toInt().toByte()