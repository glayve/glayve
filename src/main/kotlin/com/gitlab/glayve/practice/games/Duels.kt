package com.gitlab.glayve.practice.games

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.*
import com.gitlab.glayve.inventory.Container.CHEST_2
import com.gitlab.glayve.network.Id
import com.gitlab.glayve.practice.GameNPC
import com.gitlab.glayve.practice.types.duels.Boxing
import com.gitlab.glayve.practice.types.duels.Bridges
import com.gitlab.glayve.practice.types.duels.OpDuels
import com.gitlab.glayve.practice.types.duels.Sumo
import com.gitlab.glayve.util.Location
import com.gitlab.glayve.world.HashWorld

fun Toggled.Duels(
    online: Channels, hub: Adepts,
    playing: Mutable<Id, Togglable>
) {
    fun start(first: Adept, second: Adept, type: Id) {
        val game = phased {
            val players = online and playing.group(this)
            players.onRemoved { _, _ ->
                if (players.size <= 2) disable()
            }
            val arena = getArena(type)
            val world = HashWorld(arena.world)
            val adepts = Adepts(players, world, arena.spectators)
            val red = adepts.filter { otherId, _ -> otherId == first.id }
            val blue = adepts.filter { otherId, _ -> otherId == second.id }
            when (type) {
                BRIDGES_DUEL -> Bridges(adepts, red, blue, arena)
               // UHC_DUEL -> Uhc(world, adepts, red, blue, arena)
                SUMO_DUEL -> Sumo(adepts, red, blue, arena)
                BOXING_DUEL -> Boxing(adepts, red, blue, arena)
                OP_DUEL -> OpDuels(world, adepts, red, blue, arena)
            }
        }
        playing[first.id] = game; playing[second.id] = game
        game.enable()
    }

    val queue = MutableTable<Id, Adept>() //game id
    val menu = hub.mapMutable()
    queue.onEach { _, adept ->
        playing.onAdded { id, _ -> if (adept.id == id) disable() }
        online.onRemoved { id, _ -> if (adept.id == id) disable() }
    }
    DuelMenu(menu) { adept, game ->
        val queued = queue.set(game, null)
        if (queued == null || (queued == adept && !DEBUG)) {
            queue.forEach { id, other ->
                if (other == adept) queue[id] = null
            }
            queue[game] = adept
            adept.send("${RED}You are now in queue.")
        } else start(adept, queued, game)
    }

    GameNPC(hub, Location(80.5, 58.0, 106.5), "${BLUE}${BOLD}Duels", menu::set)
}
private fun Toggled.DuelMenu(viewers: MutableAdepts, action: (Adept, Id) -> (Unit))
    = Menu(viewers, CHEST_2, just("Select A Mode")) { adept, slots ->
        slots[(0..8) - 4] = {
            material(160); meta(COLORED_BLACK)
            name(""); onClick { PASS }
        }
        slots[4] = {
            material(397)
            name("${RED}Player Info")
            lore(
                "${WHITE}Name:$RED ${adept.name}", "",
                "${RED}Click to go to store"
            ); onClick { PASS }
        }
        slots[9] = {
            material(159); meta(11)
            name("${RED}Bridge Duels")
            lore("${SILVER}Click to queue into ${RED}Bridge Duels")
            onClick { disable(); action(adept, BRIDGES_DUEL); PASS }
        }
//        slots[10] = {
//            material(322)
//            name("${RED}UHC Duels")
//            lore("${GREY}Click to queue into ${RED}UHC Duels")
//            onClick { disable(); action(adept, UHC_DUEL); PASS }
//        }
        slots[11] = {
            material(341)
            name("${RED}Sumo Duels")
            lore("${SILVER}Click to queue into ${RED}Sumo Duels")
            onClick { disable(); action(adept, SUMO_DUEL); PASS }
        }
        slots[12] = {
            material(267); tags(1 shl 1)
            name("${RED}Boxing Duels")
            lore("${SILVER}Click to queue into ${RED}OP Duels")
            onClick { disable(); action(adept, BOXING_DUEL); PASS }
        }
        slots[13] = {
            material(373); meta(16385); tags(1 shl 5)
            name("${RED}OP Duels")
            lore("${SILVER}Click to queue into ${RED}OP Duels")
            onClick { disable(); action(adept, OP_DUEL); PASS }
        }
    }
