const val WATER_FLOWING = 8.toShort()
const val WATER_STATIC = 9.toShort()
const val LAVA_FLOWING = 10.toShort()
const val LAVA_STATIC = 11.toShort()

const val IRON_SWORD = 267.toShort()
const val DIAMOND_SWORD = 276.toShort()
const val GOLD_SWORD = 283.toShort()
const val WOOD_SWORD = 268.toShort()
const val STONE_SWORD = 272.toShort()

const val WOODEN_PICKAXE = 270.toShort()
const val STONE_PICKAXE = 274.toShort()
const val IRON_PICKAXE = 257.toShort()
const val GOLDEN_PICKAXE = 285.toShort()
const val DIAMOND_PICKAXE = 278.toShort()

const val DIAMOND_AXE = 279.toShort()

const val ARROW = 262.toShort()

const val ROD = 346.toShort()
const val BOW = 261.toShort()
