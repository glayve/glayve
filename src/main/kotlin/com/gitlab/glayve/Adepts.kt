package com.gitlab.glayve

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.inventory.Inventory
import com.gitlab.glayve.network.*
import com.gitlab.glayve.util.Bounds
import com.gitlab.glayve.util.Location
import com.gitlab.glayve.util.Velocity
import com.gitlab.glayve.util.distance
import com.gitlab.glayve.world.*
import java.util.concurrent.ThreadLocalRandom.current
import kotlin.experimental.and
import kotlin.experimental.or
import kotlin.math.max
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

const val CONSUME = false
const val PASS = true

const val DEFAULT_FLY = 0.05f
const val DEFAULT_MOVE = 0.1f

interface Adept : Channel {
    override val location: PublishSubject<Location>
    val blocks: MutableWorld
    val effects: MutableHolder<Effect>
    val attributes: Attributes
    val board: Toggled.(Int, Observable<Any>) -> (Unit)

    val inventory: Inventory
    val onTeleport: Event<(Location) -> (Unit)>
    val onKnockback: Event<(Velocity) -> (Unit)>

    val invulnerable: PublishSubject<Boolean>

    val fly: PublishSubject<Float>
    val move: PublishSubject<Float>
    val flight: PublishSubject<Bool>
    val movement: PublishSubject<Bool>

    val bounds: PublishedObservable<Bounds>
    val fall: PublishedObservable<Double>

    fun damage(
        amount: Float, velocity: Velocity,
        critical: Bool = false, magical: Bool = false,
        direct: Bool = false
    ): Bool
}
typealias Adepts = Mutated<Id, Adept>
typealias MutableAdepts = Mutable<Id, Adept>
//TODO give these both right and left click so that we can make picks and what not.
//TODO figure out how to abstract away breaking blocks cleanly.
val Adepts.Block get(): Toggled.(
    Short, Short, Byte, Bounds, String,
    Togglable.(Adept, Interaction) -> (Bool),
) -> (Block) = { material, meta, hardness, bounds, sound, use ->
    Block(material, meta, hardness, bounds, sound).also { block ->
        onEach { _, adept -> adept.onRightClick {
            if (it.position != null && adept.blocks[it.clicked!!] === block)
                use(adept, it) else PASS
        } }
    }
}

val Adepts.serverBreak get(): Togglable.(Adept, Position, Duration) -> (Unit) = { adept, position, hardness ->
    val testId = adept.hashCode()
    fun pop() {
        val type = adept.blocks.set(position, BLOCK_AIR)
        val state = type.material.toInt() or (type.meta.toInt() shl 12)
        send { effect(2001, position, state, false) }
        disable()
    }
    onDisabled {
        send { breakAnimation(testId, position, 10) }
    }
    val hard = hardness / 10
    if (!hard.isPositive()) pop()
    else {
        scheduler.every(hard)() { stage ->
            if (stage > 9) pop() else send {
                breakAnimation(testId, position, stage)
            }
        }
        enable()
    }
}

val BLANKS = Array(16) { String(charArrayOf('§', 'q' + it)) }
fun Toggled.Adepts(
    channels: Channels, world: MutableWorld, spawn: Location
): Adepts {
    val adepts = MutableTable<Id, Adept>()
    //TODO handle moving in and out of world as they are added and removed from the mutable.
    channels.onEach { id, channel ->
        val adept = object : Adept, Channel by channel {
            override val blocks = world
            override val effects = MutableHolder<Effect>()
            override val attributes = MutableTable<String, Double>()
            override val inventory = Inventory(channel)
            override val board by lazy {
                object : (Toggled, Int, Observable<Any>) -> (Unit) {
                    init {
                        BLANKS.forEach { blank -> channel.teams(0, blank, "", "", blank) }
                        objective("main", 0, "Title")
                        display(1, "main")
                    }

                    override fun invoke(
                        scope: Toggled, index: Int, line: Observable<Any>
                    ) = scope.run {
                        line.map { it.toString().take(28) }() {
                            if (index != 0) {
                                val split = if (it.length < 16) 0 else if (it[15] == '§') 15 else 16
                                val prefix = if (split == 0) it else it.substring(0, split)
                                val color = if (split == 16) prefix.toCharArray().run color@{
                                    for (i in (0..lastIndex).reversed())
                                        if (get(i) == '§' && get(i + 1) < 'k')
                                            return@color "§${get(i + 1)}"; ""
                                } else ""
                                //FIXME lolol this is so shit... do it properly at some point.
                                val mode = if (split == 16) prefix.toCharArray().run color@{
                                    for (i in (0..lastIndex).reversed())
                                        if (get(i) == '§' && get(i + 1) >= 'k')
                                            return@color "§${get(i + 1)}"; ""
                                } else ""
                                val suffix = if (split == 0) "" else color + mode + it.substring(split)
                                teams(2, BLANKS[index], prefix, suffix)
                            } else objective("main", 2, it)
                        }
                        if (index != 0) {
                            onEnabled { updateScore(0, "main", BLANKS[index], 0) }
                            onDisabled { updateScore(1, "main", BLANKS[index]) }
                        }
                    }
                }
            }

            override val onTeleport = TreeEvent<(Location) -> (Unit)>()
            override val onKnockback = TreeEvent<(Velocity) -> (Unit)>()
            override val invulnerable = Published(false)

            override val location = object : PublishSubject<Location> {
                val event = TreeEvent<(Location) -> (Unit)>()
                var teleported: Location? = null
                //TODO fix this
                override val last get() = teleported ?: channel.location.last
                override val current get() = teleported ?: channel.location.current

                override fun invoke(destination: Location) {
                    if (!enabled) {
                        println("Tried to teleport $channel to $destination while disabled!")
                        RuntimeException().printStackTrace()
                    }
                    if (teleported != destination) {
                        println("teleporting to ${destination.toString()}")
                        teleported = destination
                        onTeleport(destination)
                        position(destination)
                    }
                }

                override fun invoke(
                    scope: Toggled, observer: (Location) -> (Unit)
                ) = event(scope, observer)
            }

            override val fly = Published(0f)
            override val move = Published(DEFAULT_MOVE)
            override val flight = Published(false)
            override val movement = Published(true)

            override val bounds = location.map {
                Bounds(
                    it.x - WIDTH, it.y, it.z - WIDTH,
                    it.x + WIDTH, it.y + HEIGHT, it.z + WIDTH
                )
            }
            override val fall = location.aggregate(0.0) { distance, location ->
                if (location.ground) 0.0 else distance +
                        max(0.0, this.location.last.y - location.y)
            }


            override fun damage(amount: Float, velocity: Velocity, critical: Bool, magical: Bool, direct: Bool): Bool {
                //TODO why does magical true/false not effect anything?
                if (!direct) {
                    if (invulnerable.last) return false
                    invulnerable(true)
                    transient {
                        scheduler.after(0.5.seconds)() {
                            invulnerable(false)
                        }
                    }
                }

                val shields = shield.last
                if (shields > 0) shield(max(0f, shields - amount))
                if (shields < amount) health(max(0f, health.last - (amount - shields)))

                onKnockback(velocity)
                adepts.send {
                    velocity(id, velocity)
                    animation(id, 1)
                    val pitch = (current().nextFloat() - current().nextFloat()) * 0.2f + 1.0f
                    sound("game.neutral.hurt", location.last, 1f, pitch)
                    if (critical) animation(id, 4)
                    if (magical) animation(id, 5)
                }
                return true
            }

            override fun equals(other: Any?) = other is Channel && other.id == id
            override fun toString() = name

            init {
                fun abilities(fly: Float, move: Float, flight: Bool, movement: Bool) {
                    val god = this.gamemode.last == Gamemode.CREATIVE
                    if (movement) abilities(god, flight, god, fly, move) else {
                        abilities(god, true, god, 0f, move)
                        if (this.movement.last) {
                            velocity(id, Velocity(0.0))
//                            onKnockback(Velocity(0.0))
                        }
                    }
                }
                fly { abilities(it, move.last, flight.last, movement.last) }
                move { abilities(fly.last, it, flight.last, movement.last) }
                flight { abilities(fly.last, move.last, it, movement.last) }
                movement { abilities(fly.last, move.last, flight.last, it) }
                health { updateHealth(it, food.last, saturation.last) }
                shield { metadata(id, mapOf(17 to Meta(3, it))) }
                saturation { updateHealth(health.last, food.last, it) }
                food { updateHealth(health.last, it, saturation.last) }
                gamemode { state(3, it.ordinal.toFloat()) }
            }
        }

        val flags = Published(0.toByte()).apply {
            channel.using { this(if (it) last or 0x10 else last and 0x10.inv()) }
            channel.sprinting { this(if (it) last or 0x08 else last and 0x08.inv()) }
            channel.sneaking { this(if (it) last or 0x02 else last and 0x02.inv()) }
            channel.burning { this(if (it) last or 0x01 else last and 0x01.inv()) }
        }
        flags { channel.metadata(id, mapOf(0 to Meta(0, it))) }

        onDisabled { channel.burning(false) }

        channel.location {
            val teleported = adept.location.teleported
            if (
                teleported == null || it == teleported ||
                it.distance(adept.location.last) < 10
            ) {
                adept.location.teleported = null
                adept.location.event(it)
            } else adept.position(teleported)
        }

        adept.effects.onEach { _, effect ->
            scheduler.after(effect.duration)() {
                adept.effects.remove(effect)
            }
            //TODO consider implications of only having one effect of a type at a time.
            onEnabled {
                adepts.send {
                    addEffect(
                        id, effect.type.toByte(), effect.strength,
                        effect.duration.inTicks.toInt(), effect.particles
                    )
                }
            }
            onDisabled { adepts.send { removeEffect(id, effect.type.toByte()) } }
        }
        onDisabled { adept.effects.keys.forEach { adept.effects[it] = null } }
        adept.effects.onChanged { _, _, _ ->
            var reds = 0f;
            var greens = 0f
            var blues = 0f;
            var total = 0
            adept.effects.forEach { _, effect ->
                val (red, green, blue) = effect.color
                total += (effect.strength + 1).also {
                    reds += red.toFloat() / 255 * it
                    greens += green.toFloat() / 255 * it
                    blues += blue.toFloat() / 255 * it
                }
            }
            val meta = mapOf(
                7 to Meta(
                    2, Color(
                        red = ((reds / total) * 255).toInt(),
                        green = ((greens / total) * 255).toInt(),
                        blue = ((blues / total) * 255).toInt()
                    )
                )
            )
            adepts.send { metadata(id, meta) }
        }

        //TODO this is just temp
        adept.addEffect(id, 4, -1, -1)

        adept.attributes.onChanged { name, _, to ->
            val attribute = Attribute(name, to ?: 0.0)
            adepts.send { attributes(id, attribute) }
        }

        //right and left are effectively equal to using and digging
        //also we can use transient here.

        //Ok so first we get loaded into respawn screen IG. (can we make this cleaner IDK)
        onEnabled { channel.respawn(0, adept.difficulty.last, adept.gamemode.last, "") }
        //Ok then we steam out the world data.
        world.streamTo(channel)
        //Then we add to adepts, which may trigger moving to a new spot away from spawn (is this bad?)
        onEnabled { adepts[id] = adept }

        //First we clear the inventory
        onDisabled { adept.inventory.clear() }
        //Then we show the downloading terrain screen.
        onDisabled { channel.respawn(1, adept.difficulty.last, adept.gamemode.last, "") }
        //Finally we update adepts.
        onDisabled { adepts[id] = null }

        //--Contract--
        //By the time adepts.onRemoved runs they will be in the terrain screen and
        //completely ready to be added to a new adepts. By the time adepts.onAdded
        //they will be in the fully loaded new world.
    }
    return adepts
}