package com.gitlab.glayve.practice.types.parkour

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.items.ItemBlock
import com.gitlab.glayve.network.Position
import com.gitlab.glayve.practice.CancelClicks
import com.gitlab.glayve.practice.games.Arena
import com.gitlab.glayve.practice.games.ladderSpawn
import com.gitlab.glayve.practice.show
import com.gitlab.glayve.world.BLOCK_AIR
import com.gitlab.glayve.world.BLOCK_LADDER
import kotlin.math.floor
import kotlin.time.Duration.Companion.seconds

fun Togglable.Ladders(players: Adepts, playing: Adepts, arena: Arena) {
    val spectators = players - playing
    spectators.show(playing)

    val attempts = Published(0)
    val completed = Published(0)
    val ladders = MutableTable<Position, Adept>()
    val ladder =
        ItemBlock(playing, BLOCK_LADDER, mapOf("CanPlaceOn" to arrayOf("minecraft:gold_block"))) { adept, position ->
            adept.blocks[position] = BLOCK_LADDER
            ladders[position] = adept; CONSUME
        }

    ladders.onEach { position, adept ->
        scheduler.after(1.seconds)() { ladders[position] = null }
        onDisabled { adept.blocks[position] = BLOCK_AIR }
    }
    CancelClicks(players)

    players.onEach { _, adept ->
        adept.board(0, just("§c§lPractice"))
        adept.board(1, just("  §7§oLadder Clutching"))
        adept.board(2, just(""))
        adept.board(3, attempts.map { "Current attempts: §7$it" })
        adept.board(4, completed.map { "Completed: §7$it" })
        adept.board(5, just("Difficulty: §7Normal"))
        adept.board(6, just(""))
        adept.board(7, just("§cplay.pyrelic.net"))
    }

    val preGame = Component {
        playing.onEach { _, adept ->
            adept.inventory.clear()
            adept.inventory[36] = Stack(ladder, 64)
            adept.location(ladderSpawn)
            adept.location { if (it.y < 23 && it.y > 6) disable() }
        }
    }

    val game = Component {
        playing.onEach { _, adept ->
            adept.location {
                val x = floor(it.x).toInt()
                val y = floor(adept.bounds.last.min.y).toInt()
                val z = floor(it.z).toInt()
                val position = Position(x, y, z)
                val block = adept.blocks[position]
                val onLadder = block == BLOCK_LADDER

                if (onLadder || it.ground) {
                    if (onLadder) completed(completed.last + 1); attempts(attempts.last + 1)
                    val msg = if (onLadder) "You have ladder clutched" else "You have failed"
                    adept.send(msg)
                    disable()
                }
            }
        }
    }

    this.onDisabled { preGame.disable(); }
    this.onEnabled(preGame::enable)
    preGame.onDisabled(game::enable)
    game.onDisabled(preGame::enable)
}