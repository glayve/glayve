package com.gitlab.glayve.world

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.after
import com.gitlab.glayve.network.*
import com.gitlab.glayve.scheduler
import com.gitlab.glayve.util.Location
import kotlinx.coroutines.runBlocking
import java.io.ByteArrayInputStream
import java.io.DataInputStream
import java.nio.file.Files
import java.nio.file.Path
import java.util.zip.InflaterInputStream
import kotlin.math.ceil
import kotlin.time.Duration.Companion.seconds

interface Section {
    val isEmpty: Bool

    val blockData: ShortArray
    val chunkBlockData: ByteArray
    val emittedLight: ByteArray
    val skyLight: ByteArray
}
interface Chunk {
    val x: Int
    val z: Int
    val sections: Array<Section>
    val biomes: ByteArray
}


fun chunks(path: Path, location: Location, radius: Int = 2) = runBlocking {
    ArrayList<Chunk>().apply {
        val centerX = (location.x / 16).toInt()
        val centerZ = (location.z / 16).toInt()
        for (x in -radius..radius) for (z in -radius..radius) {
            val loaded = loadChunk(path, centerX + x, centerZ + z)
            if (loaded?.sections?.all { it.isEmpty } == true)
                if (path.toString().contains("bridge"))
                    println("No chunk at: $x, $z")
            if (loaded != null) add(loaded)
        }
    }
}

suspend fun loadChunk(region: Path, x: Int, z: Int): Chunk? = Files.newByteChannel(region).use {
    val read = it.toRead()
    it.position((4 * ((z and 0x1F) shl 5 or (x and 0x1F))).toLong())
    val offset = read.int() ushr 8
    if (offset != 0) {
        it.position(offset * 4096L)
        val length = read.int()
        if (read.byte() == 2.toByte()) {
            val data = ByteArray(length)
            read.bytes(data, data.size, 0)
            DataInputStream(InflaterInputStream(ByteArrayInputStream(data))).use { decompressed ->
                val level = decompressed.toRead().compound()["Level"] as TagCompound
                object : Chunk {
                    override val x = level["xPos"] as Int
                    override val z = level["zPos"] as Int
                    override val biomes = level["Biomes"] as ByteArray
                    override val sections = Array<Section>(16) { _ ->
                        object : Section {
                            override val isEmpty = true
                            override val blockData = ShortArray(0)
                            override val chunkBlockData = ByteArray(0)
                            override val emittedLight = ByteArray(0)
                            override val skyLight = ByteArray(0)
                        }
                    }
                    override fun equals(other: Any?): Boolean {
                        return other is Chunk && other.x == x && other.z == z
                    }
                }.apply {
                    (level["Sections"] as TagList?)?.forEach { nbt ->
                        val section = nbt as TagCompound
                        val meta = section["Data"] as ByteArray
                        val blocks = section["Blocks"] as ByteArray
                        val result = ShortArray(4096) { i ->
                            (blocks[i].toUByte().toInt() shl 4 or (meta[i / 2].toUByte().toInt() shr 4 * (i % 2) and 0xF)).toShort()
                        }
                        val packetData = ByteArray(4096 * 2)
                        var i = 0
                        result.forEach { block ->
                            packetData[i++] = block.toByte()
                            packetData[i++] = (block.toInt() ushr 8).toByte()
                        }
                        sections[(section["Y"] as Byte).toInt()] = object : Section {
                            override val isEmpty = result.all { block -> block < 1 }
                            override val emittedLight = section["BlockLight"] as ByteArray
                            override val skyLight = section["SkyLight"] as ByteArray
                            override val chunkBlockData = packetData
                            override val blockData = result
                        }
                    }
                }
            }
        } else throw IllegalStateException("Corrupt Region.")
    } else null
}

suspend fun loadSchematic(schematic: Path): Array<Chunk> {
    Files.newByteChannel(schematic).use {
        val tag = it.toRead().compound()
        val height = (tag["Height"] as Short).toInt()
        println("Height: $height")
        val width = (tag["Width"] as Short).toInt()
        val length = (tag["Length"] as Short).toInt()
        val blocks = tag["Blocks"] as ByteArray
        val meta = tag["Data"] as ByteArray

        val chunkWidth = ceil(width / 16.0).toInt()
        val chunkLength = ceil(width / 16.0).toInt()

        return Array(chunkWidth * chunkLength) { i ->
            object : Chunk {
                override val x = i % chunkWidth
                override val z = i / chunkWidth
                override val sections = Array<Section>(16) {
                    object : Section {
                        override val isEmpty = true
                        override val blockData = ShortArray(0)
                        override val emittedLight = ByteArray(0)
                        override val skyLight = ByteArray(0)
                        override val chunkBlockData = ByteArray(0)
                    }
                }
                override val biomes = ByteArray(256)
                override fun equals(other: Any?): Boolean {
                    return other is Chunk && other.x == x && other.z == z
                }
            }
        }
    }
}