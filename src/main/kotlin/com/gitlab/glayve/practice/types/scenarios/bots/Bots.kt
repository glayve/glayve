package com.gitlab.glayve.practice.types.scenarios.bots

import DIAMOND_SWORD
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Item
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.items.Item
import com.gitlab.glayve.items.SHARPNESS
import com.gitlab.glayve.network.*
import com.gitlab.glayve.practice.*
import com.gitlab.glayve.practice.games.*
import com.gitlab.glayve.util.minus
import com.gitlab.glayve.util.setDirection
import com.gitlab.glayve.util.toVelocity
import java.util.concurrent.ThreadLocalRandom.current
import kotlin.math.cos
import kotlin.math.sin
import kotlin.time.Duration.Companion.ZERO
import kotlin.time.Duration.Companion.seconds

fun Togglable.Bots(online: Adepts, players: Adepts, arena: Arena) {
//    val spectators = online.filter { id, _ -> id !in players }
//    Spectate(spectators, arena.spectators)
    Play(players, arena.red)
//
//    val scores = MutableTable<Difficulty, Int>()
//    val difficulty = Published(EASY)
//    val hits = Published(0)
//
//    Freeze(players)
//    CancelClicks(players)
//
//    val distance = Published(2.0)
//    val speed = Published(0.04)
//    val npcLocation = Published(arena.red.copy(x = 2756.5, z = 1125.5))
//
//    val preGame = phased {
//        online.onEach { _, adept ->
//            adept.board(4, just("Time Left:$RED 10 Second(s)"))
////            if (adept.id in players) adept.inventory[44] = Stack(switcher, 1)
//        }
//        onEnabled { hits(0) }
//    }
//
//
//    val postGame = phased {
//        onEnabled {
//            players.send {
//                title(
//                    "${RED}${hits.last}", "${GOLD}Hits",
//                    stay = 3.seconds, wax = .2.seconds
//                )
//            }
//        }
//
//        scheduler.after(3.seconds)() { this@phased.disable() }
//    }
//
//    val inGame = phased {
//        val timer = scheduler.every(1.seconds, ZERO)
//        timer {
//            if (10 - it <= 0) {
//                if (hits.last > (scores[difficulty.last] ?: 0)) {
//                    scores[difficulty.last] = scores[difficulty.last]!!.plus(hits.last)
//                }; postGame.enable(); disable()
//            }
//        }
//
//        players.onEach { _, adept ->
//            adept.inventory[44] = Stack.EMPTY
//
//            var i = 0.0
//            var y = 0.0
//            var multSpeed = 1
//            var height = 1
//
//            adept.board(4, timer.map { "Time Left:$RED ${10 - it} Second(s)" })
//
//            scheduler.every(1.ticks)() { tick ->
//                val location = adept.location.last
//                if (tick % 5 == 0) height *= -1
//                y -= height * (1.32 / 5)
//
//                current().apply {
//                    if (nextDouble(1.0) > 0.96) multSpeed *= -1
//                    i += multSpeed * (speed.last)
//                    val x = location.x + sin(i) * distance.last
//                    val z = location.z + cos(i) * distance.last
//                    val next = location.copy(x = x, y = location.y + y, z = z)
//                    npcLocation(next.setDirection(adept.location.last.toVelocity() - next.toVelocity()))
//                }
//            }
//        }
//
//        onDisabled { npcLocation(arena.red.copy(x = 2756.5, z = 1125.5)); }
//    }
//
////    GameDifficulty(viewers, {
////        difficulty(EASY)
////        speed(0.08)
////        distance(2.25)
////    }, {
////        difficulty(MEDIUM)
////        speed(0.10)
////        distance(2.5)
////    }, {
////        difficulty(HARD)
////        speed(0.125)
////        distance(2.75)
////    })
//
//    val restarter = Item(players, 399, 0, 1, displayName("Restart"), { adept, _ ->
//        if (inGame.enabled) {
//            adept.send("You have restarted.")
//            inGame.disable()
//            postGame.enable()
//        }; CONSUME
//    }) { _, _ -> PASS }
//
//    players.onEach { _, adept ->
//        adept.inventory[36] = Stack(
//            Item(
//                DIAMOND_SWORD, 0, 1, mapOf(
//                    "Unbreakable" to TRUE, "HideFlags" to (1 shl 2),
//                    "ench" to arrayOf(mapOf("id" to SHARPNESS, "lvl" to 1))
//                )
//            ), 1
//        )
//        adept.inventory[40] = Stack(restarter, 1)
//        NPC(adept, "Bot", npcLocation, onLeftClick = {
//            if (!postGame.enabled) {
//                val pitch = (current().nextFloat() - current().nextFloat()) * 0.2f + 1.0f
//                adept.sound("game.neutral.hurt", npcLocation.last, 1f, pitch)
//                adept.animation(it, 1)
//                adept.animation(it, 0)
//                hits(hits.last + 1)
//                if (preGame.enabled) preGame.disable()
//            }
//        })
//    }
//
//    online.onEach { _, adept ->
//        adept.board(0, just("${RED}${BOLD}Bot Practice"))
//        adept.board(1, date())
//        adept.board(2, just(" "))
//        adept.board(4, just("Time Left:$RED 10 Second(s)"))
//        adept.board(5, hits.map { "Hits: $RED$it Hit(s)" })
//        adept.board(6, difficulty.map { "Difficulty: ${it.name}" })
//        adept.board(7, just(""))
//        adept.board(8, just("Scenario:$RED B-Hop Trainer"))
//        adept.board(9, scores.watch(difficulty).map { "Personal Record: $RED${it ?: 0} Hit(s)" })
//        adept.board(10, just(" "))
//        adept.board(11, just(DOMAIN))
//        adept.gamemode(Gamemode.ADVENTURE)
//    }
//
//
//    this.onEnabled(preGame::enable)
//    preGame.onDisabled(inGame::enable)
//    postGame.onDisabled(preGame::enable)
    this.onEnabled {
        println("enabled")
    }
}

fun Float.fixRotation(): Float {
    return (((this % 360f) + 360f) % 360f)
}
