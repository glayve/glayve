@file:Suppress("NOTHING_TO_INLINE")
package com.gitlab.glayve.inventory

import com.gitlab.glayve.network.TagCompound

interface Item {
    val type: Int; val max: Byte
    val tags: TagCompound
}
val Item.material get() = (type ushr 16).toShort()
val Item.meta get() = (type and 0xFFFF).toShort()
fun Item(
    material: Short, meta: Short = 0, max: Byte = 64,
    nbt: TagCompound = emptyMap()
) = object : Item {
    override val type = (material.toInt() shl 16) or meta.toInt()
    override val max = max
    override val tags: Map<String, Any> = nbt
}