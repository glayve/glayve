package com.gitlab.glayve

import com.gitlab.ballysta.architecture.just

const val WHITE = "§f"
const val RED = "§c"
const val SILVER = "§7"
const val GRAY = "§8"
const val GREEN = "§a"
const val DARK_GREEN = "§2"
const val GOLD = "§6"
const val AQUA = "§b"
const val CYAN = "§3"
const val DARK_BLUE = "§1"
const val YELLOW = "§e"
const val PINK = "§d"
const val PURPLE = "§5"
const val BLUE = "§9"
const val BOLD = "§l"
const val ITALIC = "§o"
const val DOMAIN = "${RED}play.pyrelic.net"
val SPACER = just(" ")

val CHAT_COLORS = arrayOf(WHITE, GOLD, AQUA, AQUA, YELLOW, GREEN, PINK, GRAY, SILVER, CYAN, PURPLE, BLUE, DARK_BLUE, DARK_GREEN, RED, "§o")

typealias Color = Int

val Color.red get() = this shr 16 and 255
val Color.green get() = this shr 8 and 255
val Color.blue get() = this shr 0 and 255
inline operator fun Color.component1() = red //Why are these inline
inline operator fun Color.component2() = green
inline operator fun Color.component3() = blue
fun Color(red: Int, green: Int, blue: Int) =
    (red shl 16) or (green shl 8) or blue

const val COLORED_WHITE = 0
const val COLORED_ORANGE = 1
const val COLORED_MAGENTA = 2
const val COLORED_LIGHT_BLUE = 3
const val COLORED_YELLOW = 4
const val COLORED_LIME = 5
const val COLORED_PINK = 6
const val COLORED_GRAY = 7
const val COLORED_SILVER = 8
const val COLORED_CYAN = 9
const val COLORED_PURPLE = 10
const val COLORED_BLUE = 11
const val COLORED_BROWN = 12
const val COLORED_GREEN = 13
const val COLORED_RED = 14
const val COLORED_BLACK = 15