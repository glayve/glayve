package com.gitlab.glayve.inventory

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.Adept
import com.gitlab.glayve.PASS
import com.gitlab.glayve.network.*

interface Button {
    val onClick: Event<() -> (Bool)>
//    fun name(text: Any)
//    fun count(amount: Number)
    val type: PublishSubject<Id>
    val tags: PublishSubject<TagCompound>
    val count: PublishSubject<Int>
}
interface Slots {
    operator fun set(slot: Slot, item: Button.() -> (Unit))
    operator fun set(slots: Iterable<Int>, item: Button.() -> (Unit))
        = slots.forEach { set(it.toSlot(), item) }
}

val MutableTable<Id, Adept>.display get(): Toggled.(
    Container, PublishedObservable<String>,
    Togglable.(Adept, Slots) -> (Unit)
) -> (Unit) = { type, name, block -> Menu(this@display, type, name, block) }
fun Toggled.Menu(
    viewers: MutableTable<Id, Adept>,
    type: Container, name: PublishedObservable<String>,
    block: Togglable.(Adept, Slots) -> (Unit)
) { viewers.onEach { _, viewer ->
        val inventory = viewer.inventory
        onDisabled { //this listener goes first to allow simulated post event.
            viewer.inventory.type(Container.PLAYER)
            viewer.inventory.clear(45..98)
        }
        block(viewer, object : Slots {
            override fun set(slot: Slot, item: Button.() -> Unit) {
                val instance = object : Item {
                    override var type = 0
                    override var max = 64.toByte()
                    override var tags = emptyMap<String, Tag>()
                }
                val button = object : Button {
                    override val onClick
                        get(): Event<() -> Bool> = {
                             inventory.onClick { clicked ->
                                if (clicked == (slot + 45).toByte()) it() else PASS
                            }
                            inventory.onShiftClick { clicked ->
                                if (clicked == (slot + 45).toByte()) it() else PASS
                            }
                            inventory.onSwap { _, _ -> PASS }
                            inventory.onDrop { _, _ -> PASS }
                        }

                    override val type = Published(instance.type)
                    override val tags = Published(instance.tags)
                    override val count = Published(1)
                }
                fun update(count: Int) {
                    inventory[slot + 45] = Stack(instance, count.toByte())
                }
                button.type {
                  //  println(instance.material)
                    instance.type = it
                    update(button.count.last)
                }
                button.tags { instance.tags = it; update(button.count.last) }
                button.count { update(it) }
                item(button); disable()
            }
        })
        name { inventory.title("""{"text": "$it"}""") }
        onEnabled { inventory.type(type) }
        viewer.onWindowClose { disable() }
    }
}

internal val Button.display get() = tags.map(
    { it["display"] as TagCompound? ?: emptyMap() },
    { tags.last + ("display" to it) }
)
val Button.tags get() = tags.map(
    { it["HideFlags"] as Int? ?: 0 },
    { tags.last + ("HideFlags" to it) }
)
val Button.name get() = display.map(
    { it["Name"] as String ?: "" },
    { display.last + ("Name" to it) }
)
val Button.lore get() = display.map(
    { it["Lore"] as Array<out String> ?: emptyArray() },
    { display.last + ("Lore" to it) }
)
val Button.meta get() = type.map(
    { it and 0xFFFF },
    { (type.last and 0xFFFF.inv()) or it }
)
val Button.material get() = type.map(
    { it ushr 16 },
    { (type.last and 0xFFFF) or (it shl 16) }
)
