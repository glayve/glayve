package com.gitlab.glayve.practice

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import org.apache.commons.lang3.StringUtils.chop
import java.util.*
import kotlin.text.StringBuilder

interface Team {
    var leader: Channel
    val members: MutableHolder<Channel>
    val open: PublishSubject<Boolean>
    fun message(msg: String)
}

fun Toggled.Teams(online: Channels) {
    val teams = MutableTable<UUID, Team>()
    val players = MutableTable<Channel, UUID>()

    teams.onEach { id, team ->
        team.members.onEach { _, member ->
            onDisabled {
                players[member] = null
                team.message("§c${member.name}§7 has left the team.")
            }
            onEnabled {
                players[member] = id
                team.message("§c${member.name}§7 has joined the team.")
            }
        }

        onDisabled {
            team.message("§cThe team has been disbanded.")
            team.members.values.forEach { players[it] = null }
            players[team.leader] = null
        }
        onEnabled {
            players[team.leader] = id
            team.message("§aYou have created a team.")
        }
    }

    online.onEach { id, channel ->
        channel.handle("/t") {
            val tid = players[channel]

            match("create") {
                if (tid != null) channel.send("§cYou are already in a team.")
                else teams[UUID.randomUUID()] = object : Team {
                    override var leader = channel
                    override val members = MutableHolder<Channel>()
                    override val open = Published(false)
                    override fun message(msg: String) {
                        leader.send(msg)
                        members.send(msg)
                    }
                }
            }

            match("invite") {

            }

            match("join") {

            }

            match("leave") {
                if (tid != null) {
                    val team = teams[tid]!!

                    if (team.leader != channel) {
                        team.members[channel] = null
                    } else {
                        var transfer: Channel? = null
                        team.members.values.forEach { if (transfer == null) transfer = it }
                        team.leader = transfer!!
                    }
                }
            }

            match("leader|transfer") {
                if (tid != null) {
                    val team = teams[tid]!!
                    if (team.leader == channel) {
                        val target = player(online, "Can't find player.")
                        if (target != channel) {
                            if (team.members[target] != null) {
                                team.members[target] = null
                                team.leader = target
                            } else channel.send("$RED${target.name} ${SILVER}is not on your team.")
                        } else channel.send("${RED}Can't leader yourself.")
                    } else channel.send("${RED}You are not the leader.")
                }
            }

            match("kick") {
                if (tid != null) {
                    val team = teams[tid]!!
                    if (team.leader == channel) {
                        val target = player(online, "Can't find player.")
                        if (target != channel) {
                            team.members[target] = null
                        } else channel.send("${RED}Can't kick yourself.")
                    } else channel.send("${RED}You are not the leader.")
                }
            }

            match("disband") {
                if (tid != null) {
                    val team = teams[tid]!!
                    if (team.leader == channel) {
                        teams[tid] = null
                    } else channel.send("§cYou are not the leader.")
                } else channel.send("§cYou are not in a team.")
            }

            if (tid == null) channel.send("§cYou are not in a team.") else {
                val team = teams[tid]!!
                val open = if (team.open.last) "§a§lOpen" else "§4§lClosed"
                channel.send("§c§lParty §7- $open")
                channel.send("§cLeader§7: §6${team.leader.name}")
                channel.send("§cMembers§7(§a${team.members.size}§7): ")
                channel.send(chop(StringBuilder().apply {
                    team.members.values.forEach { append("§c${channel.name}§7, ") }
                }.toString()))
            }
        }

        onDisabled { if (players[channel] != null) {
                val members = teams[players[channel]!!]!!.members
                members.remove(channel)
                members.send("§c${channel.name}§7 has left the team.")
            }
        }
    }
}