package com.gitlab.glayve.world

import com.gitlab.glayve.util.Bounds

interface Block {
    val type: Int
    val hardness: Byte
    val bounds: Bounds
    val sound: String
}
val Block.material get() = (type ushr 4).toShort()
val Block.meta get() = (type and 0xF).toShort()
val Block.friction get() = blockFrictionFactorMap[material.toInt()]


fun Block(type: Int, hardness: Byte, bounds: Bounds, sound: String) = object : Block {
    override val type = type
    override val hardness = hardness
    override val bounds = bounds
    override val sound = sound
}

fun Block(material: Short, meta: Short, hardness: Byte, bounds: Bounds, sound: String)
    = Block((material.toInt() shl 4) or meta.toInt(), hardness, bounds, sound)


val BLOCK_STONE = Block(1, 0, 0, Bounds.FULL, "")
val BLOCK_SKULL = Array(6) {
    Block(144, it.toShort(), 0, when (it) {
        0, 1 -> Bounds(0.25, 0.0, 0.25, 0.75, 0.5, 0.75)
        2 -> Bounds(0.25, 0.25, 0.5, 0.75, 0.75, 1.0)
        3 -> Bounds(0.25, 0.25, 0.0, 0.75, 0.75, 0.5)
        4 -> Bounds(0.5, 0.25, 0.25, 1.0, 0.75, 0.75)
        else -> Bounds(0.0, 0.25, 0.25, 0.5, 0.75, 0.75)
    }, "")
}
val BLOCK_IRON = Block(42, 0, 0, Bounds.FULL, "")
val BLOCK_OBSIDIAN = Block(49, 0, 0, Bounds.FULL, "")
val BLOCK_AIR = Block(0, 0, 0, Bounds.NONE, "")
val BLOCK_LADDER = Block(65, 0, 0, Bounds.NONE, "")
val BLOCK_WOOL = Block(35, 0, 0, Bounds.FULL, "")
val BLOCK_ADOBE = Array(16) {
    Block(159, it.toShort(), 0, Bounds.FULL, "")
}
val BLOCK_RED_STAINED_CLAY = Block(159, 14, 0, Bounds.FULL, "")
val BLOCK_BLUE_STAINED_CLAY = Block(159, 11, 0, Bounds.FULL, "")
val BLOCK_WOODEN_PLANK = Block(5, 0, 0, Bounds.FULL, "")
val BLOCK_COBBLESTONE = Block(4, 0, 0, Bounds.FULL, "")
val BLOCK_WATER_STATIC = Array(16) { meta ->
    Block(9, meta.toShort(), 0, Bounds.NONE, "")
}
val BLOCK_WATER_FLOWING = Array(16) { meta ->
    Block(8, meta.toShort(), 0, Bounds.NONE, "")
}
val BLOCK_LAVA_STATIC = Array(16) { meta ->
    Block(11, meta.toShort(), 0, Bounds.NONE, "")
}
val BLOCK_LAVA_FLOWING = Array(16) { meta ->
    Block(10, meta.toShort(), 0, Bounds.NONE, "")
}
val BLOCK_WATER = BLOCK_WATER_FLOWING + BLOCK_WATER_STATIC
val BLOCK_LAVA = BLOCK_LAVA_FLOWING + BLOCK_LAVA_STATIC
val BLOCK_FLOWING = BLOCK_WATER_FLOWING + BLOCK_LAVA_FLOWING
val BLOCK_STATIC = BLOCK_WATER_STATIC + BLOCK_LAVA_STATIC