package com.gitlab.glayve.anticheat

import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.onEach
import com.gitlab.glayve.*
import com.gitlab.glayve.anticheat.checks.combat.ClickCheck
import com.gitlab.glayve.anticheat.checks.combat.HitboxCheck
import com.gitlab.glayve.anticheat.checks.combat.KillAuraCheck
import com.gitlab.glayve.anticheat.checks.move.MoveCheck
import com.gitlab.glayve.anticheat.checks.net.DerpCheck
import com.gitlab.glayve.anticheat.checks.net.TimerCheck
import com.gitlab.glayve.world.MutableWorld
import kotlin.random.Random

typealias Tags = List<Pair<String, String>>

fun Toggled.AntiCheat(
    players: Adepts,
    world: MutableWorld
) {
    players.onEach { _, adept ->
        adept.handle("/anticheat"){
            val subCommand = this.string()
            when(subCommand?.lowercase()){
                "gmc" -> {
                    adept.gamemode(Gamemode.CREATIVE)
                }
                "gms" -> {
                    adept.gamemode(Gamemode.SURVIVAL)
                }
            }
        }
    }

    CombatChecks(players, world)
    NetChecks(players, world)
    MoveChecks(players, world)

    println("Initialization anti-cheat completed")
}

fun Toggled.CombatChecks(
    players: Adepts,
    world: MutableWorld
) {
    KillAuraCheck(players)
    HitboxCheck(players)
    ClickCheck(players)
}

fun Toggled.NetChecks(
    players: Adepts,
    world: MutableWorld
) {
    TimerCheck(players)
    DerpCheck(players)
}

fun Toggled.MoveChecks(
    players: Adepts,
    world: MutableWorld
) {
    MoveCheck(players, world)
}

// TODO: make it really working
fun Adept.flag(
    type: CheckType,
    message: String,
    vl: Int = 1,
    data: Tags = emptyList()
) {
    this.send("§c[AntiCheat] §b${this.name} §7failed §f${type.name}§7: §8$message | vl: +$vl | ${data.format()} | #${Random.nextInt()}")
    println("[Flag] ${this.name} failed ${type.name}: $message | +$vl | ${data.format()}")
}

// format the tags
fun Tags.format() = if (isNotEmpty())
    joinToString(separator = ", ", prefix = "| ") {
        "${it.first}: ${it.second}"
    }
else
    ""