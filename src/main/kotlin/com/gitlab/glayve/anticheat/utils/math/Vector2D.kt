package com.gitlab.glayve.anticheat.utils.math

data class Vector2D(
    val x: Double,
    val z: Double
){
    companion object {
        val Zero = Vector2D(0.0, 0.0)
    }
}