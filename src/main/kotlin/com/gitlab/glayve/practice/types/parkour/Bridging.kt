package com.gitlab.glayve.practice.types.parkour

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.items.ItemBlock
import com.gitlab.glayve.network.Position
import com.gitlab.glayve.practice.games.Arena
import com.gitlab.glayve.practice.show
import com.gitlab.glayve.util.distance
import com.gitlab.glayve.util.toPosition
import com.gitlab.glayve.world.BLOCK_AIR
import com.gitlab.glayve.world.BLOCK_IRON
import com.gitlab.glayve.world.BLOCK_WOOL
import kotlin.time.Duration.Companion.seconds

// add checkpoint in middle
// block place sounds
//
fun Togglable.Bridging(players: Adepts, playing: Adepts, arena: Arena) {
    val spectators = players - playing
    spectators.show(playing)

    val wools = MutableTable<Position, Adept>()
    wools.onEach { position, adept ->
        scheduler.after(.75.seconds)() { wools[position] = null }
        onDisabled { adept.blocks[position] = BLOCK_AIR }
    }
    val wool = ItemBlock(playing, BLOCK_WOOL, emptyMap()) { adept, position ->
        adept.blocks[position] = BLOCK_WOOL
        wools[position] = adept; CONSUME
    }

    players.onEach { _, adept -> adept.gamemode(Gamemode.ADVENTURE) }
    val preGame = Component {
        playing.onEach { _, adept ->
            adept.location(arena.red)
            adept.inventory[36] = Stack(wool, 64)
        }
        wools.onAdded { _, _ -> disable() }
    }

    val game = Component {
        val timer = scheduler.every(1.seconds).publish(0)()
        val distance = Published(0)
        players.onEach { _, adept ->
            adept.board(0, just("Pyrelic"))
            adept.board(1, timer.map { "Timer: $it" })
            adept.board(2, distance.map { "Distance: $it" })
            adept.gamemode(Gamemode.SURVIVAL)
        }

        playing.onEach { _, adept ->
            adept.location.onNext { _, to ->
                distance(to.distance(arena.blue!!).toInt())
                val position = to.toPosition()
                val block = position.copy(y = (position.y - 0.5).toInt())
                if (adept.blocks[block] == BLOCK_IRON || to.y < arena.red.y - 7) {
                    val msg = if (to.y < arena.red.y - 7) "You have failed." else "It took you ${timer.last} seconds."
                    players.send(msg)
                    preGame.enable()
                    disable()
                }
            }
        }
    }

    this.onDisabled { preGame.disable(); game.disable() }
    this.onEnabled(preGame::enable)
    game.onDisabled(preGame::enable)
    preGame.onDisabled(game::enable)
}