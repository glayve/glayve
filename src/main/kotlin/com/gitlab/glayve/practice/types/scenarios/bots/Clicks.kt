package com.gitlab.glayve.practice.types.scenarios.bots

import DIAMOND_SWORD
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.items.Item
import com.gitlab.glayve.items.SHARPNESS
import com.gitlab.glayve.network.*
import com.gitlab.glayve.practice.*
import com.gitlab.glayve.practice.games.*
import java.util.concurrent.ThreadLocalRandom.current
import kotlin.time.Duration.Companion.ZERO
import kotlin.time.Duration.Companion.seconds

// time varies per difficulty
// spawn in with sword
fun Togglable.Clicks(online: Adepts, players: Adepts, arena: Arena) {
    val spectators = online.filter { id, _ -> id !in players }
    val npcLocation = Published(arena.red.copy(z = arena.red.z - 2.0, yaw = 0f))
    spectators.show(players)
    Spectate(spectators, arena.spectators)
    Play(players, arena.red)
    CancelClicks(players)

    val scores = MutableTable<Difficulty, Int>()
    val difficulty = Published(EASY)
    val time = difficulty.map {
        when (it) {
            HARD -> 20; MEDIUM -> 15; else -> 10;
        }
    }
    val cps = Published(0)
    val hits = Published(0)

    val preGame = phased {
        online.onEach { _, adept ->
            adept.board(3, just("Time Left: $RED${time.last} Second(s)"))
        }

        DifficultySelector(players, 44, EASY, MEDIUM, HARD)(difficulty)

        onEnabled { hits(0); cps(0) }
        hits { if (hits.last > 0) disable() }
    }

    val postGame = phased {
        onEnabled {
            players.send {
                title(
                    "${RED}${hits.last}", "${GOLD}Hits",
                    stay = 3.seconds, wax = .2.seconds
                )
            }
        }

        scheduler.after(3.seconds)() { this@phased.disable() }
    }

    var lastHits = 0
    val inGame = phased {
        players.onEach { _, adept ->
            adept.inventory[44] = Stack.EMPTY
        }

        val timer = scheduler.every(1.seconds, ZERO)
        timer {
            cps(hits.last - lastHits)
            lastHits = hits.last
            if ((time.last - it) == 0) {
                postGame.enable()
                disable()
            }
        }
        online.onEach { _, adept ->
            adept.board(3, timer.map { "Time Left: $RED${time.last - it} Second(s)" })
        }

        onDisabled {
            if (hits.last > (scores[difficulty.last] ?: 0)) {
                scores[difficulty.last] = hits.last
            }
        }
    }


    val restarter = Item(players, 399, 0, 1, displayName("Restart"), { adept, _ ->
        if (inGame.enabled) {
            adept.send("You have restarted.")
            inGame.disable()
            postGame.enable()
        }; CONSUME
    }) { _, _ -> PASS }

    players.onEach { _, adept ->
        adept.inventory[40] = Stack(restarter, 1)
        adept.inventory[36] = Stack(
            com.gitlab.glayve.inventory.Item(
                DIAMOND_SWORD, 0, 1, mapOf(
                    "Unbreakable" to TRUE, "HideFlags" to (1 shl 2),
                    "ench" to arrayOf(mapOf("id" to SHARPNESS, "lvl" to 1))
                )
            ), 1
        )

        adept.location {
            if (it.y < (arena.red.y - 2)) adept.location(arena.red)
        }

        NPC(adept, "Bot", npcLocation, onLeftClick = {
            if (!postGame.enabled) {
                hits(hits.last + 1)
                online.send {
                    animation(it, 1)
                    val pitch = (current().nextFloat() - current().nextFloat()) * 0.2f + 1.0f
                    sound("game.neutral.hurt", location.last, 1f, pitch)
                }
            }
        })
    }

    online.onEach { _, adept ->
        adept.board(0, just("${RED}${BOLD}Bot Practice"))
        adept.board(1, date())
        adept.board(2, just(" "))
        adept.board(4, hits.map { "Hits: $RED$it Hit(s)" })
        adept.board(5, difficulty.map { "Difficulty: ${it.name}" })
        adept.board(6, just(""))
        adept.board(7, just("Scenario:$RED CPS Trainer"))
        adept.board(8, scores.watch(difficulty).map { "Personal Record: $RED${it ?: 0} Hit(s)" })
        adept.board(9, just(" "))
        adept.board(10, just(DOMAIN))
        adept.gamemode(Gamemode.ADVENTURE)
        adept.inventory.onClick { it !in 0..44 }
        adept.inventory.onShiftClick { it !in 0..44 }
        adept.inventory.onSwap { from, to -> from !in 0..44 || to !in 0..44 }
    }

    this.onEnabled{
        println("enabled"); preGame.enable()

    }
    preGame.onDisabled(inGame::enable)
    postGame.onDisabled(preGame::enable)
}