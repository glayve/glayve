package com.gitlab.glayve

import com.github.exerosis.mynt.base.Address
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.network.PlayerComponent
import com.gitlab.glayve.network.largest
import com.gitlab.glayve.practice.Practice
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicInteger
import kotlin.time.Duration.Companion.seconds

var DEBUG = true
val EID = AtomicInteger(1)

val scheduler = Executors.newScheduledThreadPool(1)!!

fun main() = Unit.apply { scheduler.submit { Component {
    System.setErr(System.out)
    Runtime.getRuntime().addShutdownHook(Thread { disable() })
    val channels = MutableTable<Int, Channel>()
    PlayerComponent(scheduler, Address(25577), online = false) { _, _ ->
        """{
            "version": { "name": "1.8.8", "protocol": 47 },
            "players": { "max": 100, "online": ${channels.size} },
            "description": { "text": "Glayve Closed Alpha" }
        }"""
    }.onEach { _, channel ->
        val info = "${channel.name}@${channel.address.hostName} - ${channel.version}"
        onEnabled { println("[Glayve] $info is trying to connect.") }
        onEnabled { channels[channel.id] = channel }
        onDisabled { channels[channel.id] = null }
        onDisabled { println("[Glayve] $info lost connection or quit.") }
    }

    val subject = Published(10)
    channels.onEach { _, channel ->
        channel.handle("/test") {
            val it = Component {}
            it.onEnabled { it.disable() }
            Component {
                onEnabled { println("Enabled") }
                scheduler.after(3.seconds)() { disable() }
                subject { if (it > 100) disable() }
                onEnabled { println("Enabled") }
            }.invoke(true)
        }
        channel.handle("/set") {
            subject(200)
        }
        channel.handle("/debug") { DEBUG = !DEBUG }
        channel.handle("/gc") { System.gc(); channels.send("Forced Garbage Collection") }
        channel.handle("/exit") { channel.kick("Exited") }
        channel.handle("/stop") { this@Component.disable() }
        channel.handle("/players") { channel.send("Players: ${channels.size}") }
        channel.handle("/largest") { channel.send("Largest Packet: $largest") }
    }

    Practice(channels)

    onEnabled { println("[Glayve] Enabled") }
    onDisabled { println("[Glayve] Disabled") }
}.enable() } }