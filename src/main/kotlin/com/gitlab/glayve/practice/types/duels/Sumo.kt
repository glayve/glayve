package com.gitlab.glayve.practice.types.duels

import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.items.DefaultHit
import com.gitlab.glayve.practice.Freeze
import com.gitlab.glayve.practice.Play
import com.gitlab.glayve.practice.Spectate
import com.gitlab.glayve.practice.games.Arena
import com.gitlab.glayve.practice.show
import com.gitlab.glayve.util.toPosition
import kotlin.time.Duration.Companion.seconds

fun Togglable.Sumo(online: Adepts, red: Adepts, blue: Adepts, arena: Arena) {
    val players = red + blue
    val spectators = online.filter { id, _ -> id !in players }
    Spectate(spectators, arena.spectators)
    online.show(players)
    Play(red, arena.red)
    Play(blue, arena.blue!!)

    red.onEach { _, redAdept ->
        blue.values()?.board(6, just("${RED}${redAdept.name}"))
    }

    blue.onEach { _, blueAdept ->
        red.values()?.board(6, just("${RED}${blueAdept.name}"))
    }

    val timer = scheduler.every(1.seconds)
    online.onEach { _, adept ->
        adept.board(0, just("${RED}${BOLD}Multiplayer"))
        adept.board(1, date())
        adept.board(2, SPACER)
        adept.board(3, timer.map { "${RED}Time: $WHITE${String.format("%02d:%02d", it / 60, it % 60)}"})
        adept.board(4, SPACER)
        adept.board(5, just("Opponent:"))
        adept.board(7, SPACER)
        adept.board(8, just("Game:$RED Sumo Duel"))
        adept.board(9, just("Winstreak:$RED 0"))
        adept.board(10, SPACER)
        adept.board(11, just(DOMAIN))
    }

    val preGame = phased {
        val preTimer = scheduler.every(1.seconds)
        preTimer {
            val time = (5 - it)
            if (time == 0) disable()
        }; Freeze(players)
    }

    val game = phased {
        DefaultHit(online) { _, _ -> 0f }
        players.onEach { id, adept ->
            adept.location {
                val position = it.toPosition()
                val block = position.copy(y = (position.y - 0.5).toInt())
                if (adept.blocks[block].type == 16) {
                    this@Sumo.disable()
                }
            }
        }
    }


    onDisabled(game::disable)
    onEnabled(preGame::enable)
    preGame.onDisabled(game::enable)
//    game.onDisabled(this::disable)
}
