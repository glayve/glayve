package com.gitlab.glayve.practice.types.duels

import DIAMOND_SWORD
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Item
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.items.*
import com.gitlab.glayve.network.TRUE
import com.gitlab.glayve.network.addEffect
import com.gitlab.glayve.network.sound
import com.gitlab.glayve.practice.*
import com.gitlab.glayve.practice.games.Arena
import com.gitlab.glayve.util.Bounds
import com.gitlab.glayve.util.Velocity
import com.gitlab.glayve.util.plus
import com.gitlab.glayve.world.Block
import com.gitlab.glayve.world.MutableWorld
import com.gitlab.glayve.world.material
import java.util.concurrent.ThreadLocalRandom.current
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

fun Togglable.OpDuels(world: MutableWorld, online: Adepts, red: Adepts, blue: Adepts, arena: Arena) {
    val players = red + blue
    val spectators = online.filter { id, _ -> id !in players }
    Play(red, arena.red)
    Play(blue, arena.blue!!)
    Spectate(spectators, arena.spectators)
    online.show(players)
    players.onEach { id, adept ->
        adept.addEffect(id, Effect.DIG_DOWN.toByte(), -1, -1)
    }

    val rod = ItemRod(online, onHitPlayer = { _, _ -> 0f })
    val arrow = Item(262, 0, 1, emptyMap())

    val regen = online.Regeneration(2, 5.seconds)
    val potionRegen = online.Regeneration(0, 30.seconds)
    val speed = online.Speed(1, 1.minutes)
    val absorption = online.Absorption(0, 120.seconds)
    val apple = ItemFood(online, 322, onConsume = {
        it.effects += regen
        it.effects += absorption
    })
    val speedPotion = ItemSplash(online, 373, 16418, emptyMap()) { adept, power ->
        if (adept.id in players) { adept.effects -= speed; adept.effects += speed }
    }
    val regenPotion = ItemSplash(online, 373, 16417, emptyMap()) { adept, power ->
        if (adept.id in players) { adept.effects -= potionRegen; adept.effects += potionRegen }
    }

    players.onEach {_, adept ->
        adept.using { println("Using: $it") }
    }

    lateinit var flints: Array<Item>
    flints = Array(3) {
        Item(online, 259, (it * 22).toShort(), 1, onUse = { adept, interaction ->
            if (interaction.clicked != null) {
                val clicked = interaction.clicked
                if (world[interaction.clicked].material != 51.toShort()) {
                    world[clicked] = Block(51, 0, 0, Bounds.NONE, "")
                    val location = clicked.toLocation().plus(Velocity(clicked.x + 0.5, clicked.y + 0.5, clicked.z + 0.5))
                    online.send {
                        sound("fire.ignite", location, 1.0f, current().nextFloat() * 0.4f + 0.8f)
                    }
                    val selected = adept.selected.last
                    if (it >= 2) {
                        adept.inventory[selected] = null
                        val pitch = 0.8f + current().nextFloat() * 0.4f
                        online.send("random.break", adept.location.last, 0.8f, pitch)

                    } else adept.inventory[selected] = Stack(flints[it + 1], 1)
                    CONSUME
                } else PASS
            } else PASS
        })
    }

    Fire(world, online, players)

    val sword = ItemSword(online, DIAMOND_SWORD, mapOf(
        "ench" to arrayOf(mapOf("id" to SHARPNESS, "lvl" to 5)),
        "Unbreakable" to TRUE, "HideFlags" to (1 shl 2)
    ), bonus = 6.25f) { _, _ -> 7f }
    val bow = ItemBow(online, mapOf(
        "Unbreakable" to TRUE, "HideFlags" to (1 shl 2),
        "ench" to arrayOf(mapOf("id" to 48, "lvl" to 4))
    ), bonus = 2f, onHitPlayer = { _, _ -> 2f })
    DefaultHit(online)
    val helm = Item(310, 0, 1, mapOf("ench" to arrayOf(mapOf("id" to 0.toShort(), "lvl" to 3.toShort()))))
    val chest = Item(311, 0, 1, mapOf("ench" to arrayOf(mapOf("id" to 0.toShort(), "lvl" to 4.toShort()))))
    val legs = Item(312, 0, 1, mapOf("ench" to arrayOf(mapOf("id" to 0.toShort(), "lvl" to 3.toShort()))))
    val boots = Item(313, 0, 1, mapOf("ench" to arrayOf(mapOf("id" to 0.toShort(), "lvl" to 3.toShort()))))


    fun opItems(adept: Adept) {
//        uses[adept] = 0
        val onRed = red[adept.id] != null
        val spawn = if (onRed) arena.red else arena.blue
        adept.health(20f)
        adept.location(spawn)
        adept.inventory.clear()
        adept.inventory[5] = Stack(helm, 1)
        adept.inventory[6] = Stack(chest, 1)
        adept.inventory[7] = Stack(legs, 1)
        adept.inventory[8] = Stack(boots, 1)
        adept.inventory[36] = Stack(sword, 1)
        adept.inventory[37] = Stack(rod, 1)
        adept.inventory[38] = Stack(bow, 1)
        adept.inventory[9] = Stack(ITEM_ARROW, 20)
        adept.inventory[39] = Stack(flints[0], 1)
        adept.inventory[40] = Stack(apple, 6)
        adept.inventory[41] = Stack(speedPotion, 1)
        adept.inventory[42] = Stack(speedPotion, 1)
        adept.inventory[43] = Stack(regenPotion, 1)
        adept.inventory[44] = Stack(regenPotion, 1)
    }

    val preGame = phased {
        val timer = scheduler.every(1.seconds)
        timer { if ((1 - it) == 0) disable() }
        players.onEach { _, adept ->
            adept.gamemode(Gamemode.SURVIVAL)
            opItems(adept)
        }

        Freeze(players)
    }

    val inGame = phased {
        players.onEach { id, adept ->
            adept.health {
                if (it < 1) {
                    online.send("${adept.name}$SILVER has died.")
                    this@phased.disable()
                }
            }
        }

        onDisabled { this@OpDuels.disable() }
    }

    red.onEach { _, redAdept ->
        blue.values()?.board(6, just("${RED}${redAdept.name}"))
    }

    blue.onEach { _, blueAdept ->
        red.values()?.board(6, just("${RED}${blueAdept.name}"))
    }

    val timer = scheduler.every(1.seconds)
    online.onEach { _, adept ->
        adept.board(0, just("${RED}${BOLD}Multiplayer"))
        adept.board(1, date())
        adept.board(2, SPACER)
        adept.board(3, timer.map { "${RED}Time: $WHITE${String.format("%02d:%02d", it / 60, it % 60)}"})
        adept.board(4, SPACER)
        adept.board(5, just("Opponent:"))
        adept.board(7, SPACER)
        adept.board(8, just("Game:$RED OP Duels"))
        adept.board(9, just("Winstreak:$RED 0"))
        adept.board(10, SPACER)
        adept.board(11, just(DOMAIN))
        adept.inventory.onClick { it in 0..8 || it.toInt() == -2 }
        adept.inventory.onDrop { slot, stack -> PASS }
        adept.inventory.onShiftClick { it in 0..8 }
        adept.inventory.onCollect { slots, amount -> PASS }
    }

    this.onEnabled(preGame::enable)
    preGame.onDisabled(inGame::enable)
}