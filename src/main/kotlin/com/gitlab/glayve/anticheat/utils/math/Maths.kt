package com.gitlab.glayve.anticheat.utils.math

import com.gitlab.glayve.util.Vector
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.sin

fun floorToInt(value: Double): Int {
    val i = value.toInt()
    return if (value < i.toDouble()) i - 1 else i
}

fun ceilToInt(value: Double): Int {
    val i = value.toInt()
    return if (value > i.toDouble()) i + 1 else i
}

fun distanceOfAngles(from: Float, to: Float): Float {
    val distance = abs(from - to) % 360.0f
    return if (distance > 180f) 360f - distance else distance
}

infix fun Float.angle(to: Float): Float {
    val distance = abs(this - to) % 360.0f
    return if (distance > 180f) 360f - distance else distance
}

// first = yaw
// second = pitch
fun Pair<Float, Float>.getDirection(): Vector {
    val yRotation = Math.toRadians(second.toDouble())
    val xRotation = Math.toRadians(first.toDouble())
    val xz = cos(yRotation)
    return Vector(
        -xz * sin(xRotation),
        -sin(yRotation),
        xz * cos(xRotation)
    )
}