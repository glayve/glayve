package com.gitlab.glayve.practice.types.duels

import DIAMOND_PICKAXE
import DIAMOND_SWORD
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Item
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.inventory.add
import com.gitlab.glayve.items.*
import com.gitlab.glayve.network.*
import com.gitlab.glayve.practice.Freeze
import com.gitlab.glayve.practice.Play
import com.gitlab.glayve.practice.Spectate
import com.gitlab.glayve.practice.games.Arena
import com.gitlab.glayve.practice.show
import com.gitlab.glayve.util.Velocity
import com.gitlab.glayve.util.distance
import com.gitlab.glayve.util.toPosition
import com.gitlab.glayve.world.BLOCK_ADOBE
import com.gitlab.glayve.world.material
import java.nio.file.Paths
import kotlin.time.Duration.Companion.seconds

val redTag = mapOf("display" to mapOf("color" to 14))
val blueTag = mapOf("display" to mapOf("color" to 11))

fun Togglable.Bridges(online: Adepts, red: Adepts, blue: Adepts, arena: Arena) {
    val players = red + blue
    val spectators = online.filter { id, _ -> id !in players }
    Spectate(spectators, arena.spectators)
    Play(red, arena.red)
    Play(blue, arena.blue!!)
    online.show(players)
    online.onEach { id, adept ->
        adept.addEffect(id, Effect.DIG_DOWN.toByte(), -1, -1)
    }

    val redClay = ItemBlock(online, BLOCK_ADOBE[COLORED_RED], emptyMap()) { _, (x, y, z) ->
        x > 90 || x < 46 || y >= 60
    }
    val blueClay = ItemBlock(online, BLOCK_ADOBE[COLORED_BLUE], emptyMap()) { _, (x, y, z) ->
        x > 90 || x < 46 || y >= 60
    }
    val table = importTable(Paths.get("diamond_pickaxe.times"))
    val sword = ItemSword(online, DIAMOND_SWORD, mapOf(
        "Unbreakable" to TRUE, "HideFlags" to (1 shl 2)
    ), onHit = { source, target ->
        7f
//        if (source.id in red != target.id in red) 7f else -1f
    })
    val pickaxe = ItemTool(
        online, DIAMOND_PICKAXE, tags = mapOf(
            "Unbreakable" to TRUE, "HideFlags" to (1 shl 2)
        )
    ) { adept, position ->
        if (position.x > 90 || position.x < 46 || position.y >= 60) -1
        else {
            if (adept.blocks[position].material == BLOCK_ADOBE[0].material) {
                val time = table[adept.blocks[position].material.toInt()]
                time
            } else -1
        }
    }
    val bow = ItemBow(online, mapOf(
        "Unbreakable" to TRUE, "HideFlags" to (1 shl 2)
    ), onRelease = {
        transient {
            scheduler.after(3.5.seconds)() {
                it.inventory.add(Stack(ITEM_ARROW, 1))
                disable()
            }
        }; true
    }, onHitPlayer = { _, _ -> 1f })
    DefaultHit(online)

    val absorption = online.Absorption(0, 60.seconds)
    val apple = ItemFood(online, 322, onConsume = { adept ->
        adept.health(20f)
        if (adept.effects[absorption] == null) {
            adept.effects += absorption
            adept.shield(4f)
        }
    })

    fun resetPlayer(adept: Adept) {
        val onRed = adept.id in red
        val tag = if (onRed) redTag else blueTag
        val clay = if (onRed) redClay else blueClay
        val spawn = if (onRed) arena.red else arena.blue
        println("reset ${adept.name}")
        adept.effects -= absorption
        adept.health(20f)
        adept.velocity(adept.id, Velocity(0.0))
        adept.location(spawn)
        adept.inventory.clear()
        adept.inventory[6] = Stack(Item(299, 0, 1, tag), 1)
        adept.inventory[7] = Stack(Item(300, 0, 1, tag), 1)
        adept.inventory[8] = Stack(Item(301, 0, 1, tag), 1)
        adept.inventory[36] = Stack(sword, 1)
        adept.inventory[37] = Stack(bow, 1)
        adept.inventory[38] = Stack(pickaxe, 1)
        adept.inventory[39] = Stack(clay, 64)
        adept.inventory[40] = Stack(clay, 64)
        adept.inventory[41] = Stack(apple, 8)
        adept.inventory[44] = Stack(ITEM_ARROW, 1)
    }

    players.onEach { _, adept ->
        adept.inventory.onDrop { _, _ -> PASS }
        adept.inventory.onDrag { _, _ -> PASS }
        adept.inventory.onClick { it in 0..8 || it.toInt() == -2 }
        adept.inventory.onShiftClick { it in 0..8 }
        adept.inventory.onSwap { from, to -> to in 0..8 || from in 0..8 }
    }

    val redScore = Published(0)
    val blueScore = Published(0)
    val preGame = phased {
        Freeze(players)
        players.onEach { _, adept ->
            resetPlayer(adept)
        }
        val timer = scheduler.every(1.seconds)
        timer {
            if ((5 - it) == 0) disable() else {
                online.send("${SILVER}Cages are opening in ${RED}${(5 - it)}s${SILVER}.")
                online.send {
                    title(
                        "${RED}${BOLD}${(5 - it)}",
                        stay = 1.seconds, wax = .2.seconds
                    )
                }
            }
        }

        onDisabled {
            online.send("${GOLD}Cages have opened!")
        }
    }

    val game = phased {
        players.onEach { id, adept ->
            val onRed = red[adept.id] != null
            val color = if (onRed) RED else BLUE
            val spawn = if (onRed) arena.red else arena.blue!!
            val score = if (onRed) redScore else blueScore
            val subject = Published(false)
            var dead = false

            adept.location {
                if (it.y < 36.0) {
                    println("void ${adept.name}")
                    if (!dead) {
                        dead = true
                        println("setting dead to true")
                        subject(true)
                    }
                } else {
                    val position = it.toPosition()
                    val block = position.copy(y = (position.y - 0.5).toInt())
                    if (adept.blocks[block].type == 575) {
                        if (it.distance(spawn) > 15) {
                            online.send("$color${adept.name}$SILVER has scored.")
                            score(score.last + 1); disable()
                            if (score.last != 5) preGame.enable()
                        } else {
                            println("died to goal ${adept.name}")
                            if (!dead) {
                                dead = true
                                subject(true)
                            }
                        }
                    }
                }
            }

            adept.health.filter { it <= 0 }() {
                println("respawning ${adept.name}")
                online.send("$color${adept.name}$SILVER has died.")
                resetPlayer(adept)
            }

            subject {
                println("dead ${subject.last}:$it")
                if (dead) {
                    println("respawning ${adept.name}")
                    online.send("$color${adept.name}$SILVER has died.")
                    resetPlayer(adept)
                    dead = false
                }
            }
        }
    }

    val redScoreDisplay = Published("")
    val blueScoreDisplay = Published("")
    redScore {
        val builder = StringBuilder()
        builder.append("${RED}[R] $RED$BOLD")
        if (it > 0) {
            for (i in 1..it) builder.append("● ")
            if (it != 5) {
                builder.append("$SILVER$BOLD")
                for (i in 1..(5 - it))
                    builder.append("● ")
            }
        } else builder.append("$SILVER$BOLD● ● ● ● ●")

        redScoreDisplay(builder.toString())
        if (it >= 5) {
            online.send("Red team has won.")
            this.disable()
        }
    }

    blueScore {
        val builder = StringBuilder()
        builder.append("${BLUE}[B] $BLUE$BOLD")
        if (it > 0) {
            for (i in 1..it) builder.append("● ")
            if (it != 5) {
                builder.append("$SILVER$BOLD")
                for (i in 5..it)
                    builder.append("● ")
            }
        } else builder.append("$SILVER$BOLD● ● ● ● ●")

        blueScoreDisplay(builder.toString())
        if (it >= 5) {
            online.send("Blue team has won.")
            this.disable()
        }
    }

    val timer = scheduler.every(1.seconds)
    online.onEach { _, adept ->
        adept.board[0] = just("${RED}${BOLD}Multiplayer ")
        adept.board[1] = date()
        adept.board[2] = SPACER
        adept.board[3] = timer.map { "${WHITE}Time Left: $RED${String.format("%02d:%02d", it / 60, it % 60)}" }
        adept.board[4] = SPACER
        adept.board[5] = redScoreDisplay.map { it }
        adept.board[6] = blueScoreDisplay.map { it }
        adept.board[7] = SPACER
        // add kills and goals
        adept.board[9] = just("Game:$RED Bridge Duel")
        adept.board[10] = just("Winstreak:$RED 0")
        adept.board[11] = SPACER
        adept.board[12] = just(DOMAIN)
    }

    onDisabled(game::disable)
    onEnabled(preGame::enable)
    preGame.onDisabled(game::enable)
    game.onDisabled(preGame::enable)
}

