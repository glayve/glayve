package com.gitlab.glayve.anticheat.utils.math

class Buffer<T> constructor(private val capacity: Int) : Iterable<T> {
    private val queue = ArrayDeque<T>()
    val size
        get() = queue.size

    private fun put(element: T) {
        queue.add(element)
        if (queue.size > capacity) {
            queue.removeFirst()
        }
    }

    fun latest() = queue.last()

    fun tail(index: Int) = if(queue.size <= index) null else queue[queue.size - 1 - index]

    fun isFull() = queue.size == capacity

    fun isEmpty() = queue.isEmpty()
    fun isNotEmpty() = queue.isNotEmpty()

    fun clear() {
        queue.clear()
    }

    override fun iterator(): Iterator<T> {
        return queue.iterator()
    }

    operator fun plusAssign(element: T) = put(element)
}