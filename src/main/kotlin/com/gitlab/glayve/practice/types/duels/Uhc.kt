package com.gitlab.glayve.practice.types.duels


import DIAMOND_AXE
import DIAMOND_PICKAXE
import DIAMOND_SWORD
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Item
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.inventory.add
import com.gitlab.glayve.items.*
import com.gitlab.glayve.network.*
import com.gitlab.glayve.practice.Freeze
import com.gitlab.glayve.practice.Play
import com.gitlab.glayve.practice.Spectate
import com.gitlab.glayve.practice.games.Arena
import com.gitlab.glayve.practice.show
import com.gitlab.glayve.world.*
import java.nio.file.Paths
import kotlin.time.Duration.Companion.seconds

fun Togglable.Uhc(world: MutableWorld, online: Adepts, red: Adepts, blue: Adepts, arena: Arena) {
    val players = red + blue
    val spectators = online.filter { id, _ -> id !in players }
    Play(red, arena.red)
    Play(blue, arena.blue!!)
    Spectate(spectators, arena.spectators)
    online.show(players)

    players.onEach { id, adept ->
        adept.addEffect(id, Effect.DIG_DOWN.toByte(), -1, -1)
    }

    val wood = ItemBlock(online, BLOCK_WOODEN_PLANK, emptyMap()) { adept, position ->
        if (position.y > (arena.red.y + 8)) PASS else CONSUME
    }
    val stone = ItemBlock(online, BLOCK_COBBLESTONE, emptyMap()) { adept, position ->
        if (position.y > (arena.red.y + 8)) PASS else CONSUME
    }
    val table = importTable(Paths.get("diamond_pickaxe.times"))
    val pickaxe = ItemTool(online, DIAMOND_PICKAXE, tags = mapOf(
        "Unbreakable" to TRUE, "HideFlags" to (1 shl 2)
    )) { adept, position ->
        val material = world[position].material
        if (material != 4.toShort() && material != 5.toShort()) -1
        else table[adept.blocks[position].material.toInt()]
    }
    val axe = ItemTool(online, DIAMOND_AXE, tags = mapOf(
        "Unbreakable" to TRUE, "HideFlags" to (1 shl 2)
    )) { adept, position ->
        val material = world[position].material
        if (material != 5.toShort()) -1
        else table[adept.blocks[position].material.toInt()]
    }
    val rod = ItemRod(online, mapOf(
        "Unbreakable" to TRUE, "HideFlags" to (1 shl 2)
    ), onHitPlayer = { _, _ -> 1f })
    val bow = ItemBow(online, mapOf(
        "Unbreakable" to TRUE, "HideFlags" to (1 shl 2),
        "ench" to arrayOf(mapOf("id" to 48, "lvl" to 3))
    ), bonus = 2f, onHitPlayer = { _, _ -> 2f })
    val sword = ItemSword(online, DIAMOND_SWORD, mapOf(
        "Unbreakable" to TRUE, "HideFlags" to (1 shl 2),
        "ench" to arrayOf(mapOf("id" to SHARPNESS, "lvl" to 3))
    ), bonus = 3.75f, onHit = { _, _ -> 7f })
    DefaultHit(players as Adepts)
    val regen = online.Regeneration(2, 5.seconds)
    val speed = online.Speed(0, 10.seconds)
    val absorption = online.Absorption(0, 120.seconds)
    val heads = Item(online, 397, 0, 1, emptyMap(), { adept, interaction ->
        adept.effects += regen
        adept.effects += speed
        adept.effects += absorption
        adept.shield(4f)
        val current = adept.inventory[adept.selected.last]
        adept.inventory[adept.selected.last] = current.add(-1)
        CONSUME
    })

    val apple = ItemFood(online, 322, onConsume = {
        it.effects += regen
        it.effects += absorption
        it.shield(4f)
        println("onConsume ran")
    })

    lateinit var bucket: Item
    val lavaBucket = Item(online, 327, 0, 1, emptyMap(), { adept, interaction ->
        if (interaction.clicked != null) {
            adept.send("Actually placed instead of bugging out.")
            adept.blocks[interaction.clicked] = BLOCK_LAVA_FLOWING[0] //fix
            adept.inventory[adept.selected.last] = Stack(bucket, 1)
            CONSUME
        } else PASS
    })
    val waterBucket = Item(online, 326, 0, 1, emptyMap(), { adept, interaction ->
        if (interaction.clicked != null) {
            adept.blocks[interaction.clicked] = BLOCK_WATER_FLOWING[0] //fix
            adept.inventory[adept.selected.last] = Stack(bucket, 1)
            CONSUME
        } else PASS
    })
    bucket = Item(online, 325, 0, 1, emptyMap(), { adept, interaction ->
        if (interaction.clicked != null) {
            val type = adept.blocks[interaction.clicked]
            if (type == BLOCK_LAVA_FLOWING[0] || type == BLOCK_LAVA_STATIC[0]) {
                adept.inventory[adept.selected.last] = Stack(lavaBucket, 1)
                adept.blocks[interaction.clicked] = BLOCK_AIR
            } else if (type == BLOCK_WATER_FLOWING[0] || type == BLOCK_WATER_STATIC[0]) {
                adept.inventory[adept.selected.last] = Stack(waterBucket, 1)
                adept.blocks[interaction.clicked] = BLOCK_AIR
            }
            CONSUME
        } else PASS
    })

    fun mix(position: Position) {

    }
    world.onChanged { position, from, to ->
        //Effectively equal to notifyNeighborsOfStateChange
        if (to !in BLOCK_STATIC && (from !in BLOCK_STATIC && to !in BLOCK_FLOWING)) Face.values().forEach { direction ->
            val relative = position[direction]
            val other = world[relative]
            //Effectively equal to onNeighborBlockChange for dynamic
            if (other in BLOCK_LAVA)
                other.flowMix(world, relative, ::mix)

            //Effectively equal to onNeighborBlockChange for static
            if (other in BLOCK_WATER_STATIC)
                world[relative] = BLOCK_WATER_FLOWING[other.meta.toInt()]
            else if (other in BLOCK_LAVA_STATIC)
                world[relative] = BLOCK_LAVA_FLOWING[other.meta.toInt()]
        }

        if (to in BLOCK_LAVA && from?.material != to!!.material)
            if (to.flowMix(world, position, ::mix)) return@onChanged

        if (to in BLOCK_FLOWING) transient {
            scheduler.after((if (to in BLOCK_LAVA) 30 else 5).ticks)() {
                to!!.flow(world, position, ::mix)
            }
        }.enable()
    }

    val helm = Item(310, 0, 1, mapOf("ench" to arrayOf(mapOf("id" to 0.toShort(), "lvl" to 2.toShort()))))
    val chest = Item(311, 0, 1, mapOf("ench" to arrayOf(mapOf("id" to 4.toShort(), "lvl" to 2.toShort()))))
    val legs = Item(312, 0, 1, mapOf("ench" to arrayOf(mapOf("id" to 0.toShort(), "lvl" to 2.toShort()))))
    val boots = Item(313, 0, 1, mapOf("ench" to arrayOf(mapOf("id" to 0.toShort(), "lvl" to 2.toShort()))))

    fun uhcItems(adept: Adept) {
        adept.inventory.clear()
        adept.inventory[5] = Stack(helm, 1)
        adept.inventory[6] = Stack(chest, 1)
        adept.inventory[7] = Stack(legs, 1)
        adept.inventory[8] = Stack(boots, 1)
        adept.inventory[9] = Stack(ITEM_ARROW, 20)
        adept.inventory[10] = Stack(pickaxe, 1)
        adept.inventory[11] = Stack(axe, 1)
        adept.inventory[12] = Stack(waterBucket, 1)
        adept.inventory[13] = Stack(lavaBucket, 1)
        adept.inventory[36] = Stack(sword, 1)
        adept.inventory[37] = Stack(rod, 1)
        adept.inventory[38] = Stack(bow, 1)
        adept.inventory[39] = Stack(stone, 64)
        adept.inventory[40] = Stack(wood, 64)
        adept.inventory[41] = Stack(apple, 6)
        adept.inventory[42] = Stack(waterBucket, 1)
        adept.inventory[43] = Stack(lavaBucket, 1)
        adept.inventory[44] = Stack(heads, 3)
    }

    red.onEach { _, redAdept ->
        blue.values()?.board(6, just("${RED}${redAdept.name}"))
    }

    blue.onEach { _, blueAdept ->
        red.values()?.board(6, just("${RED}${blueAdept.name}"))
    }

    val timer = scheduler.every(1.seconds)
    players.onEach { _, adept ->
        adept.board[0] = just("${RED}${BOLD}Multiplayer")
        adept.board[1] = date()
        adept.board[2] = SPACER
        adept.board[3] = timer.map { "${RED}Time: $WHITE${String.format("%02d:%02d", it / 60, it % 60)}"}
        adept.board[4] = SPACER
        adept.board[5] = just("Opponent:")
        adept.board[7] = SPACER
        adept.board[8] = just("Game:$RED UHC Duel")
        adept.board[9] = just("Winstreak:$RED 0")
        adept.board[1] = SPACER
        adept.board[1] = just(DOMAIN)
    }


    val preGame = phased {
        val preTimer = scheduler.every(1.seconds)
        preTimer {
            val time = (5 - it)
            if (time == 0) disable()
        }; Freeze(players)
        players.onEach { _, adept -> uhcItems(adept) }
    }

    val inGame = phased {
        players.onEach { id, adept ->
            adept.health {
                if (it <= 0) {
                    println("Disabling")
                    online.send("${adept.name}$SILVER has died.")
                    adept.health(20f)
                    this@Uhc.disable()
                }
            }

            adept.inventory.onClick {
                println(it)
                it in 0..8 || it.toInt() == -2
            }
            adept.inventory.onDrop { slot, stack -> PASS }
            adept.inventory.onShiftClick { it in 0..8 }
            adept.inventory.onCollect { slots, amount -> PASS }
        }
    }


    onEnabled(preGame::enable)
    preGame.onDisabled(inGame::enable)
    inGame.onDisabled(this::disable)
}