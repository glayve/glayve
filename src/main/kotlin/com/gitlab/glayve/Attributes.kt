package com.gitlab.glayve

import com.gitlab.ballysta.architecture.Mutable

typealias Attributes = Mutable<String, Double>

const val GENERIC_SPEED = "generic.movementSpeed"
const val GENERIC_ATTACK = "generic.attack_damage"