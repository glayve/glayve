package com.gitlab.glayve.items

import com.gitlab.glayve.network.TagCompound

val SHARPNESS: Short = 16

val unbreakable = mapOf("Unbreakable" to 1.toByte())

fun enchants(array: Array<TagCompound>): TagCompound {
    return mapOf("ench" to array)
}
fun enchant(id: Short, level: Short): TagCompound {
    return mapOf("id" to id, "level" to level)
}