package com.gitlab.glayve.practice.games

import com.gitlab.ballysta.architecture.Table
import com.gitlab.ballysta.architecture.filter
import com.gitlab.glayve.GOLD
import com.gitlab.glayve.GREEN
import com.gitlab.glayve.RED

operator fun <Key, Value> Table<Key, Value>.minus(key: Key) = filter { it, _ -> it != key }

const val REACTIONS = 0
const val HOTBAR_SWAP = 1
const val CHEST_LOOTING_1 = 2
const val CHEST_LOOTING_2 = 3
const val BLOCK_FRENZY = 4
const val TRACKING_TRAINER = 5
const val CLICK_TRAINER = 6
const val COMBO_TRAINER = 7
const val BRIDGES_DUEL = 8
const val UHC_DUEL = 9
const val SUMO_DUEL = 10
const val BOXING_DUEL = 11
const val OP_DUEL = 12

fun getArena(id: Int): Arena {
    return when(id) {
        REACTIONS -> reactionsArenas[0]
        HOTBAR_SWAP -> cpsArenas[0]
        CHEST_LOOTING_1 -> chestGrabArenas[0]
        CHEST_LOOTING_2 -> chestGrabArenas[0]
        BLOCK_FRENZY -> frenzyArenas[0]
        TRACKING_TRAINER -> botHopArenas[0]
        CLICK_TRAINER -> cpsArenas[0]
        COMBO_TRAINER -> lineBotArenas[0]
        BRIDGES_DUEL -> bridgesArenas[0]
        UHC_DUEL -> duelsArena[0]
        SUMO_DUEL -> sumoArenas[0]
        BOXING_DUEL -> duelsArena[0]
        OP_DUEL -> duelsArena[0]
        else -> duelsArena[0]
    }
}