package com.gitlab.glayve

import DIAMOND_SWORD
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.inventory.*
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.network.*
import com.gitlab.glayve.network.Face.BOTTOM
import com.gitlab.glayve.network.Face.TOP
import com.gitlab.glayve.util.*
import com.gitlab.glayve.world.*
import java.io.DataInputStream
import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDate
import java.time.format.DateTimeFormatter.ofPattern
import java.util.*
import kotlin.math.*
import kotlin.time.Duration
import kotlin.time.TimeMark
import kotlin.time.TimeSource


fun getCollisions(world: World, bounds: Bounds): List<Bounds> {
    return emptyList()
}

fun moveEntity(world: World, location: Location, velocity: Velocity) {
    var (x, y, z) = location
    var motion = velocity

    //todo in web
    val bounds = Bounds(location.toVelocity(), location.toVelocity())
    val collisions = getCollisions(world, bounds)
    fun Bounds.offset(other: Bounds, offset: Double, a: Int, b: Int, target: Int): Double {
        if (
            other.max[a] > this.min[a] && other.min[a] < this.max[a] &&
            other.max[b] > this.min[b] && other.min[b] < this.max[b]
        ) if (offset > 0.0 && other.max[target] <= this.min[target]) {
            val delta = this.min[target] - other.max[target]
            if (delta < offset) return delta
        } else if (offset < 0.0 && other.min[target] >= this.max[target]) {
            val delta = this.max[target] - other.min[target]
            if (delta > offset) return delta
        }; return offset
    }
    motion = motion.copy(x = motion.x + collisions.fold(motion.x) {
            current, collision -> collision.offset(bounds, current, 1, 2, 0)
    })
    motion = motion.copy(y = motion.y + collisions.fold(motion.y) {
            current, collision -> collision.offset(bounds, current, 0, 2, 1)
    })
    motion = motion.copy(z = motion.z + collisions.fold(motion.z) {
            current, collision -> collision.offset(bounds, current,  0, 1, 2)
    })
}

fun Toggled.measure(result: (Duration) -> (Unit)) {
    var start: TimeMark? = null
    onEnabled { start = TimeSource.Monotonic.markNow() }
    onDisabled { result(start!!.elapsedNow()); start = null }
}

fun Block.isBlocked() = this != BLOCK_AIR && this !in (BLOCK_WATER + BLOCK_LAVA)
fun Block.canBeFlowedInto(other: Block): Boolean {
    if (other.isBlocked()) return false
    if (this in BLOCK_LAVA && other in BLOCK_WATER)
        return true
    if (other !in (BLOCK_WATER + BLOCK_LAVA))
        return true
    return false
}

fun Block.depletion(other: Block) =
    if (
        (this in BLOCK_WATER && other in BLOCK_WATER) ||
        (this in BLOCK_LAVA && other in BLOCK_LAVA)
    ) other.meta.toInt() else -1

fun Block.flowDirections(world: World, position: Position) =
    EnumSet.noneOf(Face::class.java).apply {
        var current = 1000
        Plane.HORIZONTAL.forEach {
            val block = position[it]
            val type = world[block]
            if (!type.isBlocked() && depletion(type) != 0) {
                val below = world[block[BOTTOM]]
                val cost = if (!below.isBlocked()) 0 else
                    flowCost(world, block, 1, it.opposite)
                if (cost < current) clear()
                if (cost <= current) { add(it); current = cost }
            }
        }
    }!!
fun Block.flowCost(world: World, position: Position, distance: Int, from: Face): Int {
    var current = 1000
    Plane.HORIZONTAL.forEach {
        if (it != from) {
            val block = position[it]
            val type = world[block]
            if (!type.isBlocked() && depletion(type) != 0) {
                if (!world[block[BOTTOM]].isBlocked())
                    return distance
                if (distance < 4) {
                    val cost = flowCost(world, block, distance + 1, it.opposite)
                    if (cost < current) current = cost
                }
            }
        }
    }
    return current
}

fun Block.flowInto(world: MutableWorld, position: Position, level: Int) {
    if (position.y !in 0..255) return
    val type = world[position]
    if (canBeFlowedInto(type))
        world[position] = if (this in BLOCK_WATER) BLOCK_WATER_FLOWING[level] else BLOCK_LAVA_FLOWING[level]
}
fun Block.flowMix(world: MutableWorld, position: Position, mix: (Position) -> (Unit)): Bool {
    if (this in BLOCK_LAVA) {
        if (!Face.values().filter { it != BOTTOM }.any {
            world[position[it]] in BLOCK_WATER
        }) return false
        if (meta == 0.toShort()) {
            world[position] = BLOCK_OBSIDIAN
            mix(position)
            return true
        }
        if (meta <= 4) {
            world[position] = BLOCK_COBBLESTONE
            mix(position)
            return true
        }
    }
    return false
}
fun Block.flow(
    world: MutableWorld, position: Position,
    mix: (Position) -> (Unit)
) {
    if (world[position] != this) return

    fun makeStatic(level: Int) {
        world[position] =
            if (this in BLOCK_WATER) BLOCK_WATER_STATIC[level]
            else BLOCK_LAVA_STATIC[level]
    }
    var depletion = depletion(this)
    val depletionPerBlock = if (this in BLOCK_WATER) 1 else 2
    if (depletion > 0) {
        var least = 8; var sources = 0
        for (direction in Plane.HORIZONTAL) {
            var depleted = depletion(world[position[direction]])
            if (depleted < 0) continue
            if (depleted == 0) ++sources
            if (depleted >= 8) depleted = 0
            least = min(depleted, least)
        }

        var next = least + depletionPerBlock
        if (next > 7) next = -1

        val above = depletion(world[position[TOP]])
        if (above >= 0) next = if (above >= 8) above else above + 8

        if (sources >= 2 && this in BLOCK_WATER) {
            val below = world[position[BOTTOM]]
            if (canBeFlowedInto(below) || depletion(below) == 0) next = 0
        }

        //some fast drain speed modifier thing here.
        // if (material == Material.lava && next < 8 && next > level && rand.nextInt(4) != 0) speed *= 4;
        if (next == depletion) makeStatic(next)
        else {
            depletion = next
            if (next < 0) { //or fast drain something here.
                world[position] = BLOCK_AIR
                return
            } else {
                world[position] =
                    if (this in BLOCK_WATER) BLOCK_WATER_FLOWING[next]
                    else BLOCK_LAVA_FLOWING[next]
                //used to have notify all but bottom here instead.
            }
        }
    } else makeStatic(depletion)

    val bottom = position[BOTTOM]
    val below = world[bottom]
    if (canBeFlowedInto(below)) {
        if (this in BLOCK_LAVA && below in BLOCK_WATER) {
            world[bottom] = BLOCK_STONE; mix(bottom)
        } else flowInto(world, bottom, if (depletion >= 8) depletion else depletion + 8)
    } else if (depletion == 0 || below.isBlocked()) {
        val directions = flowDirections(world, position)
        var next = depletion + depletionPerBlock
        if (depletion >= 8) next = 1
        if (next >= 8) return
        directions.forEach { flowInto(world, position[it], next) }
    }
}

operator fun Location.rangeTo(to: Location): Iterable<Position> {
    val distance = (distance(to) * 10).toInt()
    val dx = (to.x - x) / distance
    val dy = (to.y - y) / distance
    val dz = (to.z - z) / distance
    val blocks = HashSet<Position>()
    for (i in 0..distance) {
        blocks += copy(
            x = x + (dx * i),
            y = y + (dy * i),
            z = z + (dz * i)
        ).toPosition()
    }
    return blocks
}

data class Ray(val from: Velocity, val to: Velocity) {
    val direction = (to - from).normalize()
    val inverse = Velocity(1.0) / direction
}

fun intersection(bounds: Bounds, from: Velocity, direction: Velocity): Double {
    val min = (bounds.min - from) / direction
    val max = (bounds.max - from) / direction
    val enter = max(max(min(min.x, max.x), min(min.y, max.y)), min(min.z, max.z))
    val exit = min(min(max(min.x, max.x), max(min.y, max.y)), max(min.z, max.z))
    return if (exit < 0 || enter > exit) Double.POSITIVE_INFINITY else enter
}

fun Ray.intersects(min: Velocity, max: Velocity): Velocity? {
    var enter = Double.NEGATIVE_INFINITY
    var exit = Double.POSITIVE_INFINITY
    for (i in 0..2) {
        if (inverse[i] == 0.0) println("parallel rip")
        val t0 = (min[i] - from[i]) * inverse[i]
        val t1 = (max[i] - from[i]) * inverse[i]
        val tMin = min(t0, t1)
        val tMax = max(t0, t1)
        if (tMin > enter) enter = tMin
        if (tMax < exit) exit = tMax
        if (enter > exit || exit < 0)
            return null
        if (exit < max(0.0, enter) || enter >= Double.POSITIVE_INFINITY)
            return null
    }
    if (enter > (to - from).length) return null
    return from + (direction * enter)
}


fun Double.toRadians() = this / 180 * PI
fun Float.toRadians() = this / 180 * PI
fun Double.toDegrees() = this * 180 / PI
fun Float.toDegrees() = this * 180 / PI

fun Mutated<*, out Channel>.send(block: Channel.() -> (Unit)) = values.forEach(block)
fun Mutated<*, out Channel>.send(channel: Channel, block: Channel.() -> (Unit)) =
    values.filter { it !== channel }.forEach(block)
fun Mutated<*, out Channel>.send(text: Any, color: String = "white", action: Boolean = false) =
    send { chat(text, color, action) }

fun Mutated<*, out Channel>.send(name: String, location: Location, volume: Float, pitch: Float) {
    values.forEach {
        val range = if (volume > 1.0f) (16.0f * volume).toDouble() else 16.0
        if (it.location.last.distance(location) < range)
            it.sound(name, location, volume, pitch)
    }
}


infix fun ClosedRange<Double>.step(step: Double): Iterable<Double> {
    require(start.isFinite())
    require(endInclusive.isFinite())
    require(step > 0.0) { "Step must be positive, was: $step." }
    val sequence = generateSequence(start) { previous ->
        if (previous == Double.POSITIVE_INFINITY) return@generateSequence null
        val next = previous + step
        if (next > endInclusive) null else next
    }
    return sequence.asIterable()
}

fun Toggled.Trace(channel: Channel, ray: PublishedObservable<Ray>) {
    val count = 100
    ray { (from, to) ->
        val dx = (to.x - from.x) / count
        val dy = (to.y - from.y) / count
        val dz = (to.z - from.z) / count
        for (i in 0..count) {
            val x = (from.x + (dx * i)).toFloat()
            val y = (from.y + (dy * i)).toFloat()
            val z = (from.z + (dz * i)).toFloat()
            channel.particle(30, true, x, y, z, 0f, 0f, 0f, 0f, 1)
        }
    }
}

fun Toggled.Outline(channel: Channel, bounds: PublishedObservable<Bounds>) {
    bounds {
        fun particle(x: Double, y: Double, z: Double) {
            channel.particle(
                30, true,
                x.toFloat(), y.toFloat(), z.toFloat(),
                0f, 0f, 0f, 0f, 1
            )
        }

        val count = 0.3
        for (x in (it.min.x..it.max.x) step count) {
            particle(x, it.max.y, it.max.z)
            particle(x, it.min.y, it.min.z)
            particle(x, it.min.y, it.max.z)
            particle(x, it.max.y, it.min.z)
        }
        for (z in (it.min.z..it.max.z) step count) {
            particle(it.max.x, it.max.y, z)
            particle(it.min.x, it.min.y, z)
            particle(it.min.x, it.max.y, z)
            particle(it.max.x, it.min.y, z)
        }
        for (y in (it.min.y..it.max.y) step count) {
            particle(it.max.x, y, it.max.z)
            particle(it.min.x, y, it.min.z)
            particle(it.min.x, y, it.max.z)
            particle(it.max.x, y, it.min.z)
        }
    }
}

enum class Gamemode { SURVIVAL, CREATIVE, ADVENTURE, SPECTATOR }

fun Int.isSolid() = this != 0 && this != 4176

fun date(): PublishedObservable<String> {
    return just("${SILVER}${ofPattern("MM/dd/uuuu").format(LocalDate.now()) }              ")
}


var FRICTION = 2.0
var HORIZONTAL = 0.475
var VERTICAL = 0.3625
var EXTRA_VERTICAL = 0.0
var EXTRA_HORIZONTAL = 0.475

fun knockbackVelocity() = Velocity(0.0, -0.078375, 0.0)
fun knockbackVelocity(location: Location, attacker: Channel?) = if (attacker != null) {
    val from = attacker.location.last
    var x = location.x - from.x
    var z = location.z - from.z
    var y = VERTICAL
    val distance = sqrt(x * x + z * z)

    x /= distance; x *= HORIZONTAL
    z /= distance; z *= HORIZONTAL

    if (attacker.sprinting.last) {
        val yaw = from.yaw * PI
        x += -sin(yaw / 180) * EXTRA_HORIZONTAL
        z += cos(yaw / 180) * EXTRA_HORIZONTAL
        y += EXTRA_VERTICAL;
        //*= 0.6; z *= 0.6;
    }; Velocity(x, y, z)
} else Velocity(0.0, -0.078375, 0.0)
fun knockbackVelocity(location: Location, attacker: Location, velocity: Velocity, strength: Int): Velocity {
    var x = location.x - attacker.x
    var z = location.z - attacker.z
    var y = .3


    x *= .2; z *= .2
    val motion = sqrt(velocity.x * velocity.x + velocity.z * velocity.z)
    if (strength > 0) {
        x += (velocity.x * strength * 0.6000000238418579 / motion)
        y += 0.1
        z += (velocity.z * strength * 0.6000000238418579 / motion)
    };
    return Velocity(x, y, z)
}

fun Item.protection(id: Short, effectiveness: Float): Int {
    val enchantments = tags["ench"] as TagList? ?: emptyArray()
    enchantments.forEach {
        if ((it as TagCompound)["id"] as Short == id) {
            val lvl = it["lvl"] as Short
            return (((6 + lvl * lvl) / 3f) * effectiveness).toInt()
        }
    }; return 0
}

fun Adept.damageCombat(
    base: Float, magic: Float, critical: Float,
    protection: Int, knockback: Velocity
): Bool {
    val combined = base + magic + critical
    val holding = inventory[selected.last]!!.item
    val blocking = using.last && holding.material == DIAMOND_SWORD
    val blocked = if (blocking) (combined + 1f) * 0.5f else combined
    val armour = (5..8).sumOf { ARMOUR[inventory[it.toSlot()]!!.item.material.toInt()].toInt() }
    val absorbed = (blocked * (25 - armour)) / 25f
    val enchantment = protection.coerceIn(0..25)
    val modifier = ((enchantment + 1 shr 1) + (0..(enchantment shr 1) + 1).random()).coerceAtMost(20)
    val total = if (modifier > 0) (absorbed * (25 - modifier)) / 25f else absorbed
    return damage(total, knockback, critical > 0, magic > 0)
}
fun Adept.damageMelee(source: Channel, base: Float, magic: Float): Bool {
    val grounded = source.location.last.ground
    val protection = (5..8).sumOf { inventory[it].item.protection(0, .75f) }
    return damageCombat(
        base, magic, if (!grounded) base * .5f else 0f,
        protection, knockbackVelocity(location.last, source)
    )
}
fun Adept.damageRanged(
    source: Location, velocity: Velocity,
    base: Float, magic: Float, critical: Bool, strength: Int
): Bool {
    val amount = ceil(velocity.length * base).toInt()
    val protection = (5..8).sumOf {
        inventory[it.toSlot()]!!.item.protection(0, 0.75f)
    } + (5..8).sumOf {
        inventory[it.toSlot()]!!.item.protection(4, 1.5f)
    }
    return damageCombat(
        amount.toFloat(), magic, if (critical) (0..(amount / 2 + 2)).random().toFloat() else 0f,
        protection, knockbackVelocity(location.last, source, velocity, strength)
    )
}
fun calculateDamage(adept: Adept, source: Adept, initial: Float): Float {
    //clean up float double conversion shit here.
    val grounded = source.location.last.ground
    var amount = if (!grounded) initial * 1.5 else initial.toDouble()
    //subtract armour values.
    //check for blocking
    //etc.
    amount += source.attributes[GENERIC_ATTACK] ?: 0.0
    return amount.toFloat()
}
fun calculateFireDamage(adept: Adept, initial: Float): Float {

    return initial;
}
fun calculateBowDamage(adept: Adept, source: Adept, initial: Float): Float {

    return 1f
}



val ARMOUR = ByteArray(4096).apply {
    // leather
    this[298] = 1 // helm
    this[299] = 3
    this[300] = 2
    this[301] = 1

    // chain
    this[302] = 2
    this[303] = 5
    this[304] = 4
    this[305] = 1

    // iron
    this[306] = 2
    this[307] = 6
    this[308] = 5
    this[309] = 2

    // diamond
    this[310] = 3
    this[311] = 8
    this[312] = 6
    this[313] = 3

    // gold
    this[314] = 2
    this[315] = 5
    this[316] = 3
    this[317] = 1
}

val DURABILITY = ByteArray(4096).apply {
    for (i in 298..301) this[i] = (ARMOUR[i] * 5).toByte()
    for (i in 302..305) this[i] = (ARMOUR[i] * 15).toByte()
    for (i in 306..309) this[i] = (ARMOUR[i] * 15).toByte()
    for (i in 310..313) this[i] = (ARMOUR[i] * 7).toByte()
    for (i in 314..317) this[i] = (ARMOUR[i] * 33).toByte()

}

fun damageArmor(inventory: Inventory, passed: Float) {
    var damage = passed / 4f
    if (damage < 1f) damage = 1f
    for (i in 5..8) {
        val stack = inventory[i.toByte()] ?: Stack.EMPTY
//        inventory[i.toByte()] = stack.copy(meta =
//            (stack.item.meta + damage).toInt().toShort()
//        )
    }
}

/*
fun String.translate(): String {
    val b = toCharArray()
    for (i in 0 until b.size - 1) {
        if (b[i] == '§' && "0123456789AaBbCcDdEeFfKkLlMmNnOoRr".indexOf(b[i + 1]) > -1) {
            b[i] = '\u00A7'
            b[i + 1] = Character.toLowerCase(b[i + 1])
        }
    }
    return String(b)
}*/

/*
fun Toggled.Armor(
    player: Player,
    slot: Slot, filter: (Stack) -> (Bool)
) {
    player.onUse { _, _ ->
        player.inventory.getAndUpdate(player.selected.last) {
            if (filter(it)) {
                val previous = player.inventory.getAndUpdate(slot) { current ->
                    if (current.type < 1) it else current
                }
                if (previous?.type ?: 0 < 1) EMPTY else it
            } else it
        }
    }
}
*/



fun importTable(path: Path): ByteArray {
    val imported = ByteArray(65536)
    DataInputStream(Files.newInputStream(path)).use { out ->
        val count = out.readInt()
        for (i in 0 until count)
            imported[out.readShort().toInt()] = out.readByte()
    }
    return imported
}



fun <Key, Value, To>  Mutated<Key, Value>.mapToMutable(mapper: (Value) -> (To?)): Toggled.() -> (Mutable<Key, To>) = {
    val logic = MutableTable<Key, To>()
    onRemoved { key, _ -> logic[key] = null }
    onAdded { key, value -> logic[key] = mapper(value) }
    object : Mutable<Key, To> by logic {
        override fun set(key: Key, value: To?): To? =
            if (key in this@mapToMutable) logic.set(key, value) else null
    }
}

operator fun Adepts.contains(adept: Adept) = adept.id in this