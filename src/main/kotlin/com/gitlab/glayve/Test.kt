package com.gitlab.glayve

import com.github.exerosis.mynt.continued
import java.util.concurrent.ConcurrentLinkedDeque
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.coroutines.resume
import kotlin.coroutines.intrinsics.COROUTINE_SUSPENDED

class Sequencer {
    val tasks = ConcurrentLinkedDeque<() -> (Unit)>()
    val processing = AtomicBoolean(false)

    fun sequence(block: () -> (Unit)) {
        if (processing.compareAndSet(false, true)) {
            block()
            val next = tasks.poll()
            if (next != null) next()
            else processing.set(false)
        } else tasks.addLast {
            block()
            val next = tasks.poll()
            if (next != null) next()
            else processing.set(false)
        }
    }
    suspend fun sequence() = continued<Unit> {
        sequence { it.resume(Unit) }
        COROUTINE_SUSPENDED
    }
}