package com.gitlab.glayve.anticheat

enum class CheckType {
    KILLAURA,
    HITBOX,
    CLICK,
    MOVE,
    PACKETS
}