@file:Suppress("NOTHING_TO_INLINE")
package com.gitlab.glayve

import com.gitlab.ballysta.architecture.Mutated
import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.filter
import com.gitlab.glayve.network.complete

val Channel.handle
    get(): Toggled.(Any, Arguments.() -> (Unit)) -> (Unit) = { pattern, handler ->
        val regex = Regex(pattern.toString())
        fun execute(message: String): Boolean {
            val split = message.split(regex, 2)
            if (split.size >= 2 && split[0].isEmpty()) try {
                handler(object : Arguments {
                    override val executing = true
                    val args = split[1].split(" ")
                    var index = 0
                    val instance = this
                    override val string = Adapter0 {
                        if (++index >= args.size) emptySequence()
                        else sequenceOf(args[index])
                    }

                    override fun <Type> (Adapter0<Type>).invoke() = this(instance).firstOrNull()
                    override fun <Type> (Adapter0<Type>).invoke(message: String) = invoke() ?: error(message)
                    override fun <Type> (Adapter0<Type>).invoke(block: (Type) -> (Unit)) {
                        val current = index
                        val result = try {
                            this(instance).firstOrNull()
                        } catch (reason: Throwable) {
                            null
                        }
                        if (result != null) {
                            block(result); error("")
                        }
                        index = current
                    }
                })
            } catch (reason: Throwable) {
                if (!reason.message.isNullOrBlank())
                    this@handle.send("§c${reason.message!!}")
            } else return true
            return false
        }
        fun tab(message: String): List<String> {
            val matches = ArrayList<String>()
            val split = message.split(regex, 2)
            if (split.size >= 2 && split[0].isEmpty()) try {
                handler(object : Arguments {
                    override val executing = false
                    val args = split[1].split(" ")
                    var index = 0
                    val instance = this
                    override val string = Adapter0 {
                        if (++index >= args.size) emptySequence()
                        else sequenceOf(args[index])
                    }
                    override fun <Type> (Adapter0<Type>).invoke(): Type? {
                        val adapted = this(instance).toList()
                        if (index == args.lastIndex)
                            matches += adapted.map { it.toString() }
                        return adapted.firstOrNull()
                    }
                    override fun <Type> (Adapter0<Type>).invoke(message: String) = invoke() ?: error(message)
                    override fun <Type> (Adapter0<Type>).invoke(block: (Type) -> (Unit)) {
                        val current = index
                        val result = try { invoke() }
                        catch (_: Throwable) { null }
                        if (result != null) {
                            block(result); error("end of branch")
                        }
                        index = current
                    }
                })
            } catch (_: Throwable) {}
            return matches
        }

        onChat { execute(it) }
        onTab { message ->
            val results = tab(message)
            if (results.isNotEmpty())
                complete(results)
        }
    }

interface Arguments {
    val executing: Boolean
    val string: Adapter0<String>

    operator fun <Type> Adapter0<Type>.invoke(): Type?
    operator fun <Type> Adapter0<Type>.invoke(message: String): Type
    operator fun <Type> Adapter0<Type>.invoke(block: (Type) -> (Unit))

    operator fun <Type, First> Adapter1<Type, First>.
        invoke(first: First, message: String): Type =
    Adapter0 { invoke(this@Arguments, first) }(message)
    operator fun <Type, First> Adapter1<Type, First>.
        invoke(first: First, block: (Type) -> (Unit)) =
    Adapter0 { invoke(this@Arguments, first) }(block)
    operator fun <Type, First> Adapter1<Type, First>.
        invoke(first: First) =
    Adapter0 { invoke(this@Arguments, first) }()

    operator fun <Type, First, Second> Adapter2<Type, First, Second>.
        invoke(first: First, second: Second, message: String) =
    Adapter0 { invoke(this@Arguments, first, second) }(message)
    operator fun <Type, First, Second> Adapter2<Type, First, Second>.
        invoke(first: First, second: Second, block: (Type) -> (Unit)) =
    Adapter0 { invoke(this@Arguments, first, second) }(block)
    operator fun <Type, First, Second> Adapter2<Type, First, Second>.
        invoke(first: First, second: Second) =
    Adapter0 { invoke(this@Arguments, first, second) }()
}

typealias Adapter0<Type> = Arguments.() -> (Sequence<Type>)
typealias Adapter1<Type, First> = Arguments.(First) -> (Sequence<Type>)
typealias Adapter2<Type, First, Second> = Arguments.(First, Second) -> (Sequence<Type>)

inline fun <Type> Adapter0(noinline adapter: Adapter0<Type>) = adapter
inline fun <Type, First> Adapter1(noinline adapter: Adapter1<Type, First>) = adapter
inline fun <Type, First, Second> Adapter2(noinline adapter: Adapter2<Type, First, Second>) = adapter

inline fun Arguments.action(block: () -> (Unit)) { if (executing) block() }

val integer = Adapter0 { string(this).mapNotNull { it.toLongOrNull() } }
val real = Adapter0 { string(this).mapNotNull { it.toDoubleOrNull() } }
val match = Adapter1<String, String> { pattern ->
    val regex = Regex(pattern)
    string(this).filter { it.matches(regex) }
}
val player = Adapter1<Channel, Mutated<*, out Channel>> { channels ->
    string(this).mapNotNull { start -> channels.values.filter { it.name.startsWith(start, ignoreCase = true) }() }
}
