package com.gitlab.glayve.anticheat.checks.combat

import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.onChanged
import com.gitlab.ballysta.architecture.onEach
import com.gitlab.ballysta.architecture.onNext
import com.gitlab.glayve.Adepts
import com.gitlab.glayve.PASS
import com.gitlab.glayve.anticheat.CheckType
import com.gitlab.glayve.anticheat.flag
import com.gitlab.glayve.anticheat.utils.eyeHeight
import com.gitlab.glayve.anticheat.utils.markNow
import com.gitlab.glayve.anticheat.utils.math.Buffer
import com.gitlab.glayve.anticheat.utils.math.getDirection
import com.gitlab.glayve.util.*
import kotlin.time.TimeMark

internal fun Toggled.HitboxCheck(players: Adepts) {
    players.onEach { _, player ->
        val moveHistory = Buffer<MovementHistory>(15)

        player.location.onChanged { _, to ->
            moveHistory += MovementHistory(
                location = to,
                moveTime = markNow()
            )
        }

        players.onEach { _, other ->
            var attack: Attack? = null
            other.onLeftClick { interaction ->
                if (interaction.entity == player.id) {
                    val history = moveHistory
                        .filter { it.moveTime.elapsedNow().inWholeMilliseconds <= 1200L }
                        .map {
                            Bounds.ofEntity(it.location, 0.8, 2.0)
                        }.toMutableList().apply {
                            this += Bounds.ofEntity(
                                player.location.last, 0.8, 2.0
                            )
                        }

                    // wait for next tick to raytrace
                    attack = Attack(
                        location = other.location.last,
                        targetPositions = history
                    )
                }
                PASS
            }

            other.location.onNext { _, to ->
                attack?.let { attackData ->
                    attack = null
                    val start = attackData.location.toVelocity().plus(Vector(0.0, other.eyeHeight, 0.0))
                    val direction = (to.yaw to to.pitch).getDirection()

                    val rayTraceResult = attackData.targetPositions.map {
                        it.rayTrace(
                            start = start,
                            direction = direction,
                            maxDistance = 10.0
                        )
                    }

                    val miss = rayTraceResult.all { it == null }
                    if (miss) {
                        other.flag(
                            type = CheckType.HITBOX,
                            message = "attack players out of sight"
                        )
                        return@let
                    }

                    val distance = rayTraceResult
                        .filterNotNull()
                        .minOf {
                            it.hitPosition distance start
                        }

                    if (distance > 3.05) {
                        other.flag(
                            CheckType.HITBOX,
                            "reach: $distance"
                        )
                    }
                }
            }
        }
    }
}

private data class MovementHistory(
    val location: Location,
    val moveTime: TimeMark
)

private data class Attack(
    val location: Location,
    val targetPositions: List<Bounds>
)