package com.gitlab.glayve.anticheat.checks.net

import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.onEach
import com.gitlab.ballysta.architecture.onNext
import com.gitlab.glayve.Adepts
import com.gitlab.glayve.anticheat.CheckType
import com.gitlab.glayve.anticheat.flag
import com.gitlab.glayve.anticheat.utils.markNow
import com.gitlab.glayve.send

private const val MaxBalance = 10.0
private const val MinBalance = -15.0
private const val FlagReduce = 3.0
private const val ErrorCompensation = -0.001
private const val TeleportReward = 2.0

// Prevent player adjust the client timer value
fun Toggled.TimerCheck(players: Adepts) {
    players.onEach { _, player ->
        var balance = 0.0
        var lastMoving = markNow()

        player.onTeleport {
            balance -= TeleportReward
        }

        player.location.onNext { _, _ ->
            val delay = lastMoving.elapsedNow().inWholeMilliseconds + 1L
            lastMoving = markNow()

            balance += ((50.0 - delay) / 50.0) + ErrorCompensation

            player.send("b: $balance / $delay")

            // make sure balance is larger than the min balance
            balance = balance.coerceAtLeast(MinBalance)

            if (balance > MaxBalance) {
                player.flag(
                    CheckType.PACKETS,
                    "timer (balance: $balance)"
                )
                balance -= FlagReduce

                // player.location(from)
            }
        }
    }
}