package com.gitlab.glayve.anticheat.utils

import kotlin.math.abs
import kotlin.math.roundToInt
import kotlin.time.TimeSource

fun markNow() = TimeSource.Monotonic.markNow()

fun Float.isNearlyInt(accuracy: Float = 0.00012f) = abs(this - this.roundToInt()) <= accuracy

// format boolean
fun Boolean.format() = if(this) "+" else "-"
fun Iterable<Boolean>.format() : String = joinToString(",")
fun Array<Boolean>.format() : String = joinToString(",")

// format double
fun Double.format() = (this * 1000).roundToInt() / 1000.0
fun Double.formatToString() = format().toString()

// format float
fun Float.format() = (this * 1000).roundToInt() / 1000.0
fun Float.formatToString() = format().toString()