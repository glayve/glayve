package com.gitlab.glayve.anticheat.checks.interact

import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.onEach
import com.gitlab.glayve.Adepts
import com.gitlab.glayve.PASS
import com.gitlab.glayve.util.Bounds
import com.gitlab.glayve.world.MutableWorld

fun Toggled.EntityInteractCheck(adepts: Adepts, world: MutableWorld) {
    adepts.onEach { _, adept ->
        adept.onLeftClick { interaction ->
            interaction.entity?.let { id ->
                adepts[id]?.location?.last?.let { targetLocation ->
                    val targetBounds = Bounds.ofEntity(targetLocation, 0.4, 1.8)
                    // TODO: Get the accurate interaction bounds of the block?
                    // TODO: Add a raytrace function for world?
                }
            }
            PASS
        }
    }
}