package com.gitlab.glayve.items

import ARROW
import BOW
import ROD
import com.gitlab.ballysta.architecture.*
import com.gitlab.glayve.*
import com.gitlab.glayve.inventory.Item
import com.gitlab.glayve.inventory.Stack
import com.gitlab.glayve.inventory.add
import com.gitlab.glayve.inventory.remove
import com.gitlab.glayve.network.*
import com.gitlab.glayve.util.*
import com.gitlab.glayve.world.Block
import com.gitlab.glayve.world.material
import com.gitlab.glayve.world.meta
import java.util.concurrent.ThreadLocalRandom.current
import kotlin.math.min
import kotlin.time.TimeSource




fun Toggled.Item(
    adepts: Adepts, material: Short,
    meta: Short = 0, max: Byte = 64,
    tags: TagCompound = emptyMap(),
    onUse: Togglable.(Adept, Interaction) -> (Bool) = { _, _ -> PASS },
    onDig: Togglable.(Adept, Interaction) -> (Bool) = { _, _ -> PASS },
) = Item(material, meta, max, tags).also { item ->
    adepts.onEach { _, adept ->
        adept.onLeftClick {
            val stack = adept.inventory[adept.selected.last]
            if (stack?.item === item) onDig(adept, it) else PASS
        }
        adept.onRightClick {
            val stack = adept.inventory[adept.selected.last]
            if (stack?.item === item) {
                onUse(adept, it)
            } else PASS
        }
    }
}

fun Toggled.ItemBlock(
    adepts: Adepts, block: Block,
    tags: TagCompound = emptyMap(),
    onPlace: (Adept, Position) -> (Bool) = { _, _ -> true }
) = Item(adepts, block.material, block.meta, 64, tags, { adept, interaction ->
    val clicked = interaction.clicked
    if (clicked != null) {
        if (!onPlace(adept, clicked)) {
            adept.blocks[clicked] = block
            val selected = adept.selected.last
            adept.inventory[selected] = adept.inventory[selected].add(-1)
            val last = adept.location.last
            adepts.send {
                sound("dig.stone", last.copy(x = last.x + 0.5, y = last.y + 0.5, z = last.z + 0.5), 1.0f, 1.0f)
            }
        } else {
            val selected = adept.selected.last
            adept.blockChange(clicked, adept.blocks[clicked].type)
            adept.setSlot(0, selected, adept.inventory[selected]!!)
        }
        disable(); CONSUME
    } else PASS
}) { _, _ -> PASS }

fun Toggled.ItemFood(
    adepts: Adepts, material: Short,
    meta: Short = 0, max: Byte = 64,
    tags: TagCompound = emptyMap(),
    onConsume: (Adept) -> (Unit) = {}
) = Item(adepts, material, meta, max, tags, onUse = { adept, _ ->
    adept.onRelease { disable() }
    val selected = adept.selected.last
    val current = adept.inventory[selected]!!
    scheduler.every(4.ticks)() {
        if (it > 2) current().apply {
            val volume = 0.5f + 0.5f * nextInt(2)
            val pitch = (nextFloat() - nextFloat()) * 0.2f + 1f
            adepts.send("random.eat", adept.location.last, volume, pitch)
        }
        if (it >= 7) {
            adept.status(adept.id, 9)
            val pitch = current().nextFloat() * 0.1f + 0.9f
            adepts.send("random.burp", adept.location.last, 0.5f, pitch)
            if (adept.inventory[selected] == current) {
                adept.inventory[selected] = current.add(-1)
                onConsume(adept);
            }
            disable()
        }
    }
    enable(); CONSUME
})


fun Toggled.ItemBow(
    adepts: Adepts, tags: TagCompound = emptyMap(),
    bonus: Float = 0f,
    onRelease: (Adept) -> (Bool) = { true },
    onHitPlayer: (Adept, Adept) -> (Float) = { _, _ -> -1f },
    onHitBlock: (Adept, Position) -> (Bool) = { _, _ -> true }
) = Item(adepts, BOW, 0, 1, tags, onUse = { adept, _ ->
    if (!adept.inventory.all { slot, stack ->
        slot !in 0..44 || stack.item == ITEM_ARROW
    }) {
        val start = TimeSource.Monotonic.markNow()
        adept.onRelease {
            val i = start.elapsedNow().inWholeTicks / 20f
            val force = (i * i + i * 2) / 3
            if (force >= 0.1) {
                val critical = force >= 1
                if (
                    onRelease(adept) &&
                    adept.inventory.remove(Stack(ITEM_ARROW, 1)) == 0
                ) Component {
                    val id = EID.getAndIncrement()
                    val (location, velocity) = Projectile(
                        scheduler, adepts, id, 60, adept.id,
                        adept.location.last.add(y = 1.52),
                        arrowVelocity(
                            adept.location.last,
                            min(1f, force) * 2
                        ), 0.05, 0.99
                    )
                    onEnabled {
                        val pitch = 1.0f / (current().nextFloat() * 0.4f + 1.2f) + force * 0.5f
                        adepts.send("random.bow", location.last, 1.0f, pitch)
                        adepts.send { metadata(id, mapOf(16 to Meta(0, (if (critical) 1 else 0).toByte()))) }
                    }
                    onDisabled {
                        val pitch = 1.2f / (current().nextFloat() * 0.2f + 0.9f)
                        adepts.send("random.bowhit", location.last, 1f, pitch)
                    }
                    ProjectileTracker(adept, adepts, location) { block, other ->
                        if (!enabled) adept.send("PROJECTILE TRACKER EMERGENCY")
                        if (other != null) {
                            //problems that require passing force through here.
                            val damage = onHitPlayer(adept, other)
                            if (damage > 0 && other.damageRanged(
                                location.last, velocity.last,
                                damage, bonus, critical, 1
                            )) {
                                adept.state(6, 0f)
                                disable()
                            }
                        } else if (block != null && onHitBlock(adept, block)) disable()
                    }
                }.enable()
            }
            disable()
        };
        enable()
        CONSUME
    } else PASS //TODO figure out if passing is wrong here.
})

//TODO make this cancellable
fun Toggled.ItemSplash(
    adepts: Adepts, type: Short, meta: Short,
    tags: TagCompound = emptyMap(),
    onHit: (Adept, Int) -> (Unit)
) = Item(adepts, type, meta, 64, tags, onUse = { adept, _ ->
    val id = EID.getAndIncrement()
    this@ItemSplash.transient potion@ {
        val (location, _) = Projectile(
            scheduler, adepts, id, 73, meta.toInt(),
            adept.location.last.add(y = 1.52),
            splashVelocity(adept.location.last),
            0.05, 0.99
        )

        val bounds = location.last.run {
            Bounds(
                x - 0.125, y, z - 0.125,
                x + 0.125, y + 0.25, z + 0.125
            )
        }.expand(Velocity(4.0, 2.0, 4.0))

        onEnabled {
            adepts.send("random.bow", location.last.add(y = 1.0), 0.5f, 0.5f)
        }

        onDisabled {
            adepts.send {
                effect(2002, location.last.toPosition(), meta.toInt(), false)
                despawn(id)
            }
        }

        scheduler.after(3.ticks)() { transient {
            PotionTracker(adept, adepts, location) { block, other ->
                if (other != null || block != null) {
                    adepts.values.filter { bounds.intersects(it.bounds.last) }.forEach {
                        val distance = it.location.last.distance(location.last)
                        if (distance < 4) {
                            val power = (((1.0 - distance / 4.0) * 200) + 0.5).toInt()
                            if (power > 20) onHit(it, power)
                        }
                        this@potion.disable()
                    }
                }
            }
        }.enable() }
    }
    val selected = adept.selected.last
    val current = adept.inventory[selected]!!
    adept.inventory[selected] = current.add(-1)
    CONSUME
})

val WATER_SPLASH_POTION: Short = 438
val SPLASH_POTION = 373

fun Toggled.ItemRod(
    adepts: Adepts, tags: TagCompound = emptyMap(),
    onHitPlayer: (Adept, Adept) -> (Float) = { _, _ -> -1f },
    onHitBlock: (Adept, Position) -> (Bool) = { _, _ -> true }
): Item {
    val hooks = MutableHolder<Adept>()
    hooks.onEach { _, adept ->
        val id = EID.getAndIncrement()
        val (location, velocity) = Projectile(
            scheduler, adepts, id, 90, adept.id,
            adept.location.last.add(y = 1.52),
            hookCast(adept.location.last), 0.04, 0.92
        )

        onEnabled {
            val pitch = 0.4f / (current().nextFloat() * 0.4f + 0.8f)
            adepts.send("random.bow", location.last, 0.5f, pitch)
        }
        fun attach(target: Adept) = transient {
            val anchor = target.location.map { it.add(y = 1.0) }
            anchor.onChanged { from, to -> adepts.send { entityMove(id, from, to) } }
            anchor { if (it.distance(adept.location.last) > 20) this@onEach.disable() }
            adepts.onRemoved { _, other -> if (other === target) this@onEach.disable() }
        }.enable()

        fun anchor(location: Location) = transient {
            adept.location { if (it.distance(location) > 20) this@onEach.disable() }
        }.enable()
        transient {
            ProjectileTracker(adept, adepts, location) { block, other ->
                if (other != null) {
                    val damage = onHitPlayer(adept, other)
                    if (damage >= 0) {
                        if (other.damageRanged(
                            location.last, velocity.last,
                            damage, 0f, false, 1
                        )) adept.state(6, 0f)
                        attach(other)
                        disable()
                    }
                } else if (block != null && onHitBlock(adept, block)) {
                    anchor(location.last); disable()
                }
            }
            location { if (it.distance(adept.location.last) > 20) this@onEach.disable() }
        }.enable()
        adept.selected.onChanged { _, _ -> disable() }
        this@ItemRod.onDisabled {
            disable()
        }
        adepts.onRemoved { _, other -> if (other === adept) disable() }
    }
    return Item(adepts, ROD, 0, 1, tags, onUse = { adept, interaction ->
        if (interaction.position != null) return@Item PASS
        if (!hooks.remove(adept)) hooks.add(adept)
        disable(); CONSUME
    })
}

fun Toggled.ItemTool(
    adepts: Adepts, material: Short,
    meta: Short = 0, tags: TagCompound = emptyMap(),
    damage: Float = 1f, hardness: (Adept, Position) -> (Byte)
) = Item(adepts, material, meta, 1, tags, onDig = { adept, interaction ->
    val position = interaction.position
    if (position != null) {
        val duration = hardness(adept, position).ticks
        if (!duration.isNegative())
            adepts.serverBreak(adept, position, duration)
    } else if (interaction.entity != null) {
        val target = adepts[interaction.entity]
        //TODO replace this with a proper onHit callback.
        if (target != null && damage >= 0)
            target.damageMelee(adept, damage, 0f)
    }
    CONSUME
})

fun Toggled.ItemSword(
    adepts: Adepts, type: Short,
    tags: TagCompound = emptyMap(), bonus: Float = 0f,
    onHit: (Adept, Adept) -> (Float) = { _, _ -> 1f }
) = Item(adepts, type, 0, 1, tags, onDig = { adept, interaction ->
    if (interaction.entity == null) return@Item PASS
    val target = adepts[interaction.entity]
    if (target != null) {
        val damage = onHit(adept, target)
        if (damage >= 0) {
            if (target.damageMelee(adept, damage, bonus)) {
                adept.sprinting(false)
            }
            CONSUME
        }
    }; PASS
})


fun Toggled.DefaultHit(
    adepts: Adepts, onHit: (Adept, Adept) -> (Float) = { _, _ -> 1f }
) = adepts.onEach { _, adept ->
    adept.onLeftClick { interaction ->
        if (interaction.entity == null) return@onLeftClick PASS
        val target = adepts[interaction.entity]
        if (target != null) {
            val damage = onHit(adept, target)
            if (damage >= 0) {
                if (target.damageMelee(adept, damage, 0f)) {
                    adept.sprinting(false)
                }
            }
            CONSUME
        } else PASS
    }
}

val ITEM_AIR = Item(0, 0, 0)
val ITEM_ARROW = Item(ARROW)
val ITEM_DIAMOND_BOOTS = Item(313, 0, 1, emptyMap())
val ITEM_DIAMOND_LEGGINGS = Item(312, 0, 1, emptyMap())
val ITEM_DIAMOND_CHEST = Item(311, 0, 1, emptyMap())
val ITEM_DIAMOND_HELMET = Item(310, 0, 1, emptyMap())
